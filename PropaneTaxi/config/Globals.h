//
//  Globals.h
//

#ifndef MovieTrivia_Globals_h
#define MovieTrivia_Globals_h

extern UIColor *customBlueColor;
extern UIColor *customGreenColor;

//// global fields start

// main

extern NSString *gAPIString;

extern BOOL gIsLoggedIn;
extern NSString *gCustomerID;
extern NSString *gAuthToken;
extern NSString *gCardID;
extern NSString *gCartToken;
extern NSString *gServiceAreaID;
extern NSString *gLocationLKPID;

// order

extern NSString *gShippingPrice;
extern NSString *gQuantity;
extern NSString *gShippingDate;
extern NSString *gPickupLocation;
extern NSString *gConnectionService;

extern NSString *gProductID;
extern NSString *gProductName;
extern NSString *gProductPrice;


extern NSString *gProductID_conn;
extern NSString *gProductName_conn;
extern NSString *gProductPrice_conn;

extern NSString *gCartID;
extern NSString *gCartID_conn;

// account

extern NSString *gFirstName;
extern NSString *gLastName;
extern NSString *gEmail;
extern NSString *gPassword;
extern NSString *gMobilePhone;
extern NSString *gHomePhone;

extern NSString *gRealPassword;
extern NSString *gExpirationYear;

// shipping

extern NSString *gShipAddress1;
extern NSString *gShipAddress2;
extern NSString *gShipCity;
extern NSString *gShipState;
extern NSString *gShipZipCode;

// Payment

extern BOOL gIsAddressSameAsShipping;
extern NSString *gCardHolderName;
extern NSString *gCardType;
extern NSString *gCardNumber;
extern NSString *gCVV;
extern NSString *gExpirationMonth;

extern NSString *gDeliveryNotes;
extern NSString *gPromoCode;

extern NSString *gPayAddress1;
extern NSString *gPayAddress2;
extern NSString *gPayCity;
extern NSString *gPayState;
extern NSString *gPayZipCode;

extern NSString *gTotal;
extern NSString *gOrderID;

//// global fields end


//extern NSMutableArray *questionAnswerTbl;
//extern NSMutableArray *quizArray;
//extern NSMutableArray *customQuizArray;
//extern NSMutableArray *keyArray;
extern NSMutableArray *FARArray;
extern NSMutableArray *REGArray;
extern NSMutableArray *BECArray;
extern NSMutableArray *AUDArray;
extern NSMutableArray *cardsArrayMain;
extern UIColor *colorCustomGreen;

extern float delta; 

extern NSMutableArray *apiResponseArray;

extern NSMutableArray *viewControllersArray;

//
//extern NSInteger currentQuestion;
//extern NSInteger totalQuestions;
//extern bool isCustomQuiz;
//extern bool isQuizComplete;
//extern NSString *masterCategory;
//extern NSString *chosenAnswer;
//extern NSString *difficulty;
extern NSString *category;
//extern NSInteger numberCorrect;
//extern NSInteger numberWrong;
//extern UIColor *colorCustomGreen;
//extern UIColor *colorCustomGreenBar;
//extern UIColor *colorCustomBlueBar;

extern NSInteger currentCard;
extern NSInteger totalCards;
//extern NSInteger gameNumber;
//extern float timeLimit;
//extern bool alreadyAnswered;
//extern bool buzzPressed;
//extern bool showPressed;

#endif
