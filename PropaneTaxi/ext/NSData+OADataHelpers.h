//
//  NSData+OADataHelpers.h
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/18/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (OADataHelpers)
-(NSString*)UTF8String;
-(NSData*)dataByHealingUTF8Stream;
@end
