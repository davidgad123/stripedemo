//
//  main.m
//  PropaneTaxi
//
//  Created by Jake on 11/5/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

NSMutableArray *apiResponseArray;

UIColor *customBlueColor;
UIColor *customGreenColor;

//// global fields start

NSString *gAPIString;

// main

BOOL gIsLoggedIn;
NSString *gCustomerID;
NSString *gAuthToken;
NSString *gCardID;
NSString *gCartToken;
NSString *gServiceAreaID;
NSString *gLocationLKPID;

// order

NSString *gShippingPrice;
NSString *gProductPrice;
NSString *gQuantity;
NSString *gShippingDate;
NSString *gPickupLocation;
NSString *gConnectionService;
NSString *gProductID;
NSString *gProductName;

NSString *gProductID_conn;
NSString *gProductName_conn;
NSString *gProductPrice_conn;

// account

NSString *gFirstName;
NSString *gLastName;
NSString *gEmail;
NSString *gPassword;
NSString *gMobilePhone;
NSString *gHomePhone;

// shipping

NSString *gShipAddress1;
NSString *gShipAddress2;
NSString *gShipCity;
NSString *gShipState;
NSString *gShipZipCode;

// Payment

BOOL gIsAddressSameAsShipping;
NSString *gCardHolderName;
NSString *gCardType;
NSString *gCardNumber;
NSString *gCVV;
NSString *gExpirationMonth;

NSString *gDeliveryNotes;
NSString *gPromoCode;

NSString *gPayAddress1;
NSString *gPayAddress2;
NSString *gPayCity;
NSString *gPayState;
NSString *gPayZipCode;

NSString *gRealPassword;

NSString *gExpirationYear;

NSString *gCartID;
NSString *gCartID_conn;

NSString *gTotal;
NSString *gOrderID;

float delta;

NSMutableArray *viewControllersArray;

//// global fields end


int main(int argc, char * argv[])
{
    apiResponseArray = [[NSMutableArray alloc] init];
    
    viewControllersArray = [[NSMutableArray alloc] init];
    
    [viewControllersArray addObject:@"HomeViewController"];
    
    //// global fields start
    
  
    /* changed at 2015.6.25
     * Dev link for API
     */
    
    // TODO for release
    
    gAPIString = @"http://dev.propanecab.com/api/mobile/";
//    gAPIString = @"https://www.propanecab.com/api/mobile/";
    
    delta = 0.0;
    
    // main
    
    gIsLoggedIn = NO;
    gCustomerID = @"";
    gAuthToken = @"";
    gCardID = @"";
    gCartToken = @"";
    gServiceAreaID = @"";
    gLocationLKPID = @"";
    
    // order
    gShippingPrice = @"";
    gProductPrice  = @"";
    gProductID = @"1";

    gQuantity = @"";
    gShippingDate = @"";
    gPickupLocation = @"";
    gProductName = @"";
    
    gProductID_conn = @"";
    gProductName_conn = @"";
    gProductPrice_conn = @"";

    gConnectionService = @"No";
    
    gCartID = @"";
    gCartID_conn = @"";

    
    // account
    
    gFirstName = @"";
    gLastName = @"";
    gEmail = @"";
    gPassword = @"";
    gMobilePhone = @"";
    gHomePhone = @"";
    gRealPassword = @"";

    // shipping
    
    gShipAddress1 = @"";
    gShipAddress2 = @"";
    gShipCity = @"";
    gShipState = @"";
    gShipZipCode = @"";
    
    // Payment
    
    gIsAddressSameAsShipping = YES;
    gCardHolderName = @"";
    gCardType = @"";
    gCardNumber = @"";
    gCVV = @"";
    gExpirationMonth = @"";
    gExpirationYear = @"";
    
    gDeliveryNotes = @"";
    gPromoCode = @"";
    
    gPayAddress1 = @"";
    gPayAddress2 = @"";
    gPayCity = @"";
    gPayState = @"";
    gPayZipCode = @"";
    
    gTotal = @"";
    gOrderID = @"";
    //// global fields end

    
    customBlueColor = [UIColor colorWithRed:0.0f/255.0f green:114.0f/255.0f blue:173.0f/255.0f alpha:1.0f];
    
    customGreenColor = [UIColor colorWithRed:128.0f/255.0f green:193.0f/255.0f blue:85.0f/255.0f alpha:1.0f];
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
