//
//  GrillMasterDetailViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PTArticle;
@interface GrillMasterDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *articleName;

@property (weak, nonatomic) IBOutlet UILabel *articleTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) PTArticle *article;
//@property (weak, nonatomic) IBOutlet UITextView *articleContentTextView;
@property (weak, nonatomic) IBOutlet UIWebView *articleContentView;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;

@end
