//
//  GrillMasterDetailViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "GrillMasterDetailViewController.h"
#import "PTArticle.h"

@interface GrillMasterDetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *navigationSeparator;
@property (weak, nonatomic) IBOutlet UIImageView *breadcrumbNavigation;

@end

@implementation GrillMasterDetailViewController

@synthesize btnHome;
@synthesize btnNav;
@synthesize myScrollView;
@synthesize lblHeader;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self configureArticleFields];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (IBAction)backBtnPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }

    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (void)configureArticleFields {
    UIFont *navigationBarFont = [UIFont fontWithName:@"Open Sans" size:10.f];
    lblHeader.text = self.categoryName.uppercaseString;
    lblHeader.font = navigationBarFont;
    [self adjustLblHeaderPosition];
    [self adjustNavigationSeparatorPosition];
    
    self.articleName.text = self.article.title.uppercaseString;
    self.articleName.font = navigationBarFont;
    
//    NSString *htmlString = @"<font face=\"HelveticaNeue-Light\" size=+2>";
//    htmlString = [htmlString stringByAppendingString:self.article.article_content];
//    
//    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    self.articleContentTextView.attributedText = attributedString;
    NSString *htmlString = [NSString stringWithFormat:@"<body><font face=\"HelveticaNeue-Light\">%@</font></body>", self.article.article_content];
    [self.articleContentView loadHTMLString:htmlString baseURL:nil];
    [self.articleContentView setBackgroundColor:[UIColor clearColor]];
    [self.articleContentView setOpaque:NO];
    
    self.articleTitleLabel.text = self.article.title;
    self.authorLabel.text = self.article.author;
    self.dateLabel.text = self.article.create_date;
}

- (void)adjustLblHeaderPosition
{
    [lblHeader sizeToFit];
    lblHeader.center = CGPointMake(lblHeader.center.x, self.breadcrumbNavigation.center.y);
}

- (void)adjustNavigationSeparatorPosition
{
    CGFloat newSeparatorStarting = CGRectGetMinX(lblHeader.frame)+CGRectGetWidth(lblHeader.frame);
    CGFloat differenceBetweenXOrigin = newSeparatorStarting-CGRectGetMinX(self.navigationSeparator.frame);
    [self adjustArticleNameNavigationPosition:differenceBetweenXOrigin];
    
    CGRect newSeparatorFrame = self.navigationSeparator.frame;
    newSeparatorFrame.origin.x = newSeparatorStarting;
    self.navigationSeparator.frame = newSeparatorFrame;
}

- (void)adjustArticleNameNavigationPosition:(CGFloat)distanceToMoveLabel
{
    CGRect newArticleNameFrame = self.articleName.frame;
    newArticleNameFrame.origin.x += distanceToMoveLabel;
    self.articleName.frame = newArticleNameFrame;
}

@end
