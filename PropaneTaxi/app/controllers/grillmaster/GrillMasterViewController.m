//
//  GrillMasterViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/5/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "GrillMasterViewController.h"
#import "GrillMasterSubViewController.h"
#import "SignInViewController.h"
#import "PTCategoryCollection.h"
#import "PTCategory.h"
#import "PTArticleCollection.h"
#import "PTArticle.h"
#import "AFNetworking.h"
#import "NSData+OADataHelpers.h"
#import "MBProgressHUD.h"

@interface GrillMasterViewController () <PTCategoryCollectionDelegate,UIAlertViewDelegate>
@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, strong) NSArray *articles;
@end

@implementation GrillMasterViewController

static CGFloat const PTGrillMasterBaseFont = 9.0f;

@synthesize navBar;
@synthesize btnHome;
@synthesize btnNav;

@synthesize btnOrder;
@synthesize btnChangeZip;

@synthesize btnSpecials;
@synthesize btnRecipes;
@synthesize btnGrillTimer;

@synthesize btnSafety;
@synthesize btnMaintenance;
@synthesize btnTroubleShooting;

@synthesize btnMyAccount;
@synthesize btnGrillSecrets;
@synthesize btnContact;
@synthesize lblHeader;

@synthesize lblLoggedInAs;
@synthesize lblZip;

@synthesize myScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [viewControllersArray addObject:@"GrillMasterViewController"];
	// Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];
    
    float delta = 0;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
    } else {
        delta = -20;
    }
    
    navBar.frame = CGRectMake(0, delta+20, 320, 44);
    btnNav.frame = CGRectMake(20, delta+27, 30, 30);
    
    // scroll view
    myScrollView.frame = CGRectMake(0, 84, 320, 480);
    myScrollView.contentSize = CGSizeMake(320, 800);
    
    lblLoggedInAs.text = @"";
    lblZip.text = @"";
    
    lblLoggedInAs.text = gFirstName;
    lblZip.text = gShipZipCode;

    [self apiCalls];
    
    [self.labelGrillTimer setFont:[UIFont fontWithName:@"Open Sans" size:PTGrillMasterBaseFont]];
    [self.labelMyAccount setFont:[UIFont fontWithName:@"Open Sans" size:PTGrillMasterBaseFont]];
    [self.labelContact setFont:[UIFont fontWithName:@"Open Sans" size:PTGrillMasterBaseFont]];
}

- (void)apiCalls {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.categoryCollection = [[PTCategoryCollection alloc] init];
    self.categoryCollection.delegate = self;
    [self.categoryCollection updateCategories];

    self.articleCollection = [[PTArticleCollection alloc] init];
    self.articleCollection.collectionOwner = self;
    [self.articleCollection updateArticles];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Config Methods
- (void)configButtons:(NSArray*)categoriesFromAPI
{
//    NSArray *buttons = @[btnSafety,btnMaintenance,btnTroubleShooting,btnSpecials,btnGrillSecrets,btnRecipes];
//    NSArray *labels = @[self.labelSafety,self.labelMaintenance,self.labelTroubleShooting,self.labelSpecials,self.labelGrillSecrets,self.labelRecipes];
    
    NSArray *buttons = @[btnSafety,btnMaintenance,btnTroubleShooting,btnSpecials];
    NSArray *labels = @[self.labelSafety,self.labelMaintenance,self.labelTroubleShooting,self.labelSpecials];

    
    WEAKSELF
    [categoriesFromAPI enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if (idx >= buttons.count) {
            return;
        }
        UIButton *button = [buttons objectAtIndex:idx];
        UILabel *label = [labels objectAtIndex:idx];
        PTCategory *category = obj;
        NSString *imageString = [weakSelf getIconForButton:category.category_icon];
        UIImage *image = [UIImage imageNamed:imageString];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        button.titleLabel.textColor = [UIColor clearColor];
        button.titleLabel.text = category.category_name_short.uppercaseString;
        // we'll use the category id to fetch the correct articles for the GrillMasterSubViewController
        button.tag = category.article_category_id.integerValue;
        
        label.font = [UIFont fontWithName:@"Open Sans" size:PTGrillMasterBaseFont];
        label.text = category.category_name_short.uppercaseString;
    }];
}

- (NSString*)getIconForButton:(NSString*)categoryIcon {
    NSString *imageForButtonBackground;
    if ([categoryIcon isEqualToString:@"book"]) {
        imageForButtonBackground = @"button_book";
    } else if ([categoryIcon isEqualToString:@"warning"]) {
        imageForButtonBackground = @"button_alert";
    } else if ([categoryIcon isEqualToString:@"cutlery"]) {
        imageForButtonBackground = @"button_cutlery";
    } else {
        imageForButtonBackground = @"button_book";
    }
    return imageForButtonBackground;
}

- (void)openViewController:(UIButton*)sender {
    NSString *buttonText = [sender titleLabel].text.lowercaseString;
    NSString *viewControllerIdentifier = [self getViewControllerIdentifier:buttonText];
    if (viewControllerIdentifier) {
        GrillMasterSubViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:viewControllerIdentifier];
        NSNumber *buttonTag = [NSNumber numberWithInteger:sender.tag];
        NSString *categoryName = [self.categoryCollection getShortCategoryNameBy:buttonTag];
        vc.categoryName = categoryName;
        NSArray *articlesForCategory = [self.articleCollection articlesByCategory:[self.categoryCollection getCategoryNameBy:buttonTag]];
        vc.articles = articlesForCategory;
//        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
        
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (NSString*)getViewControllerIdentifier:(NSString*)buttonText {
    NSString *viewControllerIdentifier;
    if ([buttonText isEqualToString:@"grill timer"]) {
        viewControllerIdentifier = @"TimerViewController";
    } else if ([buttonText isEqualToString:@"my account"]) {
        viewControllerIdentifier = @"AccountViewController";
    } else if ([buttonText isEqualToString:@"contact"]) {
        viewControllerIdentifier = @"ContactViewController";
    } else if ([buttonText isEqualToString:@"f.a.q."]) {
        viewControllerIdentifier = @"GrillMasterSubViewController";
    } else if ([buttonText isEqualToString:@"tank safety"]) {
        viewControllerIdentifier = @"GrillMasterSubViewController";
    } else if ([buttonText isEqualToString:@"grill safety"]) {
        viewControllerIdentifier = @"GrillMasterSubViewController";
    } else if ([buttonText isEqualToString:@"disconnecting"]) {
        viewControllerIdentifier = @"GrillMasterSubViewController";
    } else if ([buttonText isEqualToString:@"grill maintenance"]) {
        viewControllerIdentifier = @"GrillMasterSubViewController";
    } else if ([buttonText isEqualToString:@"grilling tips"]) {
        viewControllerIdentifier = @"GrillMasterSecretsViewController";
    } else if ([buttonText isEqualToString:@"grill secrets"]) {
        viewControllerIdentifier = @"GrillMasterSecretsViewController";
    } else if ([buttonText isEqualToString:@"recipes"]) {
        viewControllerIdentifier = @"GrillMasterSubViewController";
    } else {
        DLog(@"\nViewController unknown");
    }
    return viewControllerIdentifier;
}

#pragma mark - UI Events
- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }

    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnOrderPressed:(id)sender{
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderViewController"];
    
    
    if ([gShipZipCode isEqualToString:@""]) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckAvailabilityViewController"];
    }
    
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnChangeZipPressed:(id)sender{
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AccountViewController"];
    
    if (!gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignInViewController"];
    }
    
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

#pragma mark - Main Navigation Buttons
- (IBAction)btnSpecialsPressed:(id)sender {
    if ([sender isKindOfClass:[UIButton class]]) {
        [self openViewController:sender];
    }
}

- (IBAction)btnRecipesPressed:(id)sender {
    if ([sender isKindOfClass:[UIButton class]]) {
        [self openViewController:sender];
    }
}

- (IBAction)btnGrillTimerPressed:(id)sender{
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TimerViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnSafetyPressed:(id)sender{
    if ([sender isKindOfClass:[UIButton class]]) {
        [self openViewController:sender];
    }
}

- (IBAction)btnMaintenancePressed:(id)sender
{
    if ([sender isKindOfClass:[UIButton class]]) {
        [self openViewController:sender];
    }
}

- (IBAction)btnTroubleShootingPressed:(id)sender
{
    if ([sender isKindOfClass:[UIButton class]]) {
        [self openViewController:sender];
    }
}

- (IBAction)btnMyAccountPressed:(id)sender{
    if (!gIsLoggedIn) {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignInViewController"];
        [(SignInViewController*)vc setRedirectViewControllerName:@"AccountViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    } else {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AccountViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
}

- (IBAction)btnGrillSecretsPressed:(id)sender
{
    if ([sender isKindOfClass:[UIButton class]]) {
        [self openViewController:sender];
    }
}

- (IBAction)btnContactPressed:(id)sender
{
    if ([sender isKindOfClass:[UIButton class]]) {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ContactViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
}

#pragma mark - Protocol Conformance
#pragma mark PTCategoryCollectionDelegate
- (void)didFetchCategories
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    NSArray *categories = self.categoryCollection.categories;
    if (categories) {
        [self configButtons:categories];
    }
}

- (void)didFetchCategoriesWithError
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed to Fetch Categories" message:@"There was a problem fetching the categories. Try again or hit Cancel to return to the home page." delegate:self cancelButtonTitle:@"Try Again" otherButtonTitles:@"Cancel",nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self apiCalls];
    } else {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
        if (gIsLoggedIn) {
            vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
        }
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
}

@end
