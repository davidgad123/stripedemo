//
//  AccountViewController.m
//  PropaneTaxi
//
//  Created by Jake on 4/7/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import "AccountViewController.h"

@interface AccountViewController ()
{
    NSArray *_statePickerData;
}
@end

@implementation AccountViewController

@synthesize navBar;
@synthesize btnHome;
@synthesize btnNav;

@synthesize btnUpdate;

@synthesize myScrollView;

@synthesize emailTxt;
@synthesize passwordTxt;
@synthesize confirmPasswordTxt;
//@synthesize homePhoneTxt;

@synthesize payFirstNameTxt;
@synthesize payLastNameTxt;
@synthesize payAddress1Txt;
@synthesize payAddress2Txt;
@synthesize payCityTxt;
@synthesize payStateTxt;
@synthesize payZipTxt;

@synthesize shipFirstNameTxt;
@synthesize shipLastNameTxt;
@synthesize shipAddress1Txt;
@synthesize shipAddress2Txt;
@synthesize shipCityTxt;
@synthesize shipStateTxt;
@synthesize shipZipTxt;

@synthesize emailTitle;
@synthesize passwordTitle;
@synthesize confirmPasswordTitle;
//@synthesize homePhoneTitle;

@synthesize shipFirstNameTitle;
@synthesize shipLastNameTitle;
@synthesize shipAddress1Title;
@synthesize shipAddress2Title;
@synthesize shipCityTitle;
@synthesize shipStateTitle;
@synthesize shipZipTitle;

@synthesize payFirstNameTitle;
@synthesize payLastNameTitle;
@synthesize payAddress1Title;
@synthesize paypAddress2Title;
@synthesize payCityTitle;
@synthesize payStateTitle;
@synthesize payZipTitle;

@synthesize pickerStateShip;
@synthesize pickerStatePay;

@synthesize generalImageView;
@synthesize shipImageView;
@synthesize payImageView;

@synthesize lblAccount;

@synthesize activityIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    NSString *autoLoginStatus = [[NSUserDefaults standardUserDefaults] objectForKey:@"autoSignIn"];
    if ([autoLoginStatus isEqualToString:@"1"]){
        [self.autoSignInButton setImage:[UIImage imageNamed:@"signIn_auto_check.png"] forState:UIControlStateNormal];
    } else {
        [self.autoSignInButton setImage:[UIImage imageNamed:@"signIn_auto_uncheck.png"] forState:UIControlStateNormal];
    }
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}
- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }
    
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}
- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (BOOL)textFieldShouldReturn:(CustomUITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}


- (void)textFieldDidBeginEditing:(CustomUITextField *)textField {

    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y);
    [myScrollView setContentOffset:scrollPoint animated:YES];
}

- (void)textFieldDidEndEditing:(CustomUITextField *)textField {
    
    [myScrollView setContentOffset:CGPointZero animated:YES];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [viewControllersArray addObject:@"AccountViewController"];
    
    activityIndicator.hidden = YES;
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];
    
    float delta = 0;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
    } else {
        delta = -20;
    }
    
    [self apiCall:[NSString stringWithFormat:@"/get_customer/customer_id/%@/auth_token/%@",gCustomerID,gAuthToken]:@"shipping"];
    
    CGFloat distanceToMoveAllFields = 15;
    
    lblAccount.font = [UIFont fontWithName:@"HelveticaNeueLTCom-Blk" size:12];
    lblAccount.textColor = [UIColor darkGrayColor];
    
    navBar.frame = CGRectMake(0, distanceToMoveAllFields+delta+20, 320, 44);
    btnNav.frame = CGRectMake(20, distanceToMoveAllFields+delta+27, 30, 30);
    
    if (SH > 500) {
        myScrollView.contentSize = CGSizeMake(320,distanceToMoveAllFields+1300);
    } else {
        myScrollView.contentSize = CGSizeMake(320,distanceToMoveAllFields+1400);
    }
    
    myScrollView.frame = CGRectMake(0, delta+100, 320, 480);

    
    myScrollView.userInteractionEnabled = YES;
    myScrollView.exclusiveTouch = YES;
    
    myScrollView.canCancelContentTouches = YES;
    myScrollView.delaysContentTouches = YES;
    

    
    emailTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    passwordTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    confirmPasswordTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
//    homePhoneTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    
    shipFirstNameTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    shipLastNameTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    shipAddress1Title.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    shipAddress2Title.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    shipCityTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    shipStateTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    shipZipTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];

    payFirstNameTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    payLastNameTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    payAddress1Title.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    paypAddress2Title.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    payCityTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    payStateTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    payZipTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    
    emailTitle.textColor = customBlueColor;
    passwordTitle.textColor = customBlueColor;
    confirmPasswordTitle.textColor = customBlueColor;
//    homePhoneTitle.textColor = customBlueColor;
    
    shipFirstNameTitle.textColor = customBlueColor;
    shipLastNameTitle.textColor = customBlueColor;
    shipAddress1Title.textColor = customBlueColor;
    shipAddress2Title.textColor = customBlueColor;
    shipCityTitle.textColor = customBlueColor;
    shipStateTitle.textColor = customBlueColor;
    shipZipTitle.textColor = customBlueColor;
    
    payFirstNameTitle.textColor = customBlueColor;
    payLastNameTitle.textColor = customBlueColor;
    payAddress1Title.textColor = customBlueColor;
    paypAddress2Title.textColor = customBlueColor;
    payCityTitle.textColor = customBlueColor;
    payStateTitle.textColor = customBlueColor;
    payZipTitle.textColor = customBlueColor;

    // labels
//    emailTitle.frame = CGRectMake(14, distanceToMoveAllFields+190, 290, 21);
//    passwordTitle.frame = CGRectMake(14, distanceToMoveAllFields+245, 290, 21);
//    mobilePhoneTitle.frame = CGRectMake(14, distanceToMoveAllFields+300, 290, 21);
//    homePhoneTitle.frame = CGRectMake(14, distanceToMoveAllFields+355, 290, 21);
    emailTitle.frame = CGRectMake(14, distanceToMoveAllFields+80, 290, 21);
    passwordTitle.frame = CGRectMake(14, distanceToMoveAllFields+135, 290, 21);
    confirmPasswordTitle.frame = CGRectMake(14, distanceToMoveAllFields+190, 290, 21);
//    homePhoneTitle.frame = CGRectMake(14, distanceToMoveAllFields+245, 290, 21);
    
    payFirstNameTitle.frame = CGRectMake(14, distanceToMoveAllFields+340, 290, 21);
    payLastNameTitle.frame = CGRectMake(14, distanceToMoveAllFields+395, 290, 21);
    payAddress1Title.frame = CGRectMake(14, distanceToMoveAllFields+450, 290, 21);
    paypAddress2Title.frame = CGRectMake(14, distanceToMoveAllFields+505, 290, 21);
    payCityTitle.frame = CGRectMake(14, distanceToMoveAllFields+560, 290, 21);
    payStateTitle.frame = CGRectMake(14, distanceToMoveAllFields+615, 290, 21);
    payZipTitle.frame = CGRectMake(14, distanceToMoveAllFields+670, 290, 21);
    
    shipFirstNameTitle.frame = CGRectMake(14, distanceToMoveAllFields+800, 290, 21);
    shipLastNameTitle.frame = CGRectMake(14, distanceToMoveAllFields+855, 290, 21);
    shipAddress1Title.frame = CGRectMake(14,distanceToMoveAllFields+910, 290, 21);
    shipAddress2Title.frame = CGRectMake(14, distanceToMoveAllFields+965, 290, 21);
    shipCityTitle.frame = CGRectMake(14, distanceToMoveAllFields+1020, 290, 21);
    shipStateTitle.frame = CGRectMake(14, distanceToMoveAllFields+1075, 290, 21);
    shipZipTitle.frame = CGRectMake(14, distanceToMoveAllFields+1130, 290, 21);
    // fields
    

//    emailTxt.frame = CGRectMake(14, distanceToMoveAllFields+210, 290, 30);
//    passwordTxt.frame = CGRectMake(14, distanceToMoveAllFields+265, 290, 30);
//    confirmPasswordTxt.frame = CGRectMake(14, distanceToMoveAllFields+320, 290, 30);
//    homePhoneTxt.frame = CGRectMake(14, distanceToMoveAllFields+375, 290, 30);
    emailTxt.frame = CGRectMake(14, distanceToMoveAllFields+100, 290, 30);
    passwordTxt.frame = CGRectMake(14, distanceToMoveAllFields+155, 290, 30);
    confirmPasswordTxt.frame = CGRectMake(14, distanceToMoveAllFields+210, 290, 30);

    payFirstNameTxt.frame = CGRectMake(14, distanceToMoveAllFields+360, 290, 30);
    payLastNameTxt.frame = CGRectMake(14, distanceToMoveAllFields+415, 290, 30);
    payAddress1Txt.frame = CGRectMake(14, distanceToMoveAllFields+470, 290, 30);
    payAddress2Txt.frame = CGRectMake(14, distanceToMoveAllFields+525, 290, 30);
    payCityTxt.frame = CGRectMake(14, distanceToMoveAllFields+580, 290, 30);
    payStateTxt.frame = CGRectMake(14, distanceToMoveAllFields+635, 290, 30);
    payZipTxt.frame = CGRectMake(14, distanceToMoveAllFields+690, 290, 30);

    shipFirstNameTxt.frame = CGRectMake(14, distanceToMoveAllFields+820, 290, 30);
    shipLastNameTxt.frame = CGRectMake(14, distanceToMoveAllFields+875, 290, 30);
    shipAddress1Txt.frame = CGRectMake(14, distanceToMoveAllFields+930, 290, 30);
    shipAddress2Txt.frame = CGRectMake(14, distanceToMoveAllFields+985, 290, 30);
    shipCityTxt.frame = CGRectMake(14, distanceToMoveAllFields+1040, 290, 30);
    shipStateTxt.frame = CGRectMake(14, distanceToMoveAllFields+1095, 290, 30);
    shipZipTxt.frame = CGRectMake(14, distanceToMoveAllFields+1150, 290, 30);
 
    
    UIPickerView *picker = [[UIPickerView alloc] init];
    picker.dataSource = self;
    picker.delegate = self;
    self.payStateTxt.inputView = picker;
    self.shipStateTxt.inputView = picker;
    _statePickerData = @[@"AA",@"AB",@"AE",@"AK",@"AL",@"AP",@"AR",@"AS",@"AZ",@"BC",@"CA",@"CO",@"CT",@"DC",
                         @"DE",@"FL",@"GA",@"GU",@"HI",@"IA",@"ID",@"IL",@"IN",@"KS",@"KY",@"LA",@"MA",@"MB",
                         @"MD",@"ME",@"MI",@"MN",@"MO",@"MS",@"MT",@"NB",@"NC",@"ND",@"NE",@"NF",@"NH",@"NJ",
                         @"NM",@"NS",@"NT",@"NU",@"NV",@"NY",@"OH",@"OK",@"ON",@"OR",@"PA",@"PE",@"PQ",@"PR",
                         @"RI",@"SC",@"SD",@"SK",@"TN",@"TX",@"UT",@"VA",@"VI",@"VT",@"WA",@"WI",@"WV",@"WY",@"YT"];
   // images
    
    generalImageView.frame = CGRectMake(14, distanceToMoveAllFields+40, 292, 31);
    payImageView.frame = CGRectMake(14, distanceToMoveAllFields+300, 292, 31);
    shipImageView.frame = CGRectMake(14, distanceToMoveAllFields+760, 292, 31);
    
    btnUpdate.frame = CGRectMake(12, distanceToMoveAllFields+1200, 298, 65);
    
    passwordTxt.text = @"";
    
	// Do any additional setup after loading the view.
}

#pragma mark - PickerView Data Source

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _statePickerData.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return _statePickerData[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if ([self.payStateTxt isEditing]) {
        self.payStateTxt.text = _statePickerData[row];
    }
    else if ([self.shipStateTxt isEditing]) {
        self.shipStateTxt.text = _statePickerData[row];
    }
    
    //[self.payStateTxt resignFirstResponder];
}

- (void) apiCall:(NSString *)URLString :(NSString *)requestFor {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    [apiResponseArray removeAllObjects];
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:requestFor];
        });
    });
}

- (void) processAPICall:(NSString *)requestFor  {
    
    
    if ([requestFor isEqualToString:@"shipping"]) {
        
        // NSLog(@"apiResponseArray: %@",apiResponseArray);
        
        gShipAddress1 = [apiResponseArray valueForKey:@"ship_address_1"];
        gShipAddress2 = [apiResponseArray valueForKey:@"ship_address_2"];
        gShipCity = [apiResponseArray valueForKey:@"ship_city"];
        gShipState = [apiResponseArray valueForKey:@"ship_state"];
        gShipZipCode = [[apiResponseArray valueForKey:@"ship_zip"] stringValue];
        
        gPayAddress1 = [apiResponseArray valueForKey:@"address_1"];
        gPayAddress2 = [apiResponseArray valueForKey:@"address_2"];
        gPayCity = [apiResponseArray valueForKey:@"city"];
        gPayState = [apiResponseArray valueForKey:@"state"];
        gPayZipCode = [[apiResponseArray valueForKey:@"zip"] stringValue];

        gFirstName = [apiResponseArray valueForKey:@"first_name"];
        gLastName = [apiResponseArray valueForKey:@"last_name"];
        gEmail = [apiResponseArray valueForKey:@"email_address"];
//        gShipFirstName = [apiResponseArray valueForKey:@"ship_first_name"];
//        gShipLastName = [apiResponseArray valueForKey:@"ship_last_name"];
//        gHomePhone = [apiResponseArray valueForKey:@"phone_number"];
//        gMobilePhone = [apiResponseArray valueForKey:@"phone_number_mobile"];

        
        // NSLog(@"1");
        emailTxt.text = [NSString stringWithFormat:@"%@",gEmail];
//        passwordTxt.text = [NSString stringWithFormat:@"%@",gPassword];
//        confirmPasswordTxt.text = [NSString stringWithFormat:@"%@",gMobilePhone];
//        homePhoneTxt.text = [NSString stringWithFormat:@"%@",gHomePhone];
        // NSLog(@"2");
        payFirstNameTxt.text = [NSString stringWithFormat:@"%@",gFirstName];
        payLastNameTxt.text = [NSString stringWithFormat:@"%@",gLastName];
        payAddress1Txt.text = [NSString stringWithFormat:@"%@",gPayAddress1];
        payAddress2Txt.text = [NSString stringWithFormat:@"%@",gPayAddress2];
        payCityTxt.text = [NSString stringWithFormat:@"%@",gPayCity];
        payStateTxt.text = [NSString stringWithFormat:@"%@",gPayState];
        payZipTxt.text = [NSString stringWithFormat:@"%@",gPayZipCode];
        // NSLog(@"3");
//        shipFirstNameTxt.text = [NSString stringWithFormat:@"%@",gShipFirstName];
//        shipLastNameTxt.text = [NSString stringWithFormat:@"%@",gShipLastName];
        shipFirstNameTxt.text = [NSString stringWithFormat:@"%@",gFirstName];
        shipLastNameTxt.text = [NSString stringWithFormat:@"%@",gLastName];
        shipAddress1Txt.text = [NSString stringWithFormat:@"%@",gShipAddress1];
        shipAddress2Txt.text = [NSString stringWithFormat:@"%@",gShipAddress2];
        shipCityTxt.text = [NSString stringWithFormat:@"%@",gShipCity];
        shipStateTxt.text = [NSString stringWithFormat:@"%@",gShipState];
        shipZipTxt.text = [NSString stringWithFormat:@"%@",gShipZipCode];
        
      }
    
    if ([requestFor isEqualToString:@"update"]) {
        // NSLog(@"update - apiResponseArray: %@",apiResponseArray);
        
        NSString *response = [apiResponseArray valueForKey:@"MSG"];
        if ([response isEqualToString:@"success"]) {
            
            gFirstName = payFirstNameTxt.text;
            gLastName = payLastNameTxt.text;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: @"Update successful!"
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            
            [alert show];
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"autoSignIn"]isEqualToString:@"1"]){
                [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"autoSignIn"];
            } else {
                [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"autoSignIn"];
            }
            [userDefaults synchronize];
        } else {
            NSString *errorMsg = [apiResponseArray valueForKey:@"MSG_ERROR"];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: [NSString stringWithFormat:@"Error: %@",errorMsg]
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            
            [alert show];
            
        }
        
        
        [self apiCall:[NSString stringWithFormat:@"/get_customer/customer_id/%@/auth_token/%@",gCustomerID,gAuthToken]:@"shipping"];
    }
    activityIndicator.hidden = YES;
    
}

- (void) apiCallPost:(NSString *)URLString :(NSString *)postString :(NSString *)currentSection {
    
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    [apiResponseArray removeAllObjects];
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    CGRect bounds = activityIndicator.superview.bounds;
    activityIndicator.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    
    activityIndicator.hidden = NO;
    [activityIndicator startAnimating];
    activityIndicator.hidesWhenStopped = YES;

    
    // NSLog(@"currentSection: %@",currentSection);
    // NSLog(@"URLString: %@",URLString);
    // NSLog(@"postString: %@",postString);
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        //       NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        [request setHTTPMethod:@"POST"];
        
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:currentSection];
        });
    });
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)updateBtnPressed:(id)sender{
    
    NSString *postString = @"";
    
    if ([passwordTxt.text isEqualToString:@""]) {
    
//        postString = [NSString stringWithFormat:@"first_name=%@&last_name=%@&business_name=%@&address_1=%@&address_2=%@&city=%@&state=%@&zip=%@&ship_first_name=%@&ship_last_name=%@&ship_business_name=%@&ship_address_1=%@&ship_address_2=%@&ship_city=%@&ship_state=%@&ship_zip=%@&phone_number=%@&phone_number_mobile=%@&email_address=%@&auth_token=%@&customer_id=%@", firstNameTxt.text,lastNameTxt.text,@"None",payAddress1Txt.text,payAddress2Txt.text,payCityTxt.text,payStateTxt.text,payZipTxt.text,firstNameTxt.text,lastNameTxt.text,@"None",shipAddress1Txt.text,shipAddress2Txt.text,shipCityTxt.text,shipStateTxt.text,shipZipTxt.text,homePhoneTxt.text,mobilePhoneTxt.text,emailTxt.text,gAuthToken,gCustomerID];
        postString = [NSString stringWithFormat:@"first_name=%@&last_name=%@&business_name=%@&address_1=%@&address_2=%@&city=%@&state=%@&zip=%@&ship_first_name=%@&ship_last_name=%@&ship_business_name=%@&ship_address_1=%@&ship_address_2=%@&ship_city=%@&ship_state=%@&ship_zip=%@&email_address=%@&auth_token=%@&customer_id=%@", payFirstNameTxt.text,payLastNameTxt.text,@"None",payAddress1Txt.text,payAddress2Txt.text,payCityTxt.text,payStateTxt.text,payZipTxt.text,shipFirstNameTxt.text,shipLastNameTxt.text,@"None",shipAddress1Txt.text,shipAddress2Txt.text,shipCityTxt.text,shipStateTxt.text,shipZipTxt.text,emailTxt.text,gAuthToken,gCustomerID];

        
    } else {
        // update password also
        
        if ([passwordTxt.text length] < 5) {

            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: @"New password should be atleast 5 characters long"
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            
            [alert show];

            
            return;
        }
        
//        postString = [NSString stringWithFormat:@"first_name=%@&last_name=%@&business_name=%@&address_1=%@&address_2=%@&city=%@&state=%@&zip=%@&ship_first_name=%@&ship_last_name=%@&ship_business_name=%@&ship_address_1=%@&ship_address_2=%@&ship_city=%@&ship_state=%@&ship_zip=%@&phone_number=%@&phone_number_mobile=%@&email_address=%@&auth_token=%@&customer_id=%@&password=%@&password_confirm=%@", firstNameTxt.text,lastNameTxt.text,@"None",payAddress1Txt.text,payAddress2Txt.text,payCityTxt.text,payStateTxt.text,payZipTxt.text,firstNameTxt.text,lastNameTxt.text,@"None",shipAddress1Txt.text,shipAddress2Txt.text,shipCityTxt.text,shipStateTxt.text,shipZipTxt.text,homePhoneTxt.text,mobilePhoneTxt.text,emailTxt.text,gAuthToken,gCustomerID,passwordTxt.text,passwordTxt.text];
        postString = [NSString stringWithFormat:@"first_name=%@&last_name=%@&business_name=%@&address_1=%@&address_2=%@&city=%@&state=%@&zip=%@&ship_first_name=%@&ship_last_name=%@&ship_business_name=%@&ship_address_1=%@&ship_address_2=%@&ship_city=%@&ship_state=%@&ship_zip=%@&email_address=%@&auth_token=%@&customer_id=%@&password=%@&password_confirm=%@", payFirstNameTxt.text,payLastNameTxt.text,@"None",payAddress1Txt.text,payAddress2Txt.text,payCityTxt.text,payStateTxt.text,payZipTxt.text,shipFirstNameTxt.text,shipLastNameTxt.text,@"None",shipAddress1Txt.text,shipAddress2Txt.text,shipCityTxt.text,shipStateTxt.text,shipZipTxt.text,emailTxt.text,gAuthToken,gCustomerID,passwordTxt.text,confirmPasswordTxt.text];
    }
    
    
    [self apiCallPost:@"update_customer/" :postString :@"update"];
}

- (IBAction)autoSignInBtnPressed:(id)sender {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"autoSignIn"]isEqualToString:@"1"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"autoSignIn"];
        [self.autoSignInButton setImage:[UIImage imageNamed:@"signIn_auto_uncheck.png"] forState:UIControlStateNormal];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"autoSignIn"];
        [self.autoSignInButton setImage:[UIImage imageNamed:@"signIn_auto_check.png"] forState:UIControlStateNormal];
    }
}


@end
