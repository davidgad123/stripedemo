//
//  GrillMasterSubViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GrillMasterSubViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnGrillDetail;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (nonatomic,retain)IBOutlet UITableView *myTableView;

@property (nonatomic, retain) IBOutlet UINavigationBar *mNavigationBar;

@property (nonatomic, strong) NSArray *articles;
@property (nonatomic, strong) NSString *categoryName;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)btnGrillSecretsPressed:(id)sender;

@end
