//
//  AccountViewController.h
//  PropaneTaxi
//
//  Created by Jake on 4/7/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftViewController.h"

@interface AccountViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;


@property (strong, nonatomic) IBOutlet CustomUITextField *emailTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *passwordTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *confirmPasswordTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *payFirstNameTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *payLastNameTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *payAddress1Txt;
@property (strong, nonatomic) IBOutlet CustomUITextField *payAddress2Txt;
@property (strong, nonatomic) IBOutlet CustomUITextField *payCityTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *payStateTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *payZipTxt;

@property (strong, nonatomic) IBOutlet CustomUITextField *shipFirstNameTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *shipLastNameTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *shipAddress1Txt;
@property (strong, nonatomic) IBOutlet CustomUITextField *shipAddress2Txt;
@property (strong, nonatomic) IBOutlet CustomUITextField *shipCityTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *shipStateTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *shipZipTxt;

@property (strong, nonatomic) IBOutlet UILabel *lblAccount;


@property (strong, nonatomic) IBOutlet UILabel *emailTitle;
@property (strong, nonatomic) IBOutlet UILabel *passwordTitle;
@property (strong, nonatomic) IBOutlet UILabel *confirmPasswordTitle;

@property (strong, nonatomic) IBOutlet UILabel *payFirstNameTitle;
@property (strong, nonatomic) IBOutlet UILabel *payLastNameTitle;
@property (strong, nonatomic) IBOutlet UILabel *payAddress1Title;
@property (strong, nonatomic) IBOutlet UILabel *paypAddress2Title;
@property (strong, nonatomic) IBOutlet UILabel *payCityTitle;
@property (strong, nonatomic) IBOutlet UILabel *payStateTitle;
@property (strong, nonatomic) IBOutlet UILabel *payZipTitle;

@property (strong, nonatomic) IBOutlet UILabel *shipFirstNameTitle;
@property (strong, nonatomic) IBOutlet UILabel *shipLastNameTitle;
@property (strong, nonatomic) IBOutlet UILabel *shipAddress1Title;
@property (strong, nonatomic) IBOutlet UILabel *shipAddress2Title;
@property (strong, nonatomic) IBOutlet UILabel *shipCityTitle;
@property (strong, nonatomic) IBOutlet UILabel *shipStateTitle;
@property (strong, nonatomic) IBOutlet UILabel *shipZipTitle;



@property (nonatomic,strong) IBOutlet UIPickerView *pickerStateShip;
@property (nonatomic,strong) IBOutlet UIPickerView *pickerStatePay;

@property (weak, nonatomic) IBOutlet UIImageView *generalImageView;
@property (weak, nonatomic) IBOutlet UIImageView *shipImageView;
@property (weak, nonatomic) IBOutlet UIImageView *payImageView;

@property (weak, nonatomic) IBOutlet UIButton *autoSignInButton;

//- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;
- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)updateBtnPressed:(id)sender;
- (IBAction)autoSignInBtnPressed:(id)sender;

@end
