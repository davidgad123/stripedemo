//
//  TimerViewController.h
//  PropaneTaxi
//
//  Created by Jake on 4/27/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimerViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (strong, nonatomic) IBOutlet UILabel *lblH1;
@property (strong, nonatomic) IBOutlet UILabel *lblH2;
@property (strong, nonatomic) IBOutlet UILabel *lblM1;
@property (strong, nonatomic) IBOutlet UILabel *lblM2;
@property (strong, nonatomic) IBOutlet UILabel *lblS1;
@property (strong, nonatomic) IBOutlet UILabel *lblS2;

@property (strong, nonatomic) IBOutlet UIImageView *imgOver;

@property (strong, nonatomic) IBOutlet CustomUITextField *hoursTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *minTxt;

@property (weak, nonatomic) IBOutlet UILabel *countdownLabel;


- (IBAction)homeBtnPressed:(id)sender;

- (IBAction)navBtnPressed:(id)sender;
- (IBAction)startBtnPressed:(id)sender;



@end
