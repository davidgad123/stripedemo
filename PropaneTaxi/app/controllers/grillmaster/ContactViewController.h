//
//  ContactViewController.h
//  PropaneTaxi
//
//  Created by Jake on 4/7/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftViewController.h"

@interface ContactViewController : UIViewController

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;


//- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;
- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)callBtnPressed:(id)sender;
- (IBAction)emailBtnPressed:(id)sender;

@end
