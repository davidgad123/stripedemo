//
//  GrillMasterSubViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "GrillMasterSubViewController.h"
#import "GrillMasterDetailViewController.h"
#import "PTArticle.h"
#import "CustomTableViewCell.h"

@interface GrillMasterSubViewController ()

@end

@implementation GrillMasterSubViewController

@synthesize btnHome;
@synthesize btnNav;
@synthesize myScrollView;
@synthesize btnGrillDetail;

@synthesize lblHeader,myTableView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
//    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
//    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    lblHeader.text = self.categoryName.uppercaseString;
    lblHeader.font = [UIFont fontWithName:@"Open Sans" size:10.0];
    
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

//- (IBAction)back {
//    // TODO
//}
//
- (IBAction)backBtnPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.articles.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    CustomTableViewCell *cell = [myTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] ;
    }
    
    PTArticle *article = [self.articles objectAtIndex:indexPath.row];
    cell.textLabel.text = article.title;
//    cell.textLabel.font = [UIFont fontWithName:@"HelveticanNeue-Bold" size:cell.textLabel.font.pointSize];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticanNeue-Bold" size:13];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GrillMasterDetailViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GrillMasterDetailViewController"];
    PTArticle *article = [self.articles objectAtIndex:indexPath.row];
    vc.article = article;
    vc.categoryName = self.categoryName;
    
//    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnGrillSecretsPressed:(id)sender{
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GrillMasterDetailViewController"];
//    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    [self presentViewController:vc animated:YES completion:nil];
    
}

@end
