//
//  TimerViewController.m
//  PropaneTaxi
//
//  Created by Jake on 4/27/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import "TimerViewController.h"
#import "MZTimerLabel.h"

@interface TimerViewController () <MZTimerLabelDelegate>
@property (nonatomic, strong) MZTimerLabel *timerLabel;
@end

@implementation TimerViewController

@synthesize lblH1;
@synthesize lblH2;
@synthesize lblM1;
@synthesize lblM2;
@synthesize lblS1;
@synthesize lblS2;


@synthesize btnHome;
@synthesize btnNav;
@synthesize myScrollView;
@synthesize lblHeader,hoursTxt,minTxt, imgOver, btnStart;

int hours = 0;
int min = 0;

int countH, countM, countS;

NSTimer *timer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if (!self.timerLabel) {
        self.timerLabel = [[MZTimerLabel alloc] initWithLabel:self.countdownLabel andTimerType:MZTimerLabelTypeTimer];
        self.timerLabel.delegate = self;
    }
}

- (NSString*)timerLabel:(MZTimerLabel *)timerLabel customTextToDisplayAtTime:(NSTimeInterval)time
{
    if([timerLabel isEqual:self.timerLabel]){
        int second = (int)time  % 60;
        int minute = ((int)time / 60) % 60;
        int hours = time / 3600;
        
        lblS1.text = [NSString stringWithFormat:@"%d",second/10];
        lblS2.text = [NSString stringWithFormat:@"%d",second%10];
        
        lblM1.text = [NSString stringWithFormat:@"%d",minute/10];
        lblM2.text = [NSString stringWithFormat:@"%d",minute%10];
        
        lblH1.text = [NSString stringWithFormat:@"%d",hours/10];
        lblH2.text = [NSString stringWithFormat:@"%d",hours%10];
        
        return [NSString stringWithFormat:@"%02dh %02dm %02ds",hours,minute,second];
    }    else {
        return nil;
    }
}

- (BOOL)textFieldShouldReturn:(CustomUITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(void)cancelNumberPad{
    [hoursTxt resignFirstResponder];
    [minTxt resignFirstResponder];
//    hoursTxt.text = @"0";
//    minTxt.text = @"0";
//    
//    hours = 0;
//    min = 0;
}

-(void)doneWithNumberPad{

    
    if ([hoursTxt.text isEqualToString:@""]) {
            hours = 0;
    }

    if ([minTxt.text isEqualToString:@""]) {
            min = 0;
    }

    
    hours = [hoursTxt.text intValue];
    min = [minTxt.text intValue];
    
    [hoursTxt resignFirstResponder];
    [minTxt resignFirstResponder];
    
    
    if (hours > 99) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"Hours should be less than 100"
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        [alert show];

        return;
    }

    if (min > 59) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"Min should be less than 60"
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        [alert show];
        
        return;
    }

    /*
    if (hours > 0) {
    
        
        if (hours > 9) {
            
            lblH1.text = [hoursTxt.text substringToIndex:1];
            lblH2.text = [hoursTxt.text substringFromIndex:1];

        } else {

            lblH1.text = @"0";
            lblH2.text = [hoursTxt.text substringToIndex:1];
        }
    
    }
    if (min > 0) {
    
        if (min > 9) {
            
            lblM1.text = [minTxt.text substringToIndex:1];
            lblM2.text = [minTxt.text substringFromIndex:1];
            
        } else {
            
            lblM1.text = @"0";
            lblM2.text = [minTxt.text substringToIndex:1];
        }
    }
    
    lblS1.text = @"0";
    lblS2.text = @"0";
     */
}

- (void)textFieldDidBeginEditing:(CustomUITextField *)textField {
    
    [timer invalidate];
    
    self.view.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y-220, self.view.frame.size.width,self.view.frame.size.height);
}

- (void)textFieldDidEndEditing:(CustomUITextField *)textField {
    
    
    self.view.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y+220, self.view.frame.size.width,self.view.frame.size.height);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (IBAction)startBtnPressed:(id)sender{
    if (self.timerLabel.counting) {
        [self.timerLabel reset];
    }
    
    NSNumberFormatter *number = [[NSNumberFormatter alloc] init];
    NSNumber *hrs = [number numberFromString:hoursTxt.text];
    NSNumber *minutes = [number numberFromString:minTxt.text];
    NSTimeInterval countdownTime = (hrs.integerValue * 60 * 60) + (minutes.integerValue * 60);
    [self.timerLabel setCountDownTime:countdownTime];
    [self.timerLabel start];
    
    if((hours == 0) && (min == 0)) {

        return;
    }
    
    countS = 60;
    
    if (min > 0) {
        min--;
    } else {
        hours--;
        min = 59;
    }

    imgOver.hidden = YES;
    /*
    timer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                             target: self
                                           selector:@selector(secondsTick:)
                                           userInfo: nil repeats:YES];
*/


}

- (IBAction)secondsTick:(id)sender {
    
    if((countS == 0) && (hours == 0) && (min == 0)) {
        [timer invalidate];
        imgOver.hidden = NO;
        return;
    }

    countS --;

    if (countS == 0) {
        
        if (min > 0) {
            min--;
            countS = 60;
        } else if (hours > 0) {
            hours--;
            countS = 60;
            min = 60;
        } else {
            [timer invalidate];
            imgOver.hidden = NO;
   
        }
        
        

    }
    /*
    if (countS > 9) {
        
        NSString *secondsString = [NSString stringWithFormat:@"%d",countS];
        
        lblS1.text = [secondsString substringToIndex:1];
        lblS2.text = [secondsString substringFromIndex:1];
        
    } else {
        
        lblS1.text = @"0";
        lblS2.text = [NSString stringWithFormat:@"%d",countS];
    }
    
    
    if (hours > 0) {
        
        
        
        if (hours > 9) {
            
            NSString *hoursString = [NSString stringWithFormat:@"%d",hours];
            
            lblH1.text = [hoursString substringToIndex:1];
            lblH2.text = [hoursString substringFromIndex:1];
            
        } else {
            
            lblH1.text = @"0";
            lblH2.text = [NSString stringWithFormat:@"%d",hours];
        }
        
    } else {
        lblH1.text = @"0";
        lblH2.text = @"0";

    }
*/
    if (min > 0) {
        
        if (min > 9) {
            
            NSString *minString = [NSString stringWithFormat:@"%d",min];
            
            lblM1.text = [minString substringToIndex:1];
            lblM2.text = [minString substringFromIndex:1];
            
        } else {
            
            lblM1.text = @"0";
            lblM2.text = [NSString stringWithFormat:@"%d",min];
        }
    } else {
        lblM1.text = @"0";
        lblM2.text = @"0";

    }
    
}




- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    
    countH = 0; countM = 0; countS = 0;
    
    
    

	
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    hoursTxt.inputAccessoryView = numberToolbar;
    minTxt.inputAccessoryView = numberToolbar;
    
    
    lblH1.text = @"0";
    lblH2.text = @"0";
    lblM1.text = @"0";
    lblM2.text = @"0";
    lblS1.text = @"0";
    lblS2.text = @"0";
    
    imgOver.hidden = YES;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }
    
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

@end
