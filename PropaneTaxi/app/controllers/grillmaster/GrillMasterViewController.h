//
//  GrillMasterViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/5/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftViewController.h"

@class PTCategoryCollection,PTArticleCollection;

@interface GrillMasterViewController : UIViewController

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (strong,nonatomic) IBOutlet UILabel *lblLoggedInAs;
@property (strong,nonatomic) IBOutlet UILabel *lblZip;

@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@property (weak, nonatomic) IBOutlet UIButton *btnOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnChangeZip;

@property (weak, nonatomic) IBOutlet UIButton *btnGrillTimer;
@property (weak, nonatomic) IBOutlet UILabel *labelGrillTimer;
@property (weak, nonatomic) IBOutlet UIButton *btnMyAccount;
@property (weak, nonatomic) IBOutlet UILabel *labelMyAccount;
@property (weak, nonatomic) IBOutlet UIButton *btnContact;
@property (weak, nonatomic) IBOutlet UILabel *labelContact;

@property (weak, nonatomic) IBOutlet UIButton *btnSafety;
@property (weak, nonatomic) IBOutlet UILabel *labelSafety;
@property (weak, nonatomic) IBOutlet UIButton *btnMaintenance;
@property (weak, nonatomic) IBOutlet UILabel *labelMaintenance;
@property (weak, nonatomic) IBOutlet UIButton *btnTroubleShooting;
@property (weak, nonatomic) IBOutlet UILabel *labelTroubleShooting;

@property (weak, nonatomic) IBOutlet UIButton *btnSpecials;
@property (weak, nonatomic) IBOutlet UILabel *labelSpecials;
@property (weak, nonatomic) IBOutlet UIButton *btnGrillSecrets;
@property (weak, nonatomic) IBOutlet UILabel *labelGrillSecrets;
@property (weak, nonatomic) IBOutlet UIButton *btnRecipes;
@property (weak, nonatomic) IBOutlet UILabel *labelRecipes;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

@property (nonatomic, strong) PTArticleCollection *articleCollection;
@property (nonatomic, strong) PTCategoryCollection *categoryCollection;


- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;

- (IBAction)btnOrderPressed:(id)sender;
- (IBAction)btnChangeZipPressed:(id)sender;

- (IBAction)btnSpecialsPressed:(id)sender;
- (IBAction)btnRecipesPressed:(id)sender;
- (IBAction)btnGrillTimerPressed:(id)sender;

- (IBAction)btnSafetyPressed:(id)sender;
- (IBAction)btnMaintenancePressed:(id)sender;
- (IBAction)btnTroubleShootingPressed:(id)sender;

- (IBAction)btnMyAccountPressed:(id)sender;
- (IBAction)btnGrillSecretsPressed:(id)sender;
- (IBAction)btnContactPressed:(id)sender;

@end
