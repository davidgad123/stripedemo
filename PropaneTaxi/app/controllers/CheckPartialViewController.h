//
//  CheckPartialViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckPartialViewController : UIViewController<UIAlertViewDelegate>

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnNewAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnClickToCall;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;


@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)clickGotoWebsite:(id)sender;
- (IBAction)clickEnterNewCode:(id)sender;
- (IBAction)clickIamCovered:(id)sender;

@end
