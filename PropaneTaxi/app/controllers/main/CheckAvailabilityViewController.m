//
//  CheckAvailabilityViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "CheckAvailabilityViewController.h"

@interface CheckAvailabilityViewController ()

@end

@implementation CheckAvailabilityViewController

@synthesize navBar;
@synthesize btnHome;
@synthesize btnNav;
@synthesize btnCheck;
@synthesize btnCheckNo;
@synthesize btnCheckPartial;
@synthesize activityIndicator;

@synthesize zipCodeTxt;
@synthesize zipCodeTitle;

@synthesize lblHeader;
@synthesize myScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

//    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
//    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    myScrollView.frame = CGRectMake(0, 84, 320, 480);
    myScrollView.contentSize = CGSizeMake(320, 560);

    myScrollView.backgroundColor =  [UIColor colorWithRed:(36/255.0) green:(145/255.0) blue:(223/255.0) alpha:1];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
//    self.view.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y-150, self.view.frame.size.width,self.view.frame.size.height);
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
//    self.view.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y+150, self.view.frame.size.width,self.view.frame.size.height);
}

- (IBAction)btnTestPressed:(id)sender{
    
    /* NSLog(@"\ngAPIString = %@,\ngIsLoggedIn = %d,\ngCustomerID = %@,\ngAuthToken = %@,\ngCardID = %@,\ngCartToken = %@,\ngServiceAreaID = %@,\ngLocationLKPID = %@,\ngShippingPrice = %@,\ngProductPrice = %@,\ngQuantity = %@,\ngShippingDate = %@,\ngPickupLocation = %@,\ngConnectionService = %@,\ngProductID = %@,\ngProductName = %@,\ngProductID_conn = %@,\ngProductName_conn = %@,\ngProductPrice_conn = %@,\ngFirstName = %@,\ngLastName = %@,\ngEmail = %@,\ngPassword = %@,\ngMobilePhone = %@,\ngHomePhone = %@,\ngShipAddress1 = %@,\ngShipAddress2 = %@,\ngShipCity = %@,\ngShipState = %@,\ngShipZipCode = %@,\ngIsAddressSameAsShipping = %d,\ngCardHolderName = %@,\ngCardType = %@,\ngCardNumber = %@,\ngCVV = %@,\ngExpirationMonth = %@,\ngDeliveryNotes = %@,\ngPromoCode = %@,\ngPayAddress1 = %@,\ngPayAddress2 = %@,\ngPayCity = %@,\ngPayState = %@,\ngPayZipCode =  %@"
     ,gAPIString,gIsLoggedIn,gCustomerID,gAuthToken,gCardID,gCartToken,gServiceAreaID,gLocationLKPID,gShippingPrice,gProductPrice,gQuantity,gShippingDate,gPickupLocation,gConnectionService,gProductID,gProductName,gProductID_conn,gProductName_conn,gProductPrice_conn,gFirstName,gLastName,gEmail,gPassword,gMobilePhone,gHomePhone,gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode,gIsAddressSameAsShipping,gCardHolderName,gCardType,gCardNumber,gCVV,gExpirationMonth,gDeliveryNotes,gPromoCode,gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode);
     */
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (IBAction)backBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [viewControllersArray addObject:@"CheckAvailabilityViewController"];
    
    UIButton *btnTest = [[UIButton alloc]initWithFrame:CGRectMake(250, 25, 50,20)];
    [btnTest setTitle:@"Test" forState:UIControlStateNormal];
    [btnTest addTarget:self action:@selector(btnTestPressed:)forControlEvents:UIControlEventTouchUpInside];
// [self.view addSubview:btnTest];
    
    // Do any additional setup after loading the view.
    
    [self.fieldsView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"check_fieldBlueBack.png"]]];
    
    zipCodeTitle.frame = CGRectMake(10, 5, 270, 20);
    zipCodeTitle.textColor = [UIColor whiteColor];
    zipCodeTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    zipCodeTitle.text = @"Zip Code:";
    
    zipCodeTxt.text = gShipZipCode;
    
    navBar.frame = CGRectMake(0, delta+20, 320, 44);
    
    activityIndicator.hidden = YES;
    
    [apiResponseArray removeAllObjects];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [zipCodeTxt resignFirstResponder];
}

- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }
    
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) apiCall:(NSString *)URLString {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    activityIndicator.hidden = NO;
    [activityIndicator startAnimating];
    activityIndicator.hidesWhenStopped = YES;
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall];
        });
    });
}

- (void) processAPICall {
    
    NSString *msg_info = [[apiResponseArray valueForKey:@"msg_info"] substringToIndex:5];
    
    if ([msg_info isEqualToString:@"Sorry"]) {
        gShipZipCode = @"";
        // NSLog(@"here - %@",msg_info);
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckNoViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
    if ([msg_info isEqualToString:@"Furth"]) {
//        gShipZipCode = @"";
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckPartialViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
        
    }
    if ([msg_info isEqualToString:@"Congr"]) {
        
        gShipZipCode = zipCodeTxt.text;
        
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckYesViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
}

- (IBAction)btnCheckPressed:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([zipCodeTxt.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Error"
                                                       message: @"Please fill in a zipcode"
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        [alert show];
        
        return;
    }

    gShipZipCode = zipCodeTxt.text;
    
    [self apiCall:[NSString stringWithFormat:@"is_zipcode_serviced/zipcode/%@",zipCodeTxt.text]];
}

- (IBAction)btnCheckNoPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckNoViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnCheckPartialPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckPartialViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

@end
