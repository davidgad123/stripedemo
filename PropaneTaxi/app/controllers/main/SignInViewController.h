//
//  SignInViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnSignInAuto;

@property (strong, nonatomic) IBOutlet CustomUITextField *emailTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *passwordTxt;

@property (strong, nonatomic) IBOutlet UILabel *emailTitle;
@property (strong, nonatomic) IBOutlet UILabel *passwordTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblAccount;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblSignMeIn;


@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

@property (nonatomic, strong) NSString *redirectViewControllerName;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;
- (IBAction)btnSignInPressed:(id)sender;
- (IBAction)btnSignInAutoPressed:(id)sender;

@end
