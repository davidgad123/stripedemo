//
//  ViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/5/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "HomeViewController.h"
#import "MBProgressHUD.h"

@interface HomeViewController ()
@property (nonatomic, strong) MBProgressHUD *autoLoginHud;
@end

@implementation HomeViewController

@synthesize navBar;
@synthesize btnNav;

@synthesize btnSignIn;
@synthesize btnCheck;
@synthesize btnOrder;
@synthesize btnGrillMaster;

@synthesize lblHeader;

@synthesize myScrollView;

#pragma mark - ViewController Life-cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [viewControllersArray addObject:@"HomeViewController"];
    
    UIButton *btnTest = [[UIButton alloc]initWithFrame:CGRectMake(250, 25, 50,20)];
    [btnTest setTitle:@"Test" forState:UIControlStateNormal];
    [btnTest addTarget:self action:@selector(btnTestPressed:)forControlEvents:UIControlEventTouchUpInside];
    // [self.view addSubview:btnTest];
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
    } else {
        delta = -20;
    }
    
    navBar.frame = CGRectMake(0, delta+20, 320, 44);
    btnNav.frame = CGRectMake(20, delta+27, 30, 30);
    
    myScrollView.backgroundColor =  [UIColor colorWithRed:(36/255.0) green:(145/255.0) blue:(223/255.0) alpha:1];
    
    if (SH > 500) {
        
    } else {
        myScrollView.contentSize = CGSizeMake(320,380);
        myScrollView.frame = CGRectMake(0, 213, 320, 355);
    }
    
    
    self.autoLoginHud = [[MBProgressHUD alloc] initWithView:self.view];
    self.autoLoginHud.labelText = @"Logging In";
    [self.view addSubview:self.autoLoginHud];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
//    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
//    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    [self performSelector:@selector(handlerAutoLogin) withObject:nil afterDelay:0.5];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handlerAutoLogin
{
    BOOL isFirstLogin = [userDefaults boolForKey:@"firstLogin"];
    if (!gIsLoggedIn && isFirstLogin && [[[NSUserDefaults standardUserDefaults] objectForKey:@"autoSignIn"]isEqualToString:@"1"]){
        [self.autoLoginHud show:YES];
        [self apiCall:[NSString stringWithFormat:@"/login/account_email/%@/account_password/%@",[userDefaults objectForKey:@"customerEmail"], [userDefaults objectForKey:@"customerPassword"]] currentStep:@"login" withCartItem:nil];
        [userDefaults setBool:NO forKey:@"firstLogin"];
    }
}

- (void) apiCall:(NSString *)URLString currentStep:(NSString*)currentStep withCartItem:(PTShoppingItem*)cartItem {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        [self.autoLoginHud hide:YES];

        return;
    }
    
    [apiResponseArray removeAllObjects];
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        if (response) {
            apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                               options:NSJSONReadingMutableContainers error:&jsonParsingError];
        }
        
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:currentStep withCartItem:cartItem];
        });
    });
}

- (void)apiCallPost:(NSString *)URLString postString:(NSString *)postString currentSection:(NSString *)currentSection withCartItem:(PTShoppingItem*)shoppingItem
{
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    [apiResponseArray removeAllObjects];
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    // NSLog(@"currentSection: %@",currentSection);
    // NSLog(@"URLString: %@",URLString);
    // NSLog(@"postString: %@",postString);
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        //       NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        [request setHTTPMethod:@"POST"];
        
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:currentSection withCartItem:shoppingItem];
        });
    });
}

- (void) processAPICall:(NSString*)currentStep withCartItem:(PTShoppingItem*)cartItem {
    
    // NSLog(@"apiResponseArray: %@",apiResponseArray);
    
    if ([currentStep isEqualToString:@"login"]) {
        NSString *error = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"error"];
        // NSLog(@"error: %@",error);
        
        if ([error intValue] == 0) {
            gCustomerID = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"customer_id"];
            gCartToken = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"cartToken"];
            gServiceAreaID = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"service_area_id"];
            gShipZipCode = [[[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"ship_zip"] stringValue];
            gFirstName = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"first_name"];
            gLastName = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"last_name"];
            gEmail = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"email"];
            
            gAuthToken = [[apiResponseArray valueForKey:@"auth_token"]valueForKey:@"auth_token"];
            
            gIsLoggedIn = 1;
            
            // NSLog(@"%@,%@,%@,%@,%@",gCustomerID,gCartToken,gShipZipCode,gServiceAreaID,gAuthToken);
            
            // if we added items to our cart before we logged in, we want to add them in
            /*
            WEAKSELF
            PTShoppingCart *cart = [PTShoppingCart sharedCart];
            if (cart.items.count > 0) {
                [cart.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    PTShoppingItem *cartItem = [obj isKindOfClass:[PTShoppingItem class]] ? obj : nil;
                    if (cartItem) {
                        NSNumber *productId = cartItem.itemType == PTShoppingItemTypeExchange ? @1 : @2;
                        NSString *postString = [NSString stringWithFormat:@"customer_id=%@&cartToken=%@&product_id=%@&quantity=%@&price=%@&shipping_day=%@&location_id=%@&delivery_notes=%@",gCustomerID,gCartToken,productId,cartItem.quantity,cartItem.pricePerItem,cartItem.shippingDate,cartItem.pickupLocationId,gDeliveryNotes];
                        [weakSelf apiCallPost:@"add_product/" postString:postString currentSection:@"addProduct" withCartItem:cartItem];
                    }
                }];
                
            } else {
                [self processAPICall:@"getShoppingCart" withCartItem:nil];
            }
             */
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            if (![defaults objectForKey:@"firstRun"])
                [defaults setObject:[NSDate date] forKey:@"firstRun"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
            [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
            [self.autoLoginHud hide:YES];
        } else {
            [self.autoLoginHud hide:YES];
            // show alert box for error
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: @"Invalid username/password."
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            
            [alert show];
            
            //    emailTxt.text = @"";
//            passwordTxt.text = @"";
        }
    }
}

- (IBAction)btnTestPressed:(id)sender{
    
    /* NSLog(@"\ngAPIString = %@,\ngIsLoggedIn = %d,\ngCustomerID = %@,\ngAuthToken = %@,\ngCardID = %@,\ngCartToken = %@,\ngServiceAreaID = %@,\ngLocationLKPID = %@,\ngShippingPrice = %@,\ngProductPrice = %@,\ngQuantity = %@,\ngShippingDate = %@,\ngPickupLocation = %@,\ngConnectionService = %@,\ngProductID = %@,\ngProductName = %@,\ngProductID_conn = %@,\ngProductName_conn = %@,\ngProductPrice_conn = %@,\ngFirstName = %@,\ngLastName = %@,\ngEmail = %@,\ngPassword = %@,\ngMobilePhone = %@,\ngHomePhone = %@,\ngShipAddress1 = %@,\ngShipAddress2 = %@,\ngShipCity = %@,\ngShipState = %@,\ngShipZipCode = %@,\ngIsAddressSameAsShipping = %d,\ngCardHolderName = %@,\ngCardType = %@,\ngCardNumber = %@,\ngCVV = %@,\ngExpirationMonth = %@,\ngDeliveryNotes = %@,\ngPromoCode = %@,\ngPayAddress1 = %@,\ngPayAddress2 = %@,\ngPayCity = %@,\ngPayState = %@,\ngPayZipCode =  %@"
          ,gAPIString,gIsLoggedIn,gCustomerID,gAuthToken,gCardID,gCartToken,gServiceAreaID,gLocationLKPID,gShippingPrice,gProductPrice,gQuantity,gShippingDate,gPickupLocation,gConnectionService,gProductID,gProductName,gProductID_conn,gProductName_conn,gProductPrice_conn,gFirstName,gLastName,gEmail,gPassword,gMobilePhone,gHomePhone,gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode,gIsAddressSameAsShipping,gCardHolderName,gCardType,gCardNumber,gCVV,gExpirationMonth,gDeliveryNotes,gPromoCode,gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode);
     */
}

- (NSString *)extractString:(NSString *)fullString toLookFor:(NSString *)lookFor skipForwardX:(NSInteger)skipForward toStopBefore:(NSString *)stopBefore
{
    
    NSRange firstRange = [fullString rangeOfString:lookFor];
    NSRange secondRange = [[fullString substringFromIndex:firstRange.location + skipForward] rangeOfString:stopBefore];
    NSRange finalRange = NSMakeRange(firstRange.location + skipForward, secondRange.location + [stopBefore length]);
    
    return [fullString substringWithRange:finalRange];
}

- (UIViewController *)backViewController
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
        return nil;
    else
        return [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
}

#pragma mark - IBAction
- (IBAction)btnSignInPressed:(id)sender{

    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignInViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnCheckPressed:(id)sender{

    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckAvailabilityViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnOrderPressed:(id)sender{

    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderViewController"];
    
    if ([gShipZipCode isEqualToString:@""]) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckAvailabilityViewController"];
    }
    
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    
}

- (IBAction)btnGrillMasterPressed:(id)sender{

    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GrillMasterViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnLearnMorePressed:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @""
                                                   message: @"Buy more and save. For each additional tank after the first one you purchase in a single order, you'll save $2 per tank automatically when you add them to your cart."
                                                  delegate: self
                                         cancelButtonTitle:@"Ok"
                                         otherButtonTitles:nil];
    [alert show];
    
    return;
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}
@end
