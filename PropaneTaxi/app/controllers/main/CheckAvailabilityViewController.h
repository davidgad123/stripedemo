//
//  CheckAvailabilityViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckAvailabilityViewController : UIViewController

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;

@property (nonatomic,retain)IBOutlet UIView *fieldsView;
@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;


@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckNo;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckPartial;

@property (strong, nonatomic) IBOutlet CustomUITextField *zipCodeTxt;

@property (strong, nonatomic) IBOutlet UILabel *zipCodeTitle;


@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;
- (IBAction)btnCheckPressed:(id)sender;
- (IBAction)btnCheckNoPressed:(id)sender;
- (IBAction)btnCheckPartialPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;

@end
