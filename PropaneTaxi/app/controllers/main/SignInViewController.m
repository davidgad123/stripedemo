//
//  SignInViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "SignInViewController.h"
#import "MBProgressHUD.h"

@interface SignInViewController ()

@end

@implementation SignInViewController

@synthesize btnHome;
@synthesize btnNav;
@synthesize myScrollView;
@synthesize btnSignIn;
@synthesize btnSignInAuto;
@synthesize emailTxt;
@synthesize passwordTxt;
@synthesize activityIndicator;

@synthesize emailTitle;
@synthesize passwordTitle;
@synthesize lblAccount;
@synthesize lblHeader;
@synthesize lblSignMeIn;


#pragma mark - ViewController Life-Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [viewControllersArray addObject:@"SignInViewController"];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];
    
    UIButton *btnTest = [[UIButton alloc]initWithFrame:CGRectMake(250, 25, 50,20)];
    [btnTest setTitle:@"Test" forState:UIControlStateNormal];
    [btnTest addTarget:self action:@selector(btnTestPressed:)forControlEvents:UIControlEventTouchUpInside];
    // [self.view addSubview:btnTest];
    
    activityIndicator.hidden = YES;
    
    lblHeader.frame = CGRectMake(10, 0, 270, 20);
    lblSignMeIn.frame = CGRectMake(10, 0, 270, 20);
    
    emailTitle.textColor = customBlueColor;
    passwordTitle.textColor = customBlueColor;
    
    lblAccount.textColor = [UIColor blackColor];
    lblHeader.textColor = [UIColor blueColor];
    lblSignMeIn.textColor = [UIColor blueColor];
    
    emailTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    passwordTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    
    lblAccount.font = [UIFont fontWithName:@"HelveticaNeueLTCom-Blk" size:12];
    lblAccount.textColor = [UIColor darkGrayColor];
    
    lblHeader.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    lblSignMeIn.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    
    [btnSignInAuto  setImage:[UIImage imageNamed:@"signIn_auto_check.png"] forState:UIControlStateNormal];

    // TODO for release
//    emailTxt.text = @"williamscarl@msn.com";
//    passwordTxt.text = @"propane1";
//
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
//    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
//    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"autoSignIn"]isEqualToString:@"1"]){
        emailTxt.text = [userDefaults objectForKey:@"customerEmail"];
        passwordTxt.text = [userDefaults objectForKey:@"customerPassword"];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void) apiCall:(NSString *)URLString currentStep:(NSString*)currentStep withCartItem:(PTShoppingItem*)cartItem {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    [apiResponseArray removeAllObjects];
    
    activityIndicator.hidden = NO;
    [activityIndicator startAnimating];
    activityIndicator.hidesWhenStopped = YES;
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        if (response) {
            apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                               options:NSJSONReadingMutableContainers error:&jsonParsingError];
        }
        
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:currentStep withCartItem:cartItem];
        });
    });
}

- (void)apiCallPost:(NSString *)URLString postString:(NSString *)postString currentSection:(NSString *)currentSection withCartItem:(PTShoppingItem*)shoppingItem
{
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    [apiResponseArray removeAllObjects];
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    // NSLog(@"currentSection: %@",currentSection);
    // NSLog(@"URLString: %@",URLString);
    // NSLog(@"postString: %@",postString);
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        [request setHTTPMethod:@"POST"];
        
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:currentSection withCartItem:shoppingItem];
        });
    });
}

- (void) processAPICall:(NSString*)currentStep withCartItem:(PTShoppingItem*)shoppingItem
{
    DLog(@"apiResponseArray: %@",apiResponseArray);
    
    if ([currentStep isEqualToString:@"login"]) {
        NSString *error = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"error"];
        DLog(@"error: %@",error);
        
        if (error != nil) {
            if ([error intValue] == 0) {
                gCustomerID = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"customer_id"];
                gCartToken = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"cartToken"];
                gServiceAreaID = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"service_area_id"];
                gShipZipCode = [[[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"ship_zip"] stringValue];
                gFirstName = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"first_name"];
                gLastName = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"last_name"];
                gEmail = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"email"];
                
                gAuthToken = [[apiResponseArray valueForKey:@"auth_token"]valueForKey:@"auth_token"];
                
                gIsLoggedIn = 1;
                
                DLog(@"%@,%@,%@,%@,%@",gCustomerID,gCartToken,gShipZipCode,gServiceAreaID,gAuthToken);
                
                // if we added items to our cart before we logged in, we want to add them in
                WEAKSELF
                PTShoppingCart *cart = [PTShoppingCart sharedCart];
                if (cart.items.count > 0) {
                    [cart.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        PTShoppingItem *cartItem = [obj isKindOfClass:[PTShoppingItem class]] ? obj : nil;
                        if (cartItem) {
//                            NSNumber *productId = cartItem.itemType == PTShoppingItemTypeExchange ? @1 : @2;
                            NSNumber *productId = cartItem.productID;

                            NSString *postString = [NSString stringWithFormat:@"customer_id=%@&cartToken=%@&product_id=%@&quantity=%@&price=%@&shipping_day=%@&location_id=%@&delivery_notes=%@&auth_token=%@",gCustomerID,gCartToken,productId,cartItem.quantity,cartItem.pricePerItem,cartItem.shippingDate,cartItem.pickupLocationId,gDeliveryNotes,gAuthToken];
                            [weakSelf apiCallPost:@"add_product/" postString:postString currentSection:@"addProduct" withCartItem:cartItem];
                        }
                    }];
                } else {
                    [self processAPICall:@"getShoppingCart" withCartItem:nil];
                }
                
                if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"autoSignIn"]isEqualToString:@"1"]){
                    [userDefaults setObject:gEmail forKey:@"customerEmail"];
                    [userDefaults setObject:passwordTxt.text forKey:@"customerPassword"];
                    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"autoSignIn"];
                } else {
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"autoSignIn"];
                }
                [userDefaults synchronize];
            } else {
                
                // show alert box for error
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                               message: @"Invalid username/password."
                                                              delegate: self
                                                     cancelButtonTitle:@"Ok"
                                                     otherButtonTitles:nil];
                activityIndicator.hidden = YES;
                
                [alert show];
                
                //    emailTxt.text = @"";
                passwordTxt.text = @"";
            }
        } else {
            // show alert box for error
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: @"Network Error!"
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            activityIndicator.hidden = YES;
            
            [alert show];
        }
    }
    
    if ([currentStep isEqualToString:@"addProduct"]) {
        
        // NSLog(@"addProduct - apiResponseArray: %@",apiResponseArray);
        if ([shoppingItem.connectionService isEqualToString:@"None"] ) {
            currentStep = @"addConnectionService";
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        } else {
            NSString *postString = [NSString stringWithFormat:@"customer_id=%@&cartToken=%@&product_id=%@&quantity=%@&price=%@&shipping_day=%@&location_id=%@&delivery_notes=%@&auth_token=%@",gCustomerID,gCartToken,gProductID_conn,@1,gProductPrice_conn,shoppingItem.shippingDate,shoppingItem.pickupLocationId,gDeliveryNotes,gAuthToken];
            [self apiCallPost:@"add_product/" postString:postString currentSection:@"addConnectionService" withCartItem:shoppingItem];
        }
    }
    
    if ([currentStep isEqualToString:@"addConnectionService"]) {
        // NSLog(@"addConnectionService - apiResponseArray: %@",apiResponseArray);
        [self apiCall:[NSString stringWithFormat:@"get_shopping_cart/customer_id/%@/cart_token/%@/service_area_id/%@/auth_token/%@",gCustomerID, gCartToken,gServiceAreaID,gAuthToken] currentStep:@"getShoppingCart" withCartItem:nil];
    }
    
    if ([currentStep isEqualToString:@"getShoppingCart"]) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        // NSLog(@"getShoppingCart - apiResponseArray: %@",apiResponseArray);
        [[PTShoppingCart sharedCart] emptyCart];
        NSString *viewControllerToLoad = self.redirectViewControllerName.length > 0 ? self.redirectViewControllerName : @"LoggedInHomeViewController";
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:viewControllerToLoad];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
}



- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}
- (IBAction)btnTestPressed:(id)sender{
    
    /* NSLog(@"\ngAPIString = %@,\ngIsLoggedIn = %d,\ngCustomerID = %@,\ngAuthToken = %@,\ngCardID = %@,\ngCartToken = %@,\ngServiceAreaID = %@,\ngLocationLKPID = %@,\ngShippingPrice = %@,\ngProductPrice = %@,\ngQuantity = %@,\ngShippingDate = %@,\ngPickupLocation = %@,\ngConnectionService = %@,\ngProductID = %@,\ngProductName = %@,\ngProductID_conn = %@,\ngProductName_conn = %@,\ngProductPrice_conn = %@,\ngFirstName = %@,\ngLastName = %@,\ngEmail = %@,\ngPassword = %@,\ngMobilePhone = %@,\ngHomePhone = %@,\ngShipAddress1 = %@,\ngShipAddress2 = %@,\ngShipCity = %@,\ngShipState = %@,\ngShipZipCode = %@,\ngIsAddressSameAsShipping = %d,\ngCardHolderName = %@,\ngCardType = %@,\ngCardNumber = %@,\ngCVV = %@,\ngExpirationMonth = %@,\ngDeliveryNotes = %@,\ngPromoCode = %@,\ngPayAddress1 = %@,\ngPayAddress2 = %@,\ngPayCity = %@,\ngPayState = %@,\ngPayZipCode =  %@"
          ,gAPIString,gIsLoggedIn,gCustomerID,gAuthToken,gCardID,gCartToken,gServiceAreaID,gLocationLKPID,gShippingPrice,gProductPrice,gQuantity,gShippingDate,gPickupLocation,gConnectionService,gProductID,gProductName,gProductID_conn,gProductName_conn,gProductPrice_conn,gFirstName,gLastName,gEmail,gPassword,gMobilePhone,gHomePhone,gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode,gIsAddressSameAsShipping,gCardHolderName,gCardType,gCardNumber,gCVV,gExpirationMonth,gDeliveryNotes,gPromoCode,gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode);
     */
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }

    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnSignInPressed:(id)sender {
    
    if ([emailTxt.text isEqualToString:@""] || [passwordTxt.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"Please enter a valid Username and Password"
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        activityIndicator.hidden = YES;
        [alert show];
        
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"firstRun"])
        [defaults setObject:[NSDate date] forKey:@"firstRun"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *email = [emailTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *password = [passwordTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    [self apiCall:[NSString stringWithFormat:@"/login/account_email/%@/account_password/%@",email, password] currentStep:@"login" withCartItem:nil];
    [self.view endEditing:YES];

}

- (IBAction)btnSignInAutoPressed:(id)sender {
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"autoSignIn"]isEqualToString:@"1"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"autoSignIn"];
        [btnSignInAuto  setImage:[UIImage imageNamed:@"signIn_auto_uncheck.png"] forState:UIControlStateNormal];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"autoSignIn"];
        [btnSignInAuto  setImage:[UIImage imageNamed:@"signIn_auto_check.png"] forState:UIControlStateNormal];
    }
}

@end
