//
//  LeftViewController.m
//  EasySample
//
//  Created by Marian PAUL on 12/06/12.
//  Copyright (c) 2012 Marian PAUL aka ipodishima — iPuP SARL. All rights reserved.
//

#import "LeftViewController.h"
#import "CartViewController.h"
#import "SignInViewController.h"

@interface LeftViewController ()

@end

@implementation LeftViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
        self.tableView.tableHeaderView = headerView;
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
    } else {
   //     delta = -20;
    }
    
        self.tableView.backgroundColor = [UIColor colorWithRed:(30.0/255.0) green:(118.0/255.0) blue:(187.0/255.0) alpha:(100.0/100.0)];
    
    
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    //   UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(XXX, YYY, XXX, YYY)];
    //    [headerView addSubview:imageView];
    //  UILabel *labelView = [[UILabel alloc] initWithFrame:CGRectMake(XXX, YYY, XXX, YYY)];
    // [headerView addSubview:labelView];
//    self.tableView.tableHeaderView = headerView;
//    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
   // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cellBack.png"]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 8;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // // NSLog(@"%@", menuType); 55,44
    
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    } 
   
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"BACK A PAGE";
            cell.imageView.image = [UIImage imageNamed:@"left_back.png"];
            break;
        case 1:
            cell.textLabel.text = @"HOME";
            cell.imageView.image = [UIImage imageNamed:@"left_home.png"];
            break;
        case 2:
            cell.textLabel.text = @"ORDER A TANK";
            cell.imageView.image = [UIImage imageNamed:@"left_order.png"];
            break;
        case 3:
            cell.textLabel.text = @"GRILL MASTER GUIDE";
            cell.imageView.image = [UIImage imageNamed:@"left_grill.png"];
            break;
        case 4:
            cell.textLabel.text = @"MY ACCOUNT";
            cell.imageView.image = [UIImage imageNamed:@"left_myaccount.png"];
            break;
        case 5:
            cell.textLabel.text = @"SHOPPING CART";
            cell.imageView.image = [UIImage imageNamed:@"left_cart.png"];
            break;
        case 6:
            cell.textLabel.text = @"CONTACT";
            cell.imageView.image = [UIImage imageNamed:@"left_contact.png"];
            break;
        case 7:
            cell.textLabel.text = @"LOGOUT";
            cell.imageView.image = [UIImage imageNamed:@"left_logout.png"];
            break;
    }
    
//    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellBack.png"]];
    cell.backgroundColor = [UIColor colorWithRed:(30.0/255.0) green:(118.0/255.0) blue:(187.0/255.0) alpha:(100.0/100.0)];
 //   cell.textLabel.font = [UIFont fontWithName:@"CreteRound-Regular" size:18];
    cell.textLabel.textColor = [UIColor whiteColor];

    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {

        
    } else {
        cell.textLabel.textColor = [UIColor darkGrayColor];
    }
    
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        
        // NSLog(@"%@",viewControllersArray);
        
        NSString *lastVC = [viewControllersArray objectAtIndex:viewControllersArray.count-2];
        
        // NSLog(@"lastVC: %@",lastVC);

        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:lastVC];
        
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
        
    }
    else if (indexPath.row == 1) {
        
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
        
        if (gIsLoggedIn) {
            vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
        }
        
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
    else if (indexPath.row == 2) {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderViewController"];
        
        
        if ([gShipZipCode isEqualToString:@""]) {
            vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckAvailabilityViewController"];
        }
        
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
    else if (indexPath.row == 3) {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GrillMasterViewController"];

        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
    else if (indexPath.row == 4) {

        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AccountViewController"];
        
        if (!gIsLoggedIn) {
            vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignInViewController"];
            [(SignInViewController*)vc setRedirectViewControllerName:@"AccountViewController"];
        }
        
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
        
    }
    else if (indexPath.row == 5) {
        CartViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CartViewController"];
        vc.shouldAddProduct = NO;
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
    else if (indexPath.row == 6) {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ContactViewController"];
        
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
    else if (indexPath.row == 7) {

        // logout
        
        gIsLoggedIn = NO;
        gCustomerID = @"";
        gAuthToken = @"";
        gCardID = @"";
        gCartToken = @"";
        gServiceAreaID = @"";
        gLocationLKPID = @"";
        
        // order
        gShippingPrice = @"";
        gProductPrice  = @"";
        gProductID = @"1";
        
        gQuantity = @"";
        gShippingDate = @"";
        gPickupLocation = @"";
        gProductName = @"";
        
        gProductID_conn = @"";
        gProductName_conn = @"";
        gProductPrice_conn = @"";
        
        gConnectionService = @"No";
        
        gCartID = @"";
        gCartID_conn = @"";
        
        
        // account
        
        gFirstName = @"";
        gLastName = @"";
        gEmail = @"";
        gPassword = @"";
        gMobilePhone = @"";
        gHomePhone = @"";
        gRealPassword = @"";
        
        
        // shipping
        
        gShipAddress1 = @"";
        gShipAddress2 = @"";
        gShipCity = @"";
        gShipState = @"";
        gShipZipCode = @"";
        
        // Payment
        
        gIsAddressSameAsShipping = YES;
        gCardHolderName = @"";
        gCardType = @"";
        gCardNumber = @"";
        gCVV = @"";
        gExpirationMonth = @"";
        gExpirationYear = @"";
        
        gDeliveryNotes = @"";
        gPromoCode = @"";
        
        gPayAddress1 = @"";
        gPayAddress2 = @"";
        gPayCity = @"";
        gPayState = @"";
        gPayZipCode = @"";
        
        gTotal = @"";
        gOrderID = @"";
        
        [[PTShoppingCart sharedCart] emptyCart];
        
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
    
}

@end
