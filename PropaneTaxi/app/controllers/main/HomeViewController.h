//
//  ViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/5/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UIButton *btnNav;

@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnGrillMaster;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

- (IBAction)navBtnPressed:(id)sender;

- (IBAction)btnSignInPressed:(id)sender;
- (IBAction)btnCheckPressed:(id)sender;
- (IBAction)btnOrderPressed:(id)sender;
- (IBAction)btnGrillMasterPressed:(id)sender;

- (IBAction)btnLearnMorePressed:(id)sender;

@end
