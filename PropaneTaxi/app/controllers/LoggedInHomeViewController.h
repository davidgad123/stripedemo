//
//  LoggedInHomeViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/5/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoggedInHomeViewController : UIViewController<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;

@property (strong,nonatomic) IBOutlet UILabel *lblWelcomeBack;

@property (weak, nonatomic) IBOutlet UIButton *btnOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnGrillMaster;
@property (weak, nonatomic) IBOutlet UIButton *btnMyAccount;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)btnOrderPressed:(id)sender;
- (IBAction)btnGrillMasterPressed:(id)sender;
- (IBAction)btnMyAccountPressed:(id)sender;

@end
