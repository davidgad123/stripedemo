//
//  CheckNoViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "CheckNoViewController.h"

@interface CheckNoViewController () <UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *contactInfo;
@property (weak, nonatomic) IBOutlet UIButton *keepMeUpdatedButton;
@end

@implementation CheckNoViewController

@synthesize btnHome;
@synthesize btnNav;
@synthesize myScrollView;

@synthesize emailTxt;
@synthesize emailTitle;
@synthesize lblToBeNotified;
@synthesize lblHeader;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (IBAction)btnTestPressed:(id)sender{
    
    /* NSLog(@"\ngAPIString = %@,\ngIsLoggedIn = %d,\ngCustomerID = %@,\ngAuthToken = %@,\ngCardID = %@,\ngCartToken = %@,\ngServiceAreaID = %@,\ngLocationLKPID = %@,\ngShippingPrice = %@,\ngProductPrice = %@,\ngQuantity = %@,\ngShippingDate = %@,\ngPickupLocation = %@,\ngConnectionService = %@,\ngProductID = %@,\ngProductName = %@,\ngProductID_conn = %@,\ngProductName_conn = %@,\ngProductPrice_conn = %@,\ngFirstName = %@,\ngLastName = %@,\ngEmail = %@,\ngPassword = %@,\ngMobilePhone = %@,\ngHomePhone = %@,\ngShipAddress1 = %@,\ngShipAddress2 = %@,\ngShipCity = %@,\ngShipState = %@,\ngShipZipCode = %@,\ngIsAddressSameAsShipping = %d,\ngCardHolderName = %@,\ngCardType = %@,\ngCardNumber = %@,\ngCVV = %@,\ngExpirationMonth = %@,\ngDeliveryNotes = %@,\ngPromoCode = %@,\ngPayAddress1 = %@,\ngPayAddress2 = %@,\ngPayCity = %@,\ngPayState = %@,\ngPayZipCode =  %@"
          ,gAPIString,gIsLoggedIn,gCustomerID,gAuthToken,gCardID,gCartToken,gServiceAreaID,gLocationLKPID,gShippingPrice,gProductPrice,gQuantity,gShippingDate,gPickupLocation,gConnectionService,gProductID,gProductName,gProductID_conn,gProductName_conn,gProductPrice_conn,gFirstName,gLastName,gEmail,gPassword,gMobilePhone,gHomePhone,gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode,gIsAddressSameAsShipping,gCardHolderName,gCardType,gCardNumber,gCVV,gExpirationMonth,gDeliveryNotes,gPromoCode,gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode);
     */
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (IBAction)backBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [viewControllersArray addObject:@"CheckNoViewController"];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];

    UIButton *btnTest = [[UIButton alloc]initWithFrame:CGRectMake(250, 25, 50,20)];
    [btnTest setTitle:@"Test" forState:UIControlStateNormal];
    [btnTest addTarget:self action:@selector(btnTestPressed:)forControlEvents:UIControlEventTouchUpInside];

    emailTitle.frame = CGRectMake(10, 0, 270, 20);
    lblToBeNotified.frame = CGRectMake(10, 0, 270, 20);
    lblHeader.frame = CGRectMake(10, 0, 270, 20);
    
    emailTitle.textColor = customBlueColor;
    lblToBeNotified.textColor = customBlueColor;
    lblHeader.textColor = customBlueColor;

    emailTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    lblToBeNotified.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    lblHeader.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    
	// Do any additional setup after loading the view.
    self.contactInfo.contentSize = CGSizeMake(CGRectGetWidth(self.contactInfo.frame), CGRectGetMinY(self.keepMeUpdatedButton.frame)+CGRectGetHeight(self.keepMeUpdatedButton.frame));
}

- (BOOL)textFieldShouldReturn:(CustomUITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidBeginEditing:(CustomUITextField *)textField {
    
    self.view.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y-200, self.view.frame.size.width,self.view.frame.size.height);
}

- (void)textFieldDidEndEditing:(CustomUITextField *)textField {
    
    self.view.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y+200, self.view.frame.size.width,self.view.frame.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }

    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnNewAddressPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckAvailabilityViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnKeepMeInformedPressed:(id)sender {
    
    
    if ([emailTxt.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"Please enter a valid e-mail address"
                                                      delegate: nil
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        
        [alert show];
        return;
    }
    
    if ([self.firstNameField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"Please enter a first name"
                                                      delegate: nil
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        
        [alert show];
        return;
    }
    
    if ([self.lastNameField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"Please enter a last name"
                                                      delegate: nil
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        
        [alert show];
        return;
    }
    
    NSString *postString = [NSString stringWithFormat:@"out_of_market_email/first_name/%@/last_name/%@/email_address/%@/zip/%@",self.firstNameField.text,self.lastNameField.text,emailTxt.text,gShipZipCode];
    [self apiCall:postString];
}

- (void) apiCall:(NSString *)URLString {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        NSString *responseString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        DLog(@"\nresponseString %@", responseString);

        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            if ([responseString isEqualToString:@"\"success\""]) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Thanks!"
                                                               message: @"We will keep you informed."
                                                              delegate: self
                                                     cancelButtonTitle:@"Ok"
                                                     otherButtonTitles:nil];
                
                [alert show];
                
                return;
            } else if ([responseString isEqualToString:@"\"error\""]) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"An Error Occurred!"
                                                               message: @"Sorry, but we failed to add your email to our list. Please check your first name, last name, and e-mail fields again."
                                                              delegate:nil
                                                     cancelButtonTitle:@"Ok"
                                                     otherButtonTitles:nil];
                
                [alert show];
                return;
            }
            [self processAPICall];
        });
    });
}

- (void)processAPICall {
    DLog(@"\napiResponseArray %@", apiResponseArray);
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:viewController animated:YES];
}

@end
