//
//  SignUpViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/5/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;
@property (nonatomic,retain)IBOutlet UIView *fieldsView;

@property (weak, nonatomic) IBOutlet UIButton *btnCreate;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;

@property (strong,nonatomic) IBOutlet UILabel *lblCongrats;
@property (strong,nonatomic) IBOutlet UILabel *lblDescription;
@property (strong,nonatomic) IBOutlet UILabel *lblAccountCreation;


@property (weak, nonatomic) IBOutlet UIButton *btnHome;
- (IBAction)homeBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNav;
- (IBAction)navBtnPressed:(id)sender;

@end
