//
//  CheckYesViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckYesViewController : UIViewController

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *btnOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnGrillMaster;
@property (weak, nonatomic) IBOutlet UIButton *btnCreateAccount;


- (IBAction)btnOrderPressed:(id)sender;
- (IBAction)btnGrillMasterPressed:(id)sender;
- (IBAction)btnCreateAccountPressed:(id)sender;


@end
