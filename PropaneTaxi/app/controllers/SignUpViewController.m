//
//  SignUpViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/5/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController

@synthesize myScrollView,btnCreate,btnSkip;
@synthesize lblCongrats;
@synthesize lblDescription;
@synthesize lblAccountCreation;
@synthesize btnHome;
@synthesize btnNav;
@synthesize lblHeader;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}
- (IBAction)btnTestPressed:(id)sender{
    
    /* NSLog(@"\ngAPIString = %@,\ngIsLoggedIn = %d,\ngCustomerID = %@,\ngAuthToken = %@,\ngCardID = %@,\ngCartToken = %@,\ngServiceAreaID = %@,\ngLocationLKPID = %@,\ngShippingPrice = %@,\ngProductPrice = %@,\ngQuantity = %@,\ngShippingDate = %@,\ngPickupLocation = %@,\ngConnectionService = %@,\ngProductID = %@,\ngProductName = %@,\ngProductID_conn = %@,\ngProductName_conn = %@,\ngProductPrice_conn = %@,\ngFirstName = %@,\ngLastName = %@,\ngEmail = %@,\ngPassword = %@,\ngMobilePhone = %@,\ngHomePhone = %@,\ngShipAddress1 = %@,\ngShipAddress2 = %@,\ngShipCity = %@,\ngShipState = %@,\ngShipZipCode = %@,\ngIsAddressSameAsShipping = %d,\ngCardHolderName = %@,\ngCardType = %@,\ngCardNumber = %@,\ngCVV = %@,\ngExpirationMonth = %@,\ngDeliveryNotes = %@,\ngPromoCode = %@,\ngPayAddress1 = %@,\ngPayAddress2 = %@,\ngPayCity = %@,\ngPayState = %@,\ngPayZipCode =  %@"
          ,gAPIString,gIsLoggedIn,gCustomerID,gAuthToken,gCardID,gCartToken,gServiceAreaID,gLocationLKPID,gShippingPrice,gProductPrice,gQuantity,gShippingDate,gPickupLocation,gConnectionService,gProductID,gProductName,gProductID_conn,gProductName_conn,gProductPrice_conn,gFirstName,gLastName,gEmail,gPassword,gMobilePhone,gHomePhone,gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode,gIsAddressSameAsShipping,gCardHolderName,gCardType,gCardNumber,gCVV,gExpirationMonth,gDeliveryNotes,gPromoCode,gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode);
     */
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
        [viewControllersArray addObject:@"SignUpViewController"];
    
    UIButton *btnTest = [[UIButton alloc]initWithFrame:CGRectMake(250, 25, 50,20)];
    [btnTest setTitle:@"Test" forState:UIControlStateNormal];
    [btnTest addTarget:self action:@selector(btnTestPressed:)forControlEvents:UIControlEventTouchUpInside];
    // [self.view addSubview:btnTest];
    
    self.myScrollView.backgroundColor = [UIColor colorWithRed:(32.0/255.0) green:(135.0/255.0) blue:(221.0/255.0) alpha:(100.0/100.0)];
    self.fieldsView.backgroundColor = [UIColor colorWithRed:(7.0/255.0) green:(89.0/255.0) blue:(153.0/255.0) alpha:(100.0/100.0)];
    
    // scroll view
    myScrollView.frame = CGRectMake(0, 84, 320, 480);
    myScrollView.contentSize = CGSizeMake(320, 800);
    
    //top labels
    lblCongrats.frame = CGRectMake(18, 0, 285, 32);
    lblDescription.frame = CGRectMake(18, 20, 285, 32);
    lblAccountCreation.frame = CGRectMake(18, 60, 285, 32);
    
    // fields view
    self.fieldsView.frame = CGRectMake(18, 85, 285, 435);
    
    // bottom buttons
    btnCreate.frame = CGRectMake(18, 530, 285, 32);
    btnSkip.frame = CGRectMake(18, 570, 285, 32);
    

    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }

    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}
@end
