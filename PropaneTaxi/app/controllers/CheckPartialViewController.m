//
//  CheckPartialViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "CheckPartialViewController.h"

@interface CheckPartialViewController ()

@end

@implementation CheckPartialViewController

@synthesize btnHome;
@synthesize btnNav;
@synthesize myScrollView;

@synthesize btnNewAddress;
@synthesize btnClickToCall;

@synthesize lblHeader;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)btnTestPressed:(id)sender{
    
    /* NSLog(@"\ngAPIString = %@,\ngIsLoggedIn = %d,\ngCustomerID = %@,\ngAuthToken = %@,\ngCardID = %@,\ngCartToken = %@,\ngServiceAreaID = %@,\ngLocationLKPID = %@,\ngShippingPrice = %@,\ngProductPrice = %@,\ngQuantity = %@,\ngShippingDate = %@,\ngPickupLocation = %@,\ngConnectionService = %@,\ngProductID = %@,\ngProductName = %@,\ngProductID_conn = %@,\ngProductName_conn = %@,\ngProductPrice_conn = %@,\ngFirstName = %@,\ngLastName = %@,\ngEmail = %@,\ngPassword = %@,\ngMobilePhone = %@,\ngHomePhone = %@,\ngShipAddress1 = %@,\ngShipAddress2 = %@,\ngShipCity = %@,\ngShipState = %@,\ngShipZipCode = %@,\ngIsAddressSameAsShipping = %d,\ngCardHolderName = %@,\ngCardType = %@,\ngCardNumber = %@,\ngCVV = %@,\ngExpirationMonth = %@,\ngDeliveryNotes = %@,\ngPromoCode = %@,\ngPayAddress1 = %@,\ngPayAddress2 = %@,\ngPayCity = %@,\ngPayState = %@,\ngPayZipCode =  %@"
          ,gAPIString,gIsLoggedIn,gCustomerID,gAuthToken,gCardID,gCartToken,gServiceAreaID,gLocationLKPID,gShippingPrice,gProductPrice,gQuantity,gShippingDate,gPickupLocation,gConnectionService,gProductID,gProductName,gProductID_conn,gProductName_conn,gProductPrice_conn,gFirstName,gLastName,gEmail,gPassword,gMobilePhone,gHomePhone,gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode,gIsAddressSameAsShipping,gCardHolderName,gCardType,gCardNumber,gCVV,gExpirationMonth,gDeliveryNotes,gPromoCode,gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode);
     */
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
//    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
//    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (IBAction)backBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
        [viewControllersArray addObject:@"CheckPartialViewController"];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];

    
    UIButton *btnTest = [[UIButton alloc]initWithFrame:CGRectMake(250, 25, 50,20)];
    [btnTest setTitle:@"Test" forState:UIControlStateNormal];
    [btnTest addTarget:self action:@selector(btnTestPressed:)forControlEvents:UIControlEventTouchUpInside];
    // [self.view addSubview:btnTest];
	// Do any additional setup after loading the view.
    
    [self.txtDescription setBackgroundColor:[UIColor clearColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }

    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)clickGotoWebsite:(id)sender
{
    NSString *urlString = @"https://www.propanetaxi.com/account/partiallycovered?servicecheck_zipcode=";
    urlString = [NSString stringWithFormat:@"%@%@", urlString, gShipZipCode];
    urlString = [NSString stringWithFormat:@"%@%@", urlString, @"&checkfrommobile=true"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

- (IBAction)clickEnterNewCode:(id)sender
{
    gShipZipCode = @"";
    
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckAvailabilityViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)clickIamCovered:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Continue Shopping? \n If you are not in an area we currently service your order may be subject to cancellation" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        // YES
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderViewController"];
        
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];

    }
}

@end
