//
//  CheckoutPaymentViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "CheckoutPaymentViewController.h"
#import "CartViewController.h"
#import "CDatePickerViewEx.h"

@interface CheckoutPaymentViewController ()
{
    int heightScroll;
    UITextField *txtCurrentField;
    CDatePickerViewEx *datePicker;
    NSDate *selectedDate;
}
@end

@implementation CheckoutPaymentViewController

@synthesize btnHome;
@synthesize btnNav;
@synthesize myScrollView;
@synthesize btnContinue;

@synthesize cardHolderNameTxt;
@synthesize cardTypeTxt;
@synthesize cardNumberTxt;
@synthesize cvvTxt;
@synthesize expirationDateTxt;

@synthesize activityIndicator;


NSMutableArray *arrayCardType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    self.navigationLabel.font = [UIFont fontWithName:@"Open Sans" size:10.0];
    self.navigationLabel.text = @"CREATE ACCOUNT";

    myScrollView.frame = CGRectMake(0, 84, 320, 480);
    myScrollView.contentSize = CGSizeMake(320, 560);

//    myScrollView.contentSize = myScrollView.frame.size;
    
    heightScroll = myScrollView.frame.size.height;
    txtCurrentField = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set fields layout
    activityIndicator.hidden = YES;
    
    [viewControllersArray addObject:@"CheckoutPaymentViewController"];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];
    
    // arrays to hold picker data
    arrayCardType = [[NSMutableArray alloc] initWithObjects:@"Mastercard",@"Visa",@"American Express",@"Discover",nil];
    
    datePicker = [[CDatePickerViewEx alloc] init];
    datePicker.datePickerDelegate = self;
    [datePicker selectToday];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }
    
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)cardTypeBtnPressed:(id)sender {
    
    int height = [arrayCardType count] * 45 + 30;
   
    ZSYPopoverListView *listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 280, height)];
    listView.titleName.text = @"Select a type";
    listView.datasource = self;
    listView.delegate = self;
    
    [listView show];
    
    if (txtCurrentField != nil)
        [txtCurrentField resignFirstResponder];

    height = myScrollView.frame.size.height;
    if (heightScroll != height) {
        CGRect rect = myScrollView.frame;
        CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height + 200);
        myScrollView.frame = newRect;
    }
}

- (IBAction)expirationBtnPressed:(id)sender
{
    if (txtCurrentField != nil)
        [txtCurrentField resignFirstResponder];
    
    int height = myScrollView.frame.size.height;
    if (heightScroll != height) {
        CGRect rect = myScrollView.frame;
        CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height + 200);
        myScrollView.frame = newRect;
    }
    
    CGRect rect = CGRectMake(20, 150, 280, 250);
    datePicker.frame = rect;
    datePicker.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:datePicker];
}


- (IBAction)btnViewCartPressed:(id)sender{
    CartViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    vc.shouldAddProduct = NO;
}

- (IBAction)btnContinuePressed:(id)sender{
    
    //  [self.view endEditing:YES];
    
    if (([cardHolderNameTxt.text isEqual:@""]) || ([cardTypeTxt.text isEqual:@""]) || ([cardNumberTxt.text isEqual:@""])||([cvvTxt.text isEqual:@""])||([expirationDateTxt.text isEqual:@""])) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"All payment fields are required"
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return;
    }
    
    if (!(([cardTypeTxt.text isEqual:@"Mastercard"]) || ([cardTypeTxt.text isEqual:@"Visa"]) || ([cardTypeTxt.text isEqual:@"American Express"])||([cardTypeTxt.text isEqual:@"Discover"]))) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"Card Type should be: Visa, Amex, Mastercard or Discover"
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return;
    }
    
    if (![cardNumberTxt.text length] == 16) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"Card number should be 16 digits"
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    gCardHolderName  = cardHolderNameTxt.text;
    gCardType  = cardTypeTxt.text;
    gCardNumber  = cardNumberTxt.text;
    gCVV  = cvvTxt.text;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM"];
    gExpirationMonth = [dateFormatter stringFromDate:selectedDate];
    [dateFormatter setDateFormat:@"yyyy"];
    gExpirationYear = [dateFormatter stringFromDate:selectedDate];
   
    NSString *apiUrl = [NSString stringWithFormat:@"/add_credit_card/cc_type/%@/cc_number/%@/exp_month/%@/exp_year/%@/customer_id/%@/", gCardType, gCardNumber, gExpirationMonth, gExpirationYear, gCustomerID];
    [self apiCall:apiUrl :@"addCard"];
    
}

#pragma mark - Protocol Conformance

#pragma CDatePickerDelegate
-(void)changedDatePickerValue {
    
    selectedDate = datePicker.date;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM"];
    expirationDateTxt.text = [dateFormatter stringFromDate:selectedDate];
    
    [datePicker removeFromSuperview];
}

#pragma UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    int height = myScrollView.frame.size.height;
    if (heightScroll != height) {
        CGRect rect = myScrollView.frame;
        CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height + 200);
        myScrollView.frame = newRect;
    }
    
    txtCurrentField = nil;
    
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    int height = myScrollView.frame.size.height;
    
    if (heightScroll == height) {
        CGRect rect = myScrollView.frame;
        CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height - 200);
        myScrollView.frame = newRect;
    }
    
    txtCurrentField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

#pragma mark PopOverListView
- (NSInteger)popoverListView:(ZSYPopoverListView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayCardType.count;
}

- (UITableViewCell *)popoverListView:(ZSYPopoverListView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusablePopoverCellWithIdentifier:identifier];
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.textLabel.text = arrayCardType[indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.textLabel.numberOfLines = 2;
    
    return cell;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    cardTypeTxt.text = arrayCardType[indexPath.row];
    
    [tableView dismiss];
}

- (void) apiCall:(NSString *)URLString :(NSString *)requestFor  {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    [apiResponseArray removeAllObjects];
    
    CGRect bounds = activityIndicator.superview.bounds;
    activityIndicator.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    
    activityIndicator.hidden = NO;
    [activityIndicator startAnimating];
    activityIndicator.hidesWhenStopped = YES;
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:requestFor];
        });
    });
}

- (void) processAPICall:(NSString *)requestFor  {
    
    if ([requestFor isEqualToString:@"addCard"]) {
        NSLog(@"addCard - apiResponseArray: %@",apiResponseArray);
        
        if ([[apiResponseArray valueForKey:@"msg"]  isEqual: @"success"]) {
            
            // confirm order
            //success
            [self apiCall:[NSString stringWithFormat:@"/checkout_process/customer_id/%@/cart_token/%@/service_area_id/%@/auth_token/%@",gCustomerID,gCartToken,gServiceAreaID,gAuthToken]:@"confirmOrder"];

            
        } else {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: @"Please check the credit card details."
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            activityIndicator.hidden = YES;
            [alert show];
            return;
        }
    }
    
    if ([requestFor isEqualToString:@"confirmOrder"]) {
        // NSLog(@"confirmOrder - apiResponseArray: %@",apiResponseArray);
        // if good go to confirmed screen.
        
        if (![[[apiResponseArray valueForKey:@"order_id"]  stringValue] length] > 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: @"Unknown error. Please contact support."
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            [alert show];
            activityIndicator.hidden = YES;
            return;
        }
        
        if (![[apiResponseArray valueForKey:@"msg"] isEqualToString:@"error"]) {
            
            gOrderID = [[apiResponseArray valueForKey:@"order_id"] stringValue];
            gTotal = [[apiResponseArray valueForKey:@"total_amount"] stringValue];
            
            UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderConfirmViewController"];
            [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
            
        } else {
            
            NSString *errorMsg =[apiResponseArray valueForKey:@"msg_info"];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: [NSString stringWithFormat: @"Error: %@",errorMsg]
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            [alert show];
            activityIndicator.hidden = YES;
            return;
        }
    }
    
    if ([requestFor isEqualToString:@"updateBilling"]) {
        DLog(@"\nupdateBilling response %@", apiResponseArray);
    }
    
    activityIndicator.hidden = YES;
}

- (void) apiCallPost:(NSString *)URLString :(NSString *)postString :(NSString *)requestFor {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    CGRect bounds = activityIndicator.superview.bounds;
    activityIndicator.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    
    activityIndicator.hidden = NO;
    [activityIndicator startAnimating];
    activityIndicator.hidesWhenStopped = YES;
    
    [apiResponseArray removeAllObjects];
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    // NSLog(@"currentSection: %@",requestFor);
    // NSLog(@"URLString: %@",URLString);
    // NSLog(@"postString: %@",postString);
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        [request setHTTPMethod:@"POST"];
        
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:requestFor];
        });
    });
}

@end
