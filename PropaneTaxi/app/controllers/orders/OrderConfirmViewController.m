//
//  OrderConfirmViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "OrderConfirmViewController.h"

@interface OrderConfirmViewController ()

@end

@implementation OrderConfirmViewController

@synthesize btnHome;
@synthesize btnNav;
@synthesize myScrollView;

@synthesize lblOrderNumberTitle;
@synthesize lblTotalTitle;
@synthesize lblDeliveryDateTitle;
@synthesize lblItemsOrderedTitle;

@synthesize lblOrderNumber;
@synthesize lblTotal;
@synthesize lblDeliveryDate;
@synthesize lblItemsOrdered;
@synthesize lblHeader;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void) apiCall:(NSString *)URLString :(NSString *)requestFor  {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    [apiResponseArray removeAllObjects];
    
    //    activityIndicator.hidden = NO;
    //    [activityIndicator startAnimating];
    //    activityIndicator.hidesWhenStopped = YES;
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall];
        });
    });
}
- (void) processAPICall {
    
    // NSLog(@"apiResponseArray: %@",apiResponseArray);
    
    lblOrderNumber.text = [[apiResponseArray valueForKey:@"order_id"] stringValue];
    lblTotal.text = [[apiResponseArray valueForKey:@"total_amount"] stringValue];
    lblTotal.text = [NSString stringWithFormat:@"%.2f",[lblTotal.text floatValue]];
    lblDeliveryDate.text = gShippingDate;
    lblItemsOrdered.text =  gProductName;
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (IBAction)btnTestPressed:(id)sender{
    
    /* NSLog(@"\ngAPIString = %@,\ngIsLoggedIn = %d,\ngCustomerID = %@,\ngAuthToken = %@,\ngCardID = %@,\ngCartToken = %@,\ngServiceAreaID = %@,\ngLocationLKPID = %@,\ngShippingPrice = %@,\ngProductPrice = %@,\ngQuantity = %@,\ngShippingDate = %@,\ngPickupLocation = %@,\ngConnectionService = %@,\ngProductID = %@,\ngProductName = %@,\ngProductID_conn = %@,\ngProductName_conn = %@,\ngProductPrice_conn = %@,\ngFirstName = %@,\ngLastName = %@,\ngEmail = %@,\ngPassword = %@,\ngMobilePhone = %@,\ngHomePhone = %@,\ngShipAddress1 = %@,\ngShipAddress2 = %@,\ngShipCity = %@,\ngShipState = %@,\ngShipZipCode = %@,\ngIsAddressSameAsShipping = %d,\ngCardHolderName = %@,\ngCardType = %@,\ngCardNumber = %@,\ngCVV = %@,\ngExpirationMonth = %@,\ngDeliveryNotes = %@,\ngPromoCode = %@,\ngPayAddress1 = %@,\ngPayAddress2 = %@,\ngPayCity = %@,\ngPayState = %@,\ngPayZipCode =  %@"
          ,gAPIString,gIsLoggedIn,gCustomerID,gAuthToken,gCardID,gCartToken,gServiceAreaID,gLocationLKPID,gShippingPrice,gProductPrice,gQuantity,gShippingDate,gPickupLocation,gConnectionService,gProductID,gProductName,gProductID_conn,gProductName_conn,gProductPrice_conn,gFirstName,gLastName,gEmail,gPassword,gMobilePhone,gHomePhone,gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode,gIsAddressSameAsShipping,gCardHolderName,gCardType,gCardNumber,gCVV,gExpirationMonth,gDeliveryNotes,gPromoCode,gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode);
     */
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [viewControllersArray addObject:@"OrderConfirmViewController"];
    
    UIButton *btnTest = [[UIButton alloc]initWithFrame:CGRectMake(250, 25, 50,20)];
    [btnTest setTitle:@"Test" forState:UIControlStateNormal];
    [btnTest addTarget:self action:@selector(btnTestPressed:)forControlEvents:UIControlEventTouchUpInside];
    // [self.view addSubview:btnTest];
    
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];
    
    lblOrderNumberTitle= [[UILabel alloc]initWithFrame:CGRectMake(16, delta+230, 150, 20)];
    lblTotalTitle= [[UILabel alloc]initWithFrame:CGRectMake(16, delta+250, 150, 20)];
    lblDeliveryDateTitle= [[UILabel alloc]initWithFrame:CGRectMake(16, delta+270, 150, 20)];
    lblItemsOrderedTitle= [[UILabel alloc]initWithFrame:CGRectMake(16, delta+290, 150, 20)];
    
    lblOrderNumber= [[UILabel alloc]initWithFrame:CGRectMake(150, delta+230, 150, 20)];
    lblTotal= [[UILabel alloc]initWithFrame:CGRectMake(150, delta+250, 150, 20)];
    lblDeliveryDate= [[UILabel alloc]initWithFrame:CGRectMake(150, delta+270, 150, 20)];
    lblItemsOrdered= [[UILabel alloc]initWithFrame:CGRectMake(150, delta+290, 150, 20)];

    lblOrderNumberTitle.text = @"ORDER #";
    lblTotalTitle.text = @"TOTAL:";
    lblDeliveryDateTitle.text = @"DELIVERY DATE:";
    lblItemsOrderedTitle.text = @"ITEMS ORDERED:";
    
    lblOrderNumber.textAlignment = NSTextAlignmentRight;
    lblTotal.textAlignment = NSTextAlignmentRight;
    lblDeliveryDate.textAlignment = NSTextAlignmentRight;
    lblItemsOrdered.textAlignment = NSTextAlignmentRight;
    
    lblOrderNumberTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    lblTotalTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    lblDeliveryDateTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    lblItemsOrderedTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    
    lblOrderNumber.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    lblTotal.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    lblDeliveryDate.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    lblItemsOrdered.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
    
    lblHeader.font = [UIFont fontWithName:@"HelveticaNeueLTCom-Blk" size:12];
    lblHeader.textColor = [UIColor darkGrayColor];
    
    UIView *whiteBack1 = [[UIView alloc]initWithFrame:CGRectMake(12, delta+230, 294, 20)];
    UIView *whiteBack2 = [[UIView alloc]initWithFrame:CGRectMake(12, delta+270, 294, 20)];
    
    whiteBack1.backgroundColor = [UIColor whiteColor];
    whiteBack2.backgroundColor = [UIColor whiteColor];
    
    [self.view  addSubview:whiteBack1];
    [self.view  addSubview:whiteBack2];
    
    [self.view addSubview:lblOrderNumberTitle];
    [self.view addSubview:lblTotalTitle];
    [self.view addSubview:lblDeliveryDateTitle];
    [self.view addSubview:lblItemsOrderedTitle];
    
    [self.view addSubview:lblOrderNumber];
    [self.view addSubview:lblTotal];
    [self.view addSubview:lblDeliveryDate];
    [self.view addSubview:lblItemsOrdered];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *date = [dateFormatter dateFromString: gShippingDate];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy"];
    
    NSString *convertedString = [dateFormatter stringFromDate:date];

    lblDeliveryDate.text = convertedString;
    lblItemsOrdered.text =  gProductName;
    lblTotal.text = gTotal;
    lblTotal.text = [NSString stringWithFormat:@"%.2f",[lblTotal.text floatValue]];
    lblOrderNumber.text = gOrderID;
    
    // user is logged in now
    
    gIsLoggedIn = 1;
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }

    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}
@end
