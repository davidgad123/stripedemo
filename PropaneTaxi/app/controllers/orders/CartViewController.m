//
//  CartViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "CartViewController.h"
#import "AFNetworking.h"
#import "PTCartViewCell.h"
#import "OrderViewController.h"
#import "MBProgressHUD.h"

@implementation CartViewController

@synthesize btnHome;
@synthesize btnNav;
@synthesize myScrollView;

@synthesize btnCheckOut;
@synthesize btnContinue;
@synthesize lblCartStatus;

@synthesize lblItemsInCart;

@synthesize lblHeader;
@synthesize lblCartTotalPrice;

@synthesize viewTaxes, viewContainer, viewCartContainer, viewSpliter;
@synthesize lblSubtotal, lblDiscount, lblTaxes, lblTotal;

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _shouldAddProduct = YES;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _shouldAddProduct = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //    activityIndicator.hidden = YES;
    
    [viewControllersArray addObject:@"CartViewController"];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];
    
    UIButton *btnTest = [[UIButton alloc]initWithFrame:CGRectMake(250, 25, 50,20)];
    [btnTest setTitle:@"Test" forState:UIControlStateNormal];
    [btnTest addTarget:self action:@selector(btnTestPressed:)forControlEvents:UIControlEventTouchUpInside];
    // [self.view addSubview:btnTest];
    
    float connectionPrice = 0.0;
    
    NSString *additions = @"None";
    if (![gConnectionService isEqualToString:@"None"]) {
        additions = @"Connection Service + $4.99";
        connectionPrice = 4.99;
    }
    
    lblCartStatus.text = @"Added to Cart!";
    
    lblItemsInCart.font = [UIFont fontWithName:@"HelveticaNeueLTCom-Blk" size:12];
    lblItemsInCart.textColor = [UIColor darkGrayColor];
    
    lblCartTotalPrice.font = [UIFont fontWithName:@"HelveticaNeueLTCom-Blk" size:12];
    lblCartTotalPrice.textColor = [UIColor darkGrayColor];
    
    self.navigationLabel.font = [UIFont fontWithName:@"Open Sans" size:10.0];
    self.navigationLabel.text = @"SHOPPING CART";
    self.cartItems = [NSArray array];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self getCartData];
    
    [viewTaxes setBackgroundColor:[UIColor clearColor]];
    [viewContainer setBackgroundColor:[UIColor clearColor]];
    [viewCartContainer setBackgroundColor:[UIColor clearColor]];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (IBAction)btnTestPressed:(id)sender{
    
    /* NSLog(@"\ngAPIString = %@,\ngIsLoggedIn = %d,\ngCustomerID = %@,\ngAuthToken = %@,\ngCardID = %@,\ngCartToken = %@,\ngServiceAreaID = %@,\ngLocationLKPID = %@,\ngShippingPrice = %@,\ngProductPrice = %@,\ngQuantity = %@,\ngShippingDate = %@,\ngPickupLocation = %@,\ngConnectionService = %@,\ngProductID = %@,\ngProductName = %@,\ngProductID_conn = %@,\ngProductName_conn = %@,\ngProductPrice_conn = %@,\ngFirstName = %@,\ngLastName = %@,\ngEmail = %@,\ngPassword = %@,\ngMobilePhone = %@,\ngHomePhone = %@,\ngShipAddress1 = %@,\ngShipAddress2 = %@,\ngShipCity = %@,\ngShipState = %@,\ngShipZipCode = %@,\ngIsAddressSameAsShipping = %d,\ngCardHolderName = %@,\ngCardType = %@,\ngCardNumber = %@,\ngCVV = %@,\ngExpirationMonth = %@,\ngDeliveryNotes = %@,\ngPromoCode = %@,\ngPayAddress1 = %@,\ngPayAddress2 = %@,\ngPayCity = %@,\ngPayState = %@,\ngPayZipCode =  %@"
          ,gAPIString,gIsLoggedIn,gCustomerID,gAuthToken,gCardID,gCartToken,gServiceAreaID,gLocationLKPID,gShippingPrice,gProductPrice,gQuantity,gShippingDate,gPickupLocation,gConnectionService,gProductID,gProductName,gProductID_conn,gProductName_conn,gProductPrice_conn,gFirstName,gLastName,gEmail,gPassword,gMobilePhone,gHomePhone,gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode,gIsAddressSameAsShipping,gCardHolderName,gCardType,gCardNumber,gCVV,gExpirationMonth,gDeliveryNotes,gPromoCode,gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode);
     */
}


- (void) apiCall:(NSString *)URLString :(NSString *)requestFor  {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    [apiResponseArray removeAllObjects];
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:requestFor];
        });
    });
}

- (void) processAPICall:(NSString *)currentSection  {
    
    if ([currentSection isEqualToString:@"removeCart"]) {
    
        // NSLog(@"removeCart: %@",apiResponseArray);
        
        gCartID = @"";
        
        if (![gCartID_conn isEqualToString:@""]) {
            NSString *apiUrl = [NSString stringWithFormat:@"remove_shopping_cart_item/customer_id/%@/shopping_cart_id/%@/cart_token/%@/auth_token/%@",gCustomerID, gCartID_conn,gCartToken,gAuthToken];
            [self apiCall:apiUrl:@"removeCart_conn"];
        }
        else {
            [self getCartData];
        }
    }
    
    if ([currentSection isEqualToString:@"removeCart_conn"]) {
        NSLog(@"\nremoveCart_conn: %@", apiResponseArray);
        gCartID_conn = @"";
        [self getCartData];
        /*
            UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderViewController"];
            [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
         */
    }
    
    if ([currentSection isEqualToString:@"addProduct"]) {
        
        // NSLog(@"addProduct - apiResponseArray: %@",apiResponseArray);
        
        if (![gProductID_conn isEqualToString:@""] ){
            
            // NSLog(@"here");
            
            NSString *postString = [NSString stringWithFormat:@"customer_id=%@&cartToken=%@&product_id=%@&quantity=%@&price=%@&shipping_day=%@&location_id=%@&delivery_notes=%@&auth_token=%@",gCustomerID,gCartToken,gProductID_conn,gQuantity,gProductPrice_conn,gShippingDate,gLocationLKPID,gDeliveryNotes,gAuthToken];
            
            [self apiCallPost:@"add_product/" :postString :@"addConnectionService"];
            
        } else {
            currentSection = @"addConnectionService";
        }
    }
    
    if ([currentSection isEqualToString:@"addConnectionService"]) {
        
        // NSLog(@"addConnectionService - apiResponseArray: %@",apiResponseArray);
        
        [self apiCall:[NSString stringWithFormat:@"get_shopping_cart/customer_id/%@/cart_token/%@/service_area_id/%@/auth_token/%@",gCustomerID, gCartToken,gServiceAreaID,gAuthToken]:@"getShoppingCart"];
    }
    
    if ([currentSection isEqualToString:@"getShoppingCart"]) {
        
        // NSLog(@"getShoppingCart - apiResponseArray: %@",apiResponseArray);
        
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckoutSummaryViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
    
//    activityIndicator.hidden = YES;
}

- (void) apiCallPost:(NSString *)URLString :(NSString *)postString :(NSString *)currentSection {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    [apiResponseArray removeAllObjects];
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    // NSLog(@"currentSection: %@",currentSection);
    // NSLog(@"URLString: %@",URLString);
    // NSLog(@"postString: %@",postString);
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        //       NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        [request setHTTPMethod:@"POST"];
        
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:currentSection];
        });
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - API Calls
- (void)getCartData
{
    if (gIsLoggedIn) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//        NSString *apiUrl = [NSString stringWithFormat:@"%@get_shopping_cart/customer_id/%@/cart_token/%@/service_area_id/%@/auth_token/%@",gAPIString,gCustomerID,gCartToken,gServiceAreaID,gAuthToken];
        NSString *apiUrl = [NSString stringWithFormat:@"%@get_shopping_cart/customer_id/%@/cart_token/%@/service_area_id/%@",gAPIString,gCustomerID,gCartToken,gServiceAreaID];

        WEAKSELF
        [manager GET:apiUrl
          parameters:nil
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 id cartData = [[[responseObject objectForKey:@"cart_data"] objectForKey:@"cart"] objectForKey:@"DATA"];
                 [weakSelf processCartData:cartData];
                 
                 id paymentData = [responseObject objectForKey:@"cart_data"];
                 [weakSelf processPaymentData:paymentData];
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 DLog(@"\nerror %@", error);
                 [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
             }
         ];
    } else {
        PTShoppingCart *cart = [PTShoppingCart sharedCart];
        self.cartItems = cart.items;
//        [self.cartCollectionView reloadData];
        [self refreshViews];
        btnCheckOut.enabled = (self.cartItems.count > 0) ? YES : NO;
    }
}

- (void)processCartData:(id)cartData
{
    NSArray *products = [cartData isKindOfClass:[NSArray class]] ? cartData : nil;
//    NSLog(@"processCartData - products: %@", products);
//    NSLog(@"count - %d", products.count);
    if (products && products.count) {
//        __block NSArray *cartItemsForDisplay = [NSArray array];
        NSMutableArray *cartItemsForDisplay = [[NSMutableArray alloc] init];
        [products enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            PTShoppingItem *shoppingItem = [[PTShoppingItem alloc] init];
            shoppingItem.productType = [obj objectAtIndex:3];
            shoppingItem.productID = [obj objectAtIndex:4];
            shoppingItem.itemName = [obj objectAtIndex:6];
            if ([[obj objectAtIndex:7] isEqual:[NSNull null]] != YES)
                shoppingItem.additions = [obj objectAtIndex:7];
            shoppingItem.quantity = [obj objectAtIndex:8] ? [NSNumber numberWithInteger:[[obj objectAtIndex:8] integerValue]] : @0;
            shoppingItem.disableQTY = [obj objectAtIndex:9];
            shoppingItem.pricePerItem = [NSNumber numberWithFloat:[[obj objectAtIndex:10] floatValue]];
            shoppingItem.extendedPrice = [NSNumber numberWithFloat:[[obj objectAtIndex:11] floatValue]];
            shoppingItem.discount = ![[obj objectAtIndex:12] isEqual:[NSNull null]] ? [NSNumber numberWithFloat:[[obj objectAtIndex:12] floatValue]] : @0;
            shoppingItem.orderNumber = [NSString stringWithFormat:@"%@",[obj objectAtIndex:0]];

            if ([[obj objectAtIndex:22] isEqual:[NSNull null]] != YES)
                shoppingItem.shippingDate = [obj objectAtIndex:22];

            if ([[obj objectAtIndex:24] isEqual:[NSNull null]] != YES)
                shoppingItem.pickupLocation = [obj objectAtIndex:24];

            if ([shoppingItem.productType isEqualToString:@"PRODUCT"] ||
                [shoppingItem.productType isEqualToString:@"REBATE"] ||
                [shoppingItem.productType isEqualToString:@"PROMO"])
            {
                
                if ([shoppingItem.disableQTY integerValue] == 0)
                {
                    gCartID = [[obj objectAtIndex:0] stringValue];
                    shoppingItem.orderNumber = [[obj objectAtIndex:0] stringValue];
                }
                
                [cartItemsForDisplay addObject:shoppingItem];
            }
        }];
        self.cartItems = [NSArray arrayWithArray:cartItemsForDisplay];
    } else {
        // load local data
        self.cartItems = [[PTShoppingCart sharedCart] items];
    }
//    [self.cartCollectionView reloadData];
    btnCheckOut.enabled = (self.cartItems.count > 0) ? YES : NO;
    
    [self refreshViews];
}

-(void)refreshViews
{
    CGRect orgContainer = viewCartContainer.frame;
    
    // remove all subviews
    [[viewCartContainer subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    int y = 0, index = 0;
    for (PTShoppingItem *cart in self.cartItems)
    {
        UIView *cartView = [self makeShoppingCartView:cart tag:index];
        [cartView setBackgroundColor:[UIColor whiteColor]];
        
        CGRect orgRect = cartView.frame;
        CGRect newRect = CGRectMake(0, y, orgRect.size.width, orgRect.size.height);
        cartView.frame = newRect;
        
        [viewCartContainer addSubview:cartView];
        
        y += newRect.size.height + 10 /* margin */;
        index ++;
    }

    viewCartContainer.frame = CGRectMake(orgContainer.origin.x, orgContainer.origin.y, orgContainer.size.width, y);
    
    //
    y += orgContainer.origin.y + 20 /* margin */;
    
    CGRect rect = viewContainer.frame;
    CGRect newRect = CGRectMake(rect.origin.x, y, rect.size.width, rect.size.height);
    [viewContainer setFrame:newRect];
    
    [viewSpliter setFrame:CGRectMake(newRect.origin.x, newRect.origin.y - 13, newRect.size.width, 1)];
    
    y = viewContainer.frame.origin.y + viewContainer.frame.size.height + 20;
    rect = btnCheckOut.frame;
    newRect = CGRectMake(rect.origin.x, y, rect.size.width, rect.size.height);
    [btnCheckOut setFrame:newRect];
    
    y = btnCheckOut.frame.origin.y + btnCheckOut.frame.size.height + 15;
    rect = btnContinue.frame;
    newRect = CGRectMake(rect.origin.x, y, rect.size.width, rect.size.height);
    [btnContinue setFrame:newRect];

    y = btnContinue.frame.origin.y + btnContinue.frame.size.height + 10;
    
    myScrollView.frame = CGRectMake(-8, 84, 328, 480);
    myScrollView.contentSize = CGSizeMake(320, y + 80);

//    [myScrollView setContentSize:CGSizeMake(myScrollView.frame.size.width, y)];
}


- (void)processPaymentData:(id)paymentData
{
    double tax = [[paymentData objectForKey:@"Total_Tax_Handling_Amt"] doubleValue];
    double price = [[paymentData objectForKey:@"Total_Price_Amt"] doubleValue];
    double cart = [[paymentData objectForKey:@"Total_Cart_Amt"] doubleValue];
    double discount = [[paymentData objectForKey:@"Total_Discount"] doubleValue];
    
    [lblDiscount setText:[NSString stringWithFormat:@"$%.02f", discount]];
    [lblSubtotal setText:[NSString stringWithFormat:@"$%.02f", price]];
    [lblTaxes setText:[NSString stringWithFormat:@"$%.02f", tax]];
    [lblTotal setText:[NSString stringWithFormat:@"$%.02f", cart]];
}

-(UIView *)makeShoppingCartView:(PTShoppingItem *)item tag:(NSInteger)index
{
    UIView *cartView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 180)];
    
    int y = 10; // margin
    
    // ITEM
    {
        UILabel *lblTitleItem = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 20)];
        lblTitleItem.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
        lblTitleItem.textColor = customBlueColor;
        lblTitleItem.textAlignment = NSTextAlignmentRight;
        lblTitleItem.text = @"ITEM:";
        
        UILabel *lblItem = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 180, 20)];
        [lblItem setFont:[UIFont fontWithName:@"Open Sans" size:12.0f]];
        lblItem.lineBreakMode = NSLineBreakByWordWrapping;
        lblItem.numberOfLines = 0;
        lblItem.text = item.itemName;
        [lblItem sizeToFit];
        
        [cartView addSubview:lblTitleItem];
        [cartView addSubview:lblItem];
        
        y+= lblItem.frame.size.height;
    }
    
    // QUANTITY
    {
        UILabel *lblTitleQuantity = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 20)];
        lblTitleQuantity.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
        lblTitleQuantity.textColor = customBlueColor;
        lblTitleQuantity.textAlignment = NSTextAlignmentRight;
        lblTitleQuantity.text = @"QUANTITY:";
        
        UILabel *lblQuantity = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 180, 20)];
        [lblQuantity setFont:[UIFont fontWithName:@"Open Sans" size:12.0f]];
        lblQuantity.text = [NSString stringWithFormat:@"%@",item.quantity];
        
        [cartView addSubview:lblTitleQuantity];
        [cartView addSubview:lblQuantity];
        
        y+= 20;
    }
    
    // Additions
    if (item.additions != nil)
    {
        UILabel *lblTitleAddition = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 20)];
        lblTitleAddition.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
        lblTitleAddition.textColor = customBlueColor;
        lblTitleAddition.textAlignment = NSTextAlignmentRight;
        lblTitleAddition.text = @"ADDITIONS:";
        
        UILabel *lblAddition = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 180, 20)];
        [lblAddition setFont:[UIFont fontWithName:@"Open Sans" size:12.0f]];
        lblAddition.text = item.additions;
        
        [cartView addSubview:lblTitleAddition];
        [cartView addSubview:lblAddition];
        
        y+= 20;
        
    }
    
    // Discounts
    double discount = [item.discount floatValue];
    if (discount > 0)
    {
        UILabel *lblTitleDiscount = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 20)];
        lblTitleDiscount.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
        lblTitleDiscount.textColor = customBlueColor;
        lblTitleDiscount.textAlignment = NSTextAlignmentRight;
        lblTitleDiscount.text = @"DISCOUNTS:";
        
        UILabel *lblDiscount1 = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 180, 20)];
        [lblDiscount1 setFont:[UIFont fontWithName:@"Open Sans" size:12.0f]];
        lblDiscount1.text = [NSString stringWithFormat:@"$%.2f",item.discount.floatValue];
        
        [cartView addSubview:lblTitleDiscount];
        [cartView addSubview:lblDiscount1];
        
        y+= 20;
        
    }
    
    // Price
    {
        UILabel *lblTitlePrice = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 20)];
        lblTitlePrice.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
        lblTitlePrice.textColor = customBlueColor;
        lblTitlePrice.textAlignment = NSTextAlignmentRight;
        lblTitlePrice.text = @"PRICE:";
        
        UILabel *lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 180, 20)];
        [lblPrice setFont:[UIFont fontWithName:@"Open Sans" size:12.0f]];
        lblPrice.text = [NSString stringWithFormat:@"$%.2f",item.extendedPrice.floatValue];
        
        [cartView addSubview:lblTitlePrice];
        [cartView addSubview:lblPrice];
        
        y+= 20;
        
    }
    
    // Shipping
    if (item.pickupLocation != nil)
    {
        UILabel *lblTitleShipping = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 20)];
        lblTitleShipping.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
        lblTitleShipping.textColor = customBlueColor;
        lblTitleShipping.textAlignment = NSTextAlignmentRight;
        lblTitleShipping.text = @"SHIPPING:";
        
        UILabel *lblShipping = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 180, 20)];
        [lblShipping setFont:[UIFont fontWithName:@"Open Sans" size:12.0f]];
        lblShipping.lineBreakMode = NSLineBreakByWordWrapping;
        lblShipping.numberOfLines = 0;
        lblShipping.text = item.pickupLocation;
        [lblShipping sizeToFit];
        
        [cartView addSubview:lblTitleShipping];
        [cartView addSubview:lblShipping];
        
        y+= lblShipping.frame.size.height;
    }
    
    
    // Buttons
    if ([item.disableQTY integerValue] == 0)
    {
        y += 10;
        
        UIButton *btnEdit = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnEdit.frame = CGRectMake(10, y, 130, 20);
        btnEdit.tag = index;
        [btnEdit setBackgroundImage:[UIImage imageNamed:@"checkout_edit.png"] forState:UIControlStateNormal];
        [btnEdit addTarget:self action:@selector(didPressEditOrder:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *btnRemove = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnRemove.frame = CGRectMake(150, y, 130, 20);
        btnRemove.tag = index;
        [btnRemove setBackgroundImage:[UIImage imageNamed:@"checkout_remove.png"] forState:UIControlStateNormal];
        [btnRemove addTarget:self action:@selector(didPressRemove:) forControlEvents:UIControlEventTouchUpInside];
        
        [cartView addSubview:btnEdit];
        [cartView addSubview:btnRemove];
        
        y += 20;
    }
    
    y += 10; // margin
    cartView.frame = CGRectMake(0, 0, viewCartContainer.frame.size.width, y);

    return cartView;
}

- (void)didPressEditOrder:(id)sender
{
    OrderViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderViewController"];
    
    UIButton *clicked = (UIButton *)sender;
    NSInteger index = clicked.tag;
    PTShoppingItem *shoppingItem = [self.cartItems objectAtIndex:index];
    vc.orderToEdit = shoppingItem;
    
    NSString *itemName = shoppingItem.itemName;
    if ([itemName isEqual:@"GRILL TANK - EXCHANGE"])
        gProductID = @"1";
    else
        gProductID = @"2";

//    gCustomerID
//    gCartToken =
//    gProductID
//    gProductPrice
//    gDeliveryNotes
//    gAuthToken
    gQuantity = [NSString stringWithFormat:@"%@", shoppingItem.quantity];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM, dd yyyy HH:mm:ss"];
    NSDate *createTime = [dateFormatter dateFromString:shoppingItem.shippingDate];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSString *strShippingDate = [dateFormatter stringFromDate:createTime];
    gShippingDate = strShippingDate;
    gPickupLocation = shoppingItem.pickupLocation;
    gLocationLKPID = shoppingItem.pickupLocationId;
    gConnectionService = shoppingItem.connectionService;

//    vc.orderToEdit = cell.cartItem;
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (void)didPressRemove:(id)sender
{
    UIButton *clicked = (UIButton *)sender;
    NSInteger index = clicked.tag;
    PTShoppingItem *shoppingItem = [self.cartItems objectAtIndex:index];
//    PTShoppingItem *shoppingItem = cell.cartItem;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (![gCartID isEqualToString:@""]) {
        [self apiCall:[NSString stringWithFormat:@"remove_shopping_cart_item/customer_id/%@/shopping_cart_id/%@/cart_token/%@/auth_token/%@",gCustomerID,shoppingItem.orderNumber,gCartToken,gAuthToken]:@"removeCart"];
    } else {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [[PTShoppingCart sharedCart] removeItem:shoppingItem];
        [self getCartData];
    }
    
    gQuantity = @"1";
    gShippingDate = @"";
    gPickupLocation = @"";
    gConnectionService = @"No";
}

#pragma mark - Protocol Conformance
#pragma mark UICollectionView
//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
//{
//    return self.cartItems.count;
//}
//
//- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    PTCartCollectionCell *cell = (PTCartCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cartCell" forIndexPath:indexPath];
//    cell.cartItem = [self.cartItems objectAtIndex:indexPath.row];
//    cell.delegate = self;
//    return cell;
//}

#pragma mark PTCartCellDelegate
//- (void)cartCellDidPressEdit:(PTCartCollectionCell *)cell
//{
//    OrderViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderViewController"];
//    vc.orderToEdit = cell.cartItem;
//    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
//}
//
//- (void)cartCellDidPressRemove:(PTCartCollectionCell *)cell
//{
//    PTShoppingItem *shoppingItem = cell.cartItem;
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    if (![gCartID isEqualToString:@""]) {
//        [self apiCall:[NSString stringWithFormat:@"remove_shopping_cart_item/customer_id/%@/shopping_cart_id/%@/cart_token/%@/auth_token/%@",gCustomerID,shoppingItem.orderNumber,gCartToken,gAuthToken]:@"removeCart"];
//    } else {
//        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        [[PTShoppingCart sharedCart] removeItem:cell.cartItem];
//        [self getCartData];
//    }
//    
//    gQuantity = @"1";
//    gShippingDate = @"";
//    gPickupLocation = @"";
//    gConnectionService = @"No";
//}

#pragma mark - IBActions
- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }

    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnCheckOutPressed:(id)sender {
    
    if (gIsLoggedIn) {
        [self processAPICall:@"getShoppingCart"];
    } else {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckoutAccountViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
}

- (IBAction)btnContinuePressed:(id)sender{
    NSString *viewControllerToLoad;
    if (gShipZipCode.length > 0) {
        viewControllerToLoad = @"OrderViewController";
    } else {
        viewControllerToLoad = @"CheckAvailabilityViewController";
    }
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:viewControllerToLoad];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];

}

@end
