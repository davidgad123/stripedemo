//
//  CheckoutShippingViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "CheckoutShippingViewController.h"
#import "CartViewController.h"
#import "MBProgressHUD.h"

@interface CheckoutShippingViewController ()
{
    int heightScroll;
    UITextField *txtCurrentField;
}
@end

@implementation CheckoutShippingViewController

@synthesize btnHome;
@synthesize btnNav;
@synthesize myScrollView;
@synthesize btnContinue;

@synthesize shipAddress1Txt;
@synthesize shipAddress2Txt;
@synthesize shipCityTxt;
@synthesize shipStateTxt;
@synthesize shipZipTxt;

@synthesize lblHeader;

@synthesize activityIndicator;

NSMutableArray *arrayState;
UIAlertView *errorEmailAlert;

#pragma mark - ViewController LifeCycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
            _checkOut = YES;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    
//    [self setupViewsForCheckOut:self.checkOut];
    [self setupViewsForCheckOut:false];
    
    myScrollView.frame = CGRectMake(0, 84, 320, 480);
    myScrollView.contentSize = CGSizeMake(320, 560);

//    myScrollView.contentSize = myScrollView.frame.size;
    
    heightScroll = myScrollView.frame.size.height;
    
    txtCurrentField = nil;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    lblHeader.font = [UIFont fontWithName:@"HelveticaNeueLTCom-Blk" size:12];
    lblHeader.textColor = [UIColor darkGrayColor];
    
    [viewControllersArray addObject:@"CheckoutShippingViewController"];
    
    activityIndicator.hidden = YES;
    
    UIButton *btnTest = [[UIButton alloc]initWithFrame:CGRectMake(250, 25, 50,20)];
    [btnTest setTitle:@"Test" forState:UIControlStateNormal];
    [btnTest addTarget:self action:@selector(btnTestPressed:)forControlEvents:UIControlEventTouchUpInside];
    // [self.view addSubview:btnTest];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];
    
    shipAddress1Txt.text = gShipAddress1;
    shipAddress2Txt.text = gShipAddress2;
    shipCityTxt.text = gShipCity;
    shipStateTxt.text = gShipState;
    shipZipTxt.text = gShipZipCode;

    arrayState = [[NSMutableArray alloc] initWithObjects:@"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH",@"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY",nil];
    
	// Do any additional setup after loading the view.
}

- (void)setupViewsForCheckOut:(BOOL)checkOut
{
    lblHeader.text = checkOut ? @"CHECKOUT" : @"CREATE ACCOUNT";
    self.navigationLabel.text = checkOut ? @"CHECKOUT" : @"CREATE ACCOUNT";
    self.navigationLabel.font = [UIFont fontWithName:@"Open Sans" size:10.0];
//    self.viewCartButton.hidden = !checkOut;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Protocol Conformance
#pragma mark UITextField
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    int height = myScrollView.frame.size.height;
    if (heightScroll != height) {
        CGRect rect = myScrollView.frame;
        CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height + 200);
        myScrollView.frame = newRect;
    }
    
    txtCurrentField = nil;
    
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    int height = myScrollView.frame.size.height;
    
    if (heightScroll == height) {
        CGRect rect = myScrollView.frame;
        CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height - 200);
        myScrollView.frame = newRect;
    }
    
    txtCurrentField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

}

#pragma mark - API Calls

- (void) apiCallPost:(NSString *)URLString postString:(NSString *)postString currentSection:(NSString *)currentSection {
    [self apiCallPost:URLString postString:postString currentSection:currentSection withCartItem:nil];
}

- (void) apiCallPost:(NSString *)URLString postString:(NSString *)postString currentSection:(NSString *)currentSection withCartItem:(PTShoppingItem*)shoppingItem
{
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    [apiResponseArray removeAllObjects];
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    // NSLog(@"currentSection: %@",currentSection);
    // NSLog(@"URLString: %@",URLString);
    // NSLog(@"postString: %@",postString);
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        //       NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:currentSection withCartItem:shoppingItem];
        });
    });
}

- (void) apiCall:(NSString *)URLString :(NSString *)currentSection {
    [self apiCall:URLString :currentSection withCartItem:nil];
}

- (void) apiCall:(NSString *)URLString :(NSString *)currentSection withCartItem:(PTShoppingItem*)cartItem
{
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    [apiResponseArray removeAllObjects];
    
    activityIndicator.hidden = NO;
    [activityIndicator startAnimating];
    activityIndicator.hidesWhenStopped = YES;
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:currentSection withCartItem:cartItem];
        });
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView isEqual:errorEmailAlert]) {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckoutAccountViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }    

}

- (void) processAPICall:(NSString *)currentSection  {
    [self processAPICall:currentSection withCartItem:nil];
}

- (void) processAPICall:(NSString *)currentSection withCartItem:(PTShoppingItem*)shoppingItem {
    if ([currentSection isEqualToString:@"createCustomer"]) {
        
         NSLog(@"createCustomer - apiResponseArray: %@",apiResponseArray);
        
        if (apiResponseArray == nil)
        {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            NSString *errorMsg = @"Network Error";
            
            errorEmailAlert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                        message: errorMsg
                                                       delegate: self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
            
            [errorEmailAlert show];
            return;
        }
        
        NSString *msg = [apiResponseArray valueForKey:@"msg"];
        
        if ([msg isEqualToString:@"error"]) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            NSString *errorMsg = [apiResponseArray valueForKey:@"msg_info"];
            
            NSString *message = errorMsg;
            if ([errorMsg isEqualToString:@"Email is NOT unique"] || [errorMsg isEqualToString:@"No Email Provided"])
            {
                message = @"Email address already found in system";
            }
            
            errorEmailAlert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                        message: message
                                                       delegate: self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
            
            [errorEmailAlert show];
            return;
            
        }

        id customerId = [apiResponseArray valueForKey:@"NEW_ID"];
        id password = [apiResponseArray valueForKey:@"ACCOUNT_PASSWORD"];
        
        gCustomerID = (NSString *)customerId;
        gRealPassword = (NSString *)password;
        
        // TODO
//        NSString *emailPwd = [NSString stringWithFormat:@"%@%@",gEmail,gPassword];
//        [[NSUserDefaults standardUserDefaults] setObject:gRealPassword forKey:emailPwd];
        
        [self apiCall:[NSString stringWithFormat:@"/login/account_email/%@/account_password/%@",gEmail, gRealPassword]:@"login"];
    }

    if ([currentSection isEqualToString:@"login"]) {
        
         NSLog(@"login - apiResponseArray: %@",apiResponseArray);
        
        NSString *error = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"error"];
        
        // NSLog(@"error: %@",error);
        
        if ([error intValue] == 0) {
            
            gCartToken = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"cartToken"];
            gServiceAreaID = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"service_area_id"];
            gAuthToken = [[apiResponseArray valueForKey:@"auth_token"]valueForKey:@"auth_token"];
            
            gIsLoggedIn = 1;
            
            // add product
            WEAKSELF
            PTShoppingCart *cart = [PTShoppingCart sharedCart];
            [cart.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                PTShoppingItem *cartItem = [obj isKindOfClass:[PTShoppingItem class]] ? obj : nil;
                if (cartItem) {
                    NSString *productId = cartItem.productID;

                    
                    NSString *URLString = @"add_product";
                    URLString = [NSString stringWithFormat:@"%@/customer_id/%@", URLString, gCustomerID];
                    URLString = [NSString stringWithFormat:@"%@/cartToken/%@", URLString, gCartToken];
                    URLString = [NSString stringWithFormat:@"%@/product_id/%@", URLString, productId];
                    URLString = [NSString stringWithFormat:@"%@/quantity/%d", URLString, [cartItem.quantity integerValue]];
                    URLString = [NSString stringWithFormat:@"%@/price/%f", URLString, [cartItem.pricePerItem floatValue]];
                    URLString = [NSString stringWithFormat:@"%@/shipping_day/%@", URLString, cartItem.shippingDate];
                    URLString = [NSString stringWithFormat:@"%@/location_id/%@", URLString, cartItem.pickupLocationId];
                    URLString = [NSString stringWithFormat:@"%@/delivery_notes/%@", URLString, gDeliveryNotes];
                    URLString = [NSString stringWithFormat:@"%@/auth_token/%@", URLString, gAuthToken];
                    [weakSelf apiCall:URLString :@"addProduct" withCartItem:cartItem];
                    
                    float productPrice = [gShippingPrice doubleValue];
                    if (productPrice > 0)
                    {
                        NSString *URLString = @"add_product";
                        URLString = [NSString stringWithFormat:@"%@/customer_id/%@", URLString, gCustomerID];
                        URLString = [NSString stringWithFormat:@"%@/cartToken/%@", URLString, gCartToken];
                        URLString = [NSString stringWithFormat:@"%@/product_id/%@", URLString, @"LOCATIONFEE"];
                        URLString = [NSString stringWithFormat:@"%@/quantity/%@", URLString, @"1"];
                        URLString = [NSString stringWithFormat:@"%@/price/%@", URLString, gShippingPrice];
                        URLString = [NSString stringWithFormat:@"%@/shipping_day/%@", URLString, cartItem.shippingDate];
                        URLString = [NSString stringWithFormat:@"%@/location_id/%@", URLString, cartItem.pickupLocationId];
                        URLString = [NSString stringWithFormat:@"%@/delivery_notes/%@", URLString, gDeliveryNotes];
                        URLString = [NSString stringWithFormat:@"%@/auth_token/%@", URLString, gAuthToken];
                        
                        [weakSelf apiCall:URLString :@"addFeeProduct" withCartItem:cartItem];
                    }

                }
            }];

//            NSString *postString = [NSString stringWithFormat:@"auth_token=%@&customer_id=%@&password=%@&password_confirm=%@", gAuthToken,gCustomerID,gPassword,gPassword];
//            [self apiCallPost:@"update_customer/" postString:postString currentSection:@"updatePassword"];

        } else {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            NSString *errorMsg = [[apiResponseArray valueForKey:@"customer_login"] valueForKey:@"msg"];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: errorMsg
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    
    if ([currentSection isEqualToString:@"addProduct"]) {
        NSLog(@"addProduct - apiResponseArray: %@",apiResponseArray);
        
//        if (![shoppingItem.connectionService isEqualToString:@" No"] )
        if ([shoppingItem.connectionService rangeOfString:@"No"].location == NSNotFound)
        {
            // NSLog(@"here");
            
            NSString *URLString = @"add_product";
            URLString = [NSString stringWithFormat:@"%@/customer_id/%@", URLString, gCustomerID];
            URLString = [NSString stringWithFormat:@"%@/cartToken/%@", URLString, gCartToken];
            URLString = [NSString stringWithFormat:@"%@/product_id/%@", URLString, gProductID_conn];
            URLString = [NSString stringWithFormat:@"%@/quantity/%@", URLString, gQuantity];
            URLString = [NSString stringWithFormat:@"%@/price/%@", URLString, gProductPrice_conn];
            URLString = [NSString stringWithFormat:@"%@/shipping_day/%@", URLString, gShippingDate];
            URLString = [NSString stringWithFormat:@"%@/location_id/%@", URLString, gLocationLKPID];
            URLString = [NSString stringWithFormat:@"%@/delivery_notes/%@", URLString, gDeliveryNotes];
            URLString = [NSString stringWithFormat:@"%@/auth_token/%@", URLString, gAuthToken];
            
            currentSection = @"addConnectionService";
            [self apiCall:URLString :@"addConnectionService"];
        }
        else
        {
            currentSection = @"addConnectionService";
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        }
    }
    
    if ([currentSection isEqualToString:@"addConnectionService"]) {
        [self apiCall:[NSString stringWithFormat:@"get_shopping_cart/customer_id/%@/cart_token/%@/service_area_id/%@/auth_token/%@",gCustomerID, gCartToken,gServiceAreaID,gAuthToken]:@"getShoppingCart"];
    }
    
    if ([currentSection isEqualToString:@"getShoppingCart"]) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [[PTShoppingCart sharedCart] emptyCart];
        
        currentSection = @"updatePassword";
    }
    
    if ([currentSection isEqualToString:@"updatePassword"]) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSString *viewControllerToLoad = self.checkOut ? @"CheckoutPaymentViewController" : @"HomeViewController";
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:viewControllerToLoad];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];

        gIsLoggedIn = YES;
    }
    
}

#pragma mark - IBActions
- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }

    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}


- (IBAction)btnContinuePressed:(id)sender{
    
     [self.view endEditing:YES];
    
    if (([shipAddress1Txt.text isEqual:@""]) || ([shipCityTxt.text isEqual:@""]) || ([shipStateTxt.text isEqual:@""])||([shipZipTxt.text isEqual:@""])) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"All fields are required"
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    gShipAddress1 = shipAddress1Txt.text;
    gShipAddress2 = shipAddress2Txt.text;
    gShipCity = shipCityTxt.text;
    gShipState = shipStateTxt.text;
    gShipZipCode = shipZipTxt.text;

    if (gIsAddressSameAsShipping) {
    
        gPayAddress1 = gShipAddress1;
        gPayAddress2 = gShipAddress2;
        gPayCity = gShipCity;
        gPayState = gShipState;
        gPayZipCode = gShipZipCode;
    }
    
    if (gIsLoggedIn) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckoutPaymentViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];

    } else {
        
        NSString *postString = [NSString stringWithFormat:@"first_name=%@&last_name=%@&address_1=%@&address_2=%@&city=%@&state=%@&zip=%@&ship_first_name=%@&ship_last_name=%@&ship_address_1=%@&ship_address_2=%@&ship_city=%@&ship_state=%@&ship_zip=%@&phone_number=%@&phone_number_mobile=%@&email_address=%@",gFirstName,gLastName,gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode,gFirstName,gLastName,gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode,gHomePhone,gMobilePhone,gEmail];
        
        [self apiCallPost:@"create_customer" postString:postString currentSection:@"createCustomer"];
    }
}

- (IBAction)btnViewCartPressed:(id)sender{
    CartViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    vc.shouldAddProduct = NO;
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}


- (IBAction)stateBtnPressed:(id)sender {
    
//    int height = [arrayState count] * 45 + 30;
    ZSYPopoverListView *listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 280, 250)];

    listView.titleName.text = @"Select State";
    listView.datasource = self;
    listView.delegate = self;
    
    [listView show];

    if (txtCurrentField != nil)
        [txtCurrentField resignFirstResponder];
    
    int height = myScrollView.frame.size.height;
    if (heightScroll != height) {
        CGRect rect = myScrollView.frame;
        CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height + 200);
        myScrollView.frame = newRect;
    }
}

- (NSInteger)popoverListView:(ZSYPopoverListView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayState.count;
}

- (UITableViewCell *)popoverListView:(ZSYPopoverListView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusablePopoverCellWithIdentifier:identifier];
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.textLabel.text = arrayState[indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.textLabel.numberOfLines = 2;
    
    return cell;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    shipStateTxt.text = arrayState[indexPath.row];
    
    [tableView dismiss];
}

@end
