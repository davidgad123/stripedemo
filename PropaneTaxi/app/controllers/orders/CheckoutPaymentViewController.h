//
//  CheckoutPaymentViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSYPopoverListView.h"
#import "CDatePickerViewEx.h"

@interface CheckoutPaymentViewController : UIViewController<ZSYPopoverListDatasource, ZSYPopoverListDelegate, CDatePickerDelegate>

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;

// payment fields
@property (strong, nonatomic) IBOutlet CustomUITextField *cardHolderNameTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *cardTypeTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *cardNumberTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *cvvTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *expirationDateTxt;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, assign, getter = isCheckOut) BOOL checkOut;
@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewCartButton;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;
- (IBAction)btnContinuePressed:(id)sender;
- (IBAction)cardTypeBtnPressed:(id)sender;
- (IBAction)expirationBtnPressed:(id)sender;


@end
