//
//  CheckoutSummaryViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckoutSummaryViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;
@property (strong, nonatomic) IBOutlet UIButton *btnPlaceOrder;


@property (strong, nonatomic) IBOutlet UIButton *btnChangeShipping;
@property (strong, nonatomic) IBOutlet UIButton *btnChangeBilling;
@property (strong, nonatomic) IBOutlet UIButton *btnChangePayment;
@property (strong, nonatomic) IBOutlet UIButton *btnApplyPromo;
@property (strong, nonatomic) IBOutlet UIButton *btnViewCart;

@property (strong, nonatomic) IBOutlet UITextView *shippingAddressTxt;
@property (strong, nonatomic) IBOutlet UITextView *billingAddressTxt;
@property (strong, nonatomic) IBOutlet UITextView *paymentInfoTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *promoTxt;
@property (strong, nonatomic) IBOutlet UITextView *deliveryNotesTxt;

@property (strong,nonatomic) IBOutlet UILabel *lblShippingAddressTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblBillingAddressTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblPaymentInfoTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblAdditionalDetailsTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblDeliveryNotesTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblPromoCodeTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblSummaryTitle;

@property (strong,nonatomic) IBOutlet UILabel *lblPromoCodeStatus;

// summary labels
@property (strong,nonatomic) IBOutlet UILabel *lblSubTotal;
@property (strong,nonatomic) IBOutlet UILabel *lblTotal;
@property (strong,nonatomic) IBOutlet UILabel *lblTaxes;
@property (strong,nonatomic) IBOutlet UILabel *lblShipping;
@property (strong,nonatomic) IBOutlet UILabel *lblDeliveryDate;
@property (strong,nonatomic) IBOutlet UILabel *lblTankLocation;
@property (strong,nonatomic) IBOutlet UILabel *lblPromoCodeMessage;
@property (strong,nonatomic) IBOutlet UILabel *lblNewSubTotal;
@property (strong,nonatomic) IBOutlet UILabel *lblDiscountApplied;

// summary label titles
@property (strong,nonatomic) IBOutlet UILabel *lblSubTotalTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblTotalTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblTaxesTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblShippingTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblDeliveryDateTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblTankLocationTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblPromoCodeMessageTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblNewSubTotalTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblDiscountAppliedTitle;

// popup view
@property (strong, nonatomic) IBOutlet UIView *popupView;

@property (strong, nonatomic) IBOutlet CustomUITextField *address1Txt;
@property (strong, nonatomic) IBOutlet CustomUITextField *address2Txt;
@property (strong, nonatomic) IBOutlet CustomUITextField *cityTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *stateTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *zipTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *expirationYearTxt;

@property (strong, nonatomic) IBOutlet UIButton *btnDone;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;

@property (strong,nonatomic) IBOutlet UILabel *lblAddress1Title;
@property (strong,nonatomic) IBOutlet UILabel *lblAddress2Title;
@property (strong,nonatomic) IBOutlet UILabel *lblCityTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblStateTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblZipTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblExpirationYearTitle;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;


@property (nonatomic,strong) IBOutlet UIPickerView *pickerState;
@property (nonatomic,strong) IBOutlet UIPickerView *pickerCardMonth;
@property (nonatomic,strong) IBOutlet UIPickerView *pickerCardYear;
@property (nonatomic,strong) IBOutlet UIPickerView *pickerCardType;

@property (strong,nonatomic) UIView *whiteBack1;
@property (strong,nonatomic) UIView *whiteBack2;
@property (strong,nonatomic) UIView *whiteBack3;
@property (strong,nonatomic) UIView *whiteBack4;
@property (strong,nonatomic) UIView *greenBack;

- (IBAction)btnDonePressed:(id)sender;
- (IBAction)btnCancelPressed:(id)sender;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;
- (IBAction)btnPlaceOrderPressed:(id)sender;

- (IBAction)btnChangeShippingPressed:(id)sender;
- (IBAction)btnChangeBillingPressed:(id)sender;
- (IBAction)btnChangePaymentPressed:(id)sender;
- (IBAction)btnApplyPromoPressed:(id)sender;
- (IBAction)btnViewCartPressed:(id)sender;

@end
