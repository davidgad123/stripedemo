//
//  CheckoutAccountViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "CheckoutAccountViewController.h"
#import "CheckoutShippingViewController.h"
#import "CartViewController.h"

@interface CheckoutAccountViewController ()
{
    int heightScroll;
}
@end

@implementation CheckoutAccountViewController

@synthesize btnHome;
@synthesize btnNav;
@synthesize myScrollView;
@synthesize btnContinue;

@synthesize firstNameTxt;
@synthesize lastNameTxt;
@synthesize emailTxt;
@synthesize mobilePhoneTxt;
@synthesize homePhoneTxt;

@synthesize lblHeader;

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _checkOut = YES;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _checkOut = YES;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];

    [self setupViewsForCheckOut:false];

    myScrollView.frame = CGRectMake(0, 84, 320, 480);
    myScrollView.contentSize = CGSizeMake(320, 560);

//    myScrollView.contentSize = myScrollView.frame.size;
 
    heightScroll = myScrollView.frame.size.height;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [viewControllersArray addObject:@"CheckoutAccountViewController"];
    
    lblHeader.font = [UIFont fontWithName:@"HelveticaNeueLTCom-Blk" size:12];
    lblHeader.textColor = [UIColor darkGrayColor];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];
    
    firstNameTxt.text = gFirstName;
    lastNameTxt.text = gLastName;
    emailTxt.text = gEmail;
    mobilePhoneTxt.text = gMobilePhone;
    homePhoneTxt.text = gHomePhone;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    int height = myScrollView.frame.size.height;
    if (heightScroll != height) {
        CGRect rect = myScrollView.frame;
        CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height + 200);
        myScrollView.frame = newRect;
    }

    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    int height = myScrollView.frame.size.height;
    
    if (heightScroll == height) {
        CGRect rect = myScrollView.frame;
        CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height - 200);
        myScrollView.frame = newRect;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (IBAction)homeBtnPressed:(id)sender {
    
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }

    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnViewCartPressed:(id)sender{
    CartViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    vc.shouldAddProduct = NO;
}

- (IBAction)btnContinuePressed:(id)sender{
    
     [self.view endEditing:YES];
    
    if (([firstNameTxt.text isEqual:@""]) || ([lastNameTxt.text isEqual:@""]) || ([emailTxt.text isEqual:@""])||([mobilePhoneTxt.text isEqual:@""])||([homePhoneTxt.text isEqual:@""])) {

            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: @"All fields are required"
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            
            [alert show];
            
            return;
        }
    
    gMobilePhone = [mobilePhoneTxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
//
//    if (!([gMobilePhone length] == 10)) {
//        
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
//                                                       message: @"Mobile phone number should be 10 digits"
//                                                      delegate: self
//                                             cancelButtonTitle:@"Ok"
//                                             otherButtonTitles:nil];
//        
//        [alert show];
//        
//        return;
//    }
//    
    gHomePhone = [homePhoneTxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
//
//    if (!([gHomePhone length] == 10)) {
//        
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
//                                                       message: @"Home phone number should be 10 digits"
//                                                      delegate: self
//                                             cancelButtonTitle:@"Ok"
//                                             otherButtonTitles:nil];
//        
//        [alert show];
//        
//        return;
//    }

    
    gFirstName = firstNameTxt.text;
    gLastName = lastNameTxt.text;
    gEmail = emailTxt.text;
//    gPassword = passwordTxt.text;
    
//    [[NSUserDefaults standardUserDefaults] setObject:gPassword forKey:gEmail];
    
    CheckoutShippingViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckoutShippingViewController"];
    vc.checkOut = self.checkOut;
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (void)setupViewsForCheckOut:(BOOL)checkOut
{
    lblHeader.text = checkOut ? @"CHECKOUT" : @"CREATE ACCOUNT";
    self.navigationTitle.text = checkOut ? @"CHECKOUT" : @"CREATE ACCOUNT";
    self.navigationTitle.font = [UIFont fontWithName:@"Open Sans" size:10.0];
//    self.viewCartButton.hidden = !checkOut;
    
}

@end
