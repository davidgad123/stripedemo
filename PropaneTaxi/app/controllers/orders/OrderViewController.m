//
//  OrderViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/5/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "OrderViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AFNetworking.h"

@interface OrderViewController ()
@property (nonatomic, strong) UIAlertView *addedToCartAlert;
@property (nonatomic, strong) UIAlertView *updatedCartAlert;
@end

@implementation OrderViewController

@synthesize btnHome;
@synthesize btnNav;

@synthesize btnAddCart;
@synthesize btnClosePopup;

@synthesize btnQuantity;
@synthesize btnShippingDate;
@synthesize btnConnectionService;
@synthesize btnPickupLocation;

@synthesize activityIndicator;
@synthesize myScrollView;

@synthesize quantityTxt;
@synthesize shippingDateTxt;
@synthesize pickupLocationTxt;
@synthesize connectionServiceTxt;

@synthesize lblProductName,lblProductPrice;
@synthesize lblProductDescription;
@synthesize btnChooseDifferentProduct;
@synthesize productImageView;

@synthesize lblQuantityTitle;
@synthesize lblShippingDateTitle;
@synthesize lblPickupLocationTitle;
@synthesize lblConnectionTitle;
@synthesize lblHeader;

@synthesize  addCartView;

NSString *currentSection = @"";
NSString *currentProduct = @"Exchange";

NSMutableArray *itemArray;
NSMutableArray *priceArray;
NSMutableArray *pickupLocationArray;
NSMutableArray *pickupPriceArray;
NSMutableArray *shippingDateArray;

UIAlertView * alertDelivery;

NSInteger selectedQuantity;
NSInteger selectedShippingDate;
NSInteger selectedTankLocation;
bool bConnectionService;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [viewControllersArray addObject:@"OrderViewController"];
    
    lblHeader.font = [UIFont fontWithName:@"HelveticaNeueLTCom-Blk" size:12];
    lblHeader.textColor = [UIColor darkGrayColor];
    
    
    UIButton *btnTest = [[UIButton alloc]initWithFrame:CGRectMake(250, 25, 50,20)];
    [btnTest setTitle:@"Test" forState:UIControlStateNormal];
    [btnTest addTarget:self action:@selector(btnTestPressed:)forControlEvents:UIControlEventTouchUpInside];
    // [self.view addSubview:btnTest];
    
    itemArray = [[NSMutableArray alloc] init];
    priceArray = [[NSMutableArray alloc] init];
    pickupLocationArray = [[NSMutableArray alloc] init];
    shippingDateArray= [[NSMutableArray alloc] init];
    pickupPriceArray =[[NSMutableArray alloc] init];
    currentProduct = @"Exchange";
    
    activityIndicator.hidden = YES;
    
    quantityTxt.userInteractionEnabled = NO;
    shippingDateTxt.userInteractionEnabled = NO;
    pickupLocationTxt.userInteractionEnabled = NO;
    connectionServiceTxt.userInteractionEnabled = NO;
    
    quantityTxt.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    shippingDateTxt.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    pickupLocationTxt.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    connectionServiceTxt.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    
    quantityTxt.textColor = [UIColor darkGrayColor];
    shippingDateTxt.textColor = [UIColor darkGrayColor];
    pickupLocationTxt.textColor = [UIColor darkGrayColor];
    connectionServiceTxt.textColor = [UIColor darkGrayColor];
    
    lblProductName.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblProductName.textColor = customBlueColor;
    lblProductDescription.font = [UIFont fontWithName:@"OpenSans-Bold" size:9];
    lblProductDescription.textColor = [UIColor darkGrayColor];
    
    lblProductDescription.numberOfLines = 3;
    
    lblProductPrice.font = [UIFont fontWithName:@"OpenSans-Bold" size:18];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];
    
    lblQuantityTitle.textColor = customBlueColor;
    lblShippingDateTitle.textColor = customBlueColor;
    lblPickupLocationTitle.textColor = customBlueColor;
    lblConnectionTitle.textColor = customBlueColor;
    
    lblQuantityTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblShippingDateTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblPickupLocationTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblConnectionTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    
    self.popupView.hidden = YES;
    
    // scroll view
    myScrollView.frame = CGRectMake(0, 84, 320, 480);
    myScrollView.contentSize = CGSizeMake(320, 560);
    
    if (SH >500){
        myScrollView.contentSize = CGSizeMake(320, 480);
        addCartView.frame = CGRectMake(0, 430, 320, 50);
    }
    
    [self setupProductInformation];
    
//    quantityTxt.text = gQuantity;
//    shippingDateTxt.text = gShippingDate;
//    pickupLocationTxt.text = gPickupLocation;
//    connectionServiceTxt.text  = gConnectionService;
//    
//    if ([shippingDateTxt.text isEqualToString:@""]) {
//        shippingDateTxt.text = @"Please select date";
//    }
//    
//    if ([pickupLocationTxt.text isEqualToString:@""]) {
//        pickupLocationTxt.text = @"Please select location";
//    }
    
    shippingDateTxt.text = @"Please select date";
    pickupLocationTxt.text = @"Please select location";

    
	// Do any additional setup after loading the view.
    self.addedToCartAlert = [[UIAlertView alloc] initWithTitle:@"Added to Cart" message:@"Your item has been added to the shopping cart." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    self.updatedCartAlert = [[UIAlertView alloc] initWithTitle:@"Updated Cart" message:@"Your cart has been updated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
}

- (void)setupProductInformation
{
    if ([gProductID  isEqual: @"1"]) {
        currentProduct = @"Exchange";
    } else {
        currentProduct = @"Spare";
    }
    
    if ([gQuantity  isEqual: @""]) gQuantity = @"1";
    
    currentSection = @"ProductDetails";
    quantityTxt.text = @"1";
    [self apiCall:[NSString stringWithFormat:@"get_product_prices/zipcode/%@/",gShipZipCode]:currentSection];

//    [self btnChooseDifferentProductPressed:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    if (gShipZipCode.length < 1) {
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckAvailabilityViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:viewController animated:YES];
        return;
    }
    
    
    if (self.orderToEdit) {
//        if (self.orderToEdit.itemType == PTShoppingItemTypeExchange) {
//            gProductID = @"1";
//        } else if (self.orderToEdit.itemType == PTShoppingItemTypeSpareTank) {
//            gProductID = @"2";
//        }
        
        quantityTxt.text = gQuantity;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM-dd-yyyy"];
        NSDate *createTime = [dateFormatter dateFromString:gShippingDate];
        [dateFormatter setDateFormat:@"EEE, MMM dd"];
        NSString *strShippingDate = [dateFormatter stringFromDate:createTime];
        shippingDateTxt.text = strShippingDate;
        pickupLocationTxt.text = gPickupLocation;
        connectionServiceTxt.text  = gConnectionService;
        
        if ([shippingDateTxt.text isEqualToString:@""]) {
            shippingDateTxt.text = @"Please select date";
        }
        
        if ([pickupLocationTxt.text isEqualToString:@""]) {
            pickupLocationTxt.text = @"Please select location";
        }
        
        if (bConnectionService == YES)
            connectionServiceTxt.text = @"Connection Service ($4.99)";
        else
            connectionServiceTxt.text = @"No";


//        gProductID = [NSString stringWithFormat:@"%@", self.orderToEdit.productID];
//        [self setupProductInformation];
        
        self.changeProductImage.hidden = self.orderToEdit ? YES : NO;
        self.changeProductButton.hidden = self.orderToEdit ? YES : NO;
        self.changeProductButton.enabled = self.orderToEdit ? NO : YES;
    }
    
    selectedQuantity = -1;
    selectedShippingDate = -1;
    selectedTankLocation = -1;
    bConnectionService = NO;
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (IBAction)btnTestPressed:(id)sender{
    
    /* NSLog(@"\ngAPIString = %@,\ngIsLoggedIn = %d,\ngCustomerID = %@,\ngAuthToken = %@,\ngCardID = %@,\ngCartToken = %@,\ngServiceAreaID = %@,\ngLocationLKPID = %@,\ngShippingPrice = %@,\ngProductPrice = %@,\ngQuantity = %@,\ngShippingDate = %@,\ngPickupLocation = %@,\ngConnectionService = %@,\ngProductID = %@,\ngProductName = %@,\ngProductID_conn = %@,\ngProductName_conn = %@,\ngProductPrice_conn = %@,\ngFirstName = %@,\ngLastName = %@,\ngEmail = %@,\ngPassword = %@,\ngMobilePhone = %@,\ngHomePhone = %@,\ngShipAddress1 = %@,\ngShipAddress2 = %@,\ngShipCity = %@,\ngShipState = %@,\ngShipZipCode = %@,\ngIsAddressSameAsShipping = %d,\ngCardHolderName = %@,\ngCardType = %@,\ngCardNumber = %@,\ngCVV = %@,\ngExpirationMonth = %@,\ngDeliveryNotes = %@,\ngPromoCode = %@,\ngPayAddress1 = %@,\ngPayAddress2 = %@,\ngPayCity = %@,\ngPayState = %@,\ngPayZipCode =  %@"
          ,gAPIString,gIsLoggedIn,gCustomerID,gAuthToken,gCardID,gCartToken,gServiceAreaID,gLocationLKPID,gShippingPrice,gProductPrice,gQuantity,gShippingDate,gPickupLocation,gConnectionService,gProductID,gProductName,gProductID_conn,gProductName_conn,gProductPrice_conn,gFirstName,gLastName,gEmail,gPassword,gMobilePhone,gHomePhone,gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode,gIsAddressSameAsShipping,gCardHolderName,gCardType,gCardNumber,gCVV,gExpirationMonth,gDeliveryNotes,gPromoCode,gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode);
     */
}

#pragma mark - Protocol Conformance
#pragma mark PopOverListView
- (NSInteger)popoverListView:(ZSYPopoverListView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return itemArray.count;
}

- (UITableViewCell *)popoverListView:(ZSYPopoverListView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"identifier";
    UITableViewCell *cell = [tableView dequeueReusablePopoverCellWithIdentifier:identifier];
    if (nil == cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }

    if ([currentSection isEqualToString:@"Quantity"])
    {
        cell.textLabel.text = itemArray[indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:13];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
//        cell.textLabel.numberOfLines = 2;
    }
    else if ([currentSection isEqualToString:@"ShippingDate"])
    {
        UILabel *lblItem = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 150, 42)];
        UILabel *lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(160, 0, 110, 42)];
        
        NSString *date_formatted = itemArray[indexPath.row];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *createTime = [dateFormatter dateFromString:date_formatted];
        [dateFormatter setDateFormat:@"EEE, MMM dd"];
        NSString *strShippingDate = [dateFormatter stringFromDate:createTime];
        
        lblItem.text = strShippingDate;
        lblItem.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
        lblItem.textColor = [UIColor darkGrayColor];
        lblItem.numberOfLines = 2;
        
        lblPrice.text = priceArray[indexPath.row];
        lblPrice.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
        lblPrice.textColor = [UIColor darkGrayColor];
        lblPrice.numberOfLines = 2;
        
        [cell addSubview:lblItem];
        [cell addSubview:lblPrice];
        
    }
    else//if ([currentSection isEqualToString:@"ShippingDate"])
    {
        UILabel *lblItem = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 150, 42)];
        UILabel *lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(160, 0, 110, 42)];
        
        lblItem.text = itemArray[indexPath.row];
        lblItem.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
        lblItem.textColor = [UIColor darkGrayColor];
        lblItem.numberOfLines = 2;
        
        lblPrice.text = priceArray[indexPath.row];
        lblPrice.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
        lblPrice.textColor = [UIColor darkGrayColor];
        lblPrice.numberOfLines = 2;

        [cell addSubview:lblItem];
        [cell addSubview:lblPrice];
    }
    
    return cell;
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)popoverListView:(ZSYPopoverListView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    
    
    if ([currentSection isEqualToString:@"Quantity"])
    {
        quantityTxt.text = itemArray[indexPath.row];
        selectedQuantity = [quantityTxt.text floatValue];
        
        float productPrice = selectedQuantity * [gProductPrice floatValue];

        if (selectedShippingDate >= 0)
            productPrice += [[[shippingDateArray objectAtIndex:selectedShippingDate] objectForKey:@"price"] doubleValue];
        if (bConnectionService == YES)
            productPrice += 4.99;
        if (selectedTankLocation >= 0)
            productPrice += [pickupPriceArray[selectedTankLocation] doubleValue];

        lblProductPrice.text =  [NSString stringWithFormat:@"PRICE: $%.2f", productPrice];
    }

    if ([currentSection isEqualToString:@"ShippingDate"])
    {
        selectedShippingDate = indexPath.row;

        NSString *date_formatted = itemArray[indexPath.row];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *createTime = [dateFormatter dateFromString:date_formatted];
        
        [dateFormatter setDateFormat:@"MM-dd-yyyy"];
        gShippingDate = [dateFormatter stringFromDate:createTime];
        
        [dateFormatter setDateFormat:@"EEE, MMM dd"];
        NSString *strShippingDate = [dateFormatter stringFromDate:createTime];

        
        NSString *date = [NSString stringWithFormat:@"%@  (%@)", strShippingDate, priceArray[indexPath.row]];
        shippingDateTxt.text = date;


        float productPrice = [[[shippingDateArray objectAtIndex:selectedShippingDate] objectForKey:@"price"] doubleValue];
        
        productPrice += [quantityTxt.text floatValue] * [gProductPrice floatValue];
        if (bConnectionService == YES)
            productPrice += 4.99;
        if (selectedTankLocation >= 0)
            productPrice += [pickupPriceArray[selectedTankLocation] doubleValue];
        
        lblProductPrice.text =  [NSString stringWithFormat:@"PRICE: $%.2f", productPrice];
    }

    if ([currentSection isEqualToString:@"ConnectionServices"])
    {
        connectionServiceTxt.text = itemArray[indexPath.row];
        
//        if ([pickupLocationTxt.text rangeOfString:@"Connection" options:NSCaseInsensitiveSearch].location != NSNotFound)
        if ([connectionServiceTxt.text rangeOfString:@"Connection" options:NSCaseInsensitiveSearch].location != NSNotFound)
        {
            bConnectionService = YES;
            
            connectionServiceTxt.text = @"Connection Service ($4.99)";
        }
        else
        {
            bConnectionService = NO;
        }
        
        float productPrice = bConnectionService? 4.99 : 0;
        
        productPrice += [quantityTxt.text floatValue] * [gProductPrice floatValue];
        
        if (selectedShippingDate >= 0)
            productPrice += [[[shippingDateArray objectAtIndex:selectedShippingDate] objectForKey:@"price"] doubleValue];
        if (selectedTankLocation == YES)
            productPrice += [pickupPriceArray[selectedTankLocation] doubleValue];

        
        lblProductPrice.text =  [NSString stringWithFormat:@"PRICE: $%.2f", productPrice];

    }
    
    if ([currentSection isEqualToString:@"PickupLocation"])
    {
        selectedTankLocation = indexPath.row;
        pickupLocationTxt.text = itemArray[indexPath.row];
        gLocationLKPID = pickupLocationArray[indexPath.row];
        gShippingPrice = pickupPriceArray[indexPath.row];
        
        if ([pickupLocationTxt.text rangeOfString:@"other" options:NSCaseInsensitiveSearch].location != NSNotFound)
        {
            
            alertDelivery = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter the delivery location" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
            alertDelivery.alertViewStyle = UIAlertViewStylePlainTextInput;
            [alertDelivery show];
            
        }
        
        float productPrice = [gShippingPrice doubleValue];

        productPrice += [quantityTxt.text floatValue] * [gProductPrice floatValue];
        
        if (selectedShippingDate >= 0)
            productPrice += [[[shippingDateArray objectAtIndex:selectedShippingDate] objectForKey:@"price"] doubleValue];
        if (bConnectionService == YES)
            productPrice += 4.99;

        lblProductPrice.text =  [NSString stringWithFormat:@"PRICE: $%.2f", productPrice];

    }
    
    [tableView dismiss];
}

#pragma mark UIAlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    if (alertView == alertDelivery) {
        pickupLocationTxt.text =[[alertView textFieldAtIndex:0] text];
        // NSLog(@"pickupLocationTxt.text: %@",pickupLocationTxt.text);
    } else if (alertView == self.addedToCartAlert || alertView == self.updatedCartAlert) {
        [self processAPICall:@"getShoppingCart"];
    }
}

#pragma mark - IBActions
- (IBAction)btnQuantityPressed:(id)sender {
    
    currentSection = @"Quantity";
    [self processAPICall:currentSection];
    
}
- (IBAction)btnShippingDatePressed:(id)sender {
    
    currentSection = @"ShippingDate";
    
    [self apiCall:[NSString stringWithFormat:@"get_delivery_days/zipcode/%@",gShipZipCode]:currentSection];
    
}
- (IBAction)btnConnectionServicePressed:(id)sender {
    
    currentSection = @"ConnectionServices";
    [self apiCall:[NSString stringWithFormat:@"get_connection_service/"]:currentSection];
    
}
- (IBAction)btnPickupLocationPressed:(id)sender {
    
    currentSection = @"PickupLocation";
    
    NSString *URLString = @"get_delivery_locations/";
//    URLString = [NSString stringWithFormat:@"%@/Product_ID/%@", URLString, gProductID];
    if (bConnectionService == YES)
        URLString = [NSString stringWithFormat:@"%@%@", URLString, @"/Connection_Service/1"];
    else
        URLString = [NSString stringWithFormat:@"%@%@", URLString, @"/Connection_Service/0"];

    [self apiCall:URLString :currentSection];
}

- (IBAction)btnChooseDifferentProductPressed:(id)sender {

    currentSection = @"ProductDetails";
    
    if ([currentProduct isEqual:@"Exchange"]) {
        currentProduct = @"Spare";
        lblHeader.text = @"SPARE TANK ORDER PREFERENCES";
        
    } else {
        currentProduct = @"Exchange";
        lblHeader.text = @"EXCHANGE TANK ORDER PREFERENCES";
    }
    quantityTxt.text = @"1";
    
    [self apiCall:[NSString stringWithFormat:@"get_product_prices/zipcode/%@/",gShipZipCode]:currentSection];
    
    
}

- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }

    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}

- (IBAction)btnAddCartPressed:(id)sender{
    
    if ([shippingDateTxt.text isEqualToString: @"Please select date"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"Please select Shipping Date."
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return;
    }
    
    if (([pickupLocationTxt.text isEqualToString: @"Please select location"])||([pickupLocationTxt.text isEqualToString: @""]))  {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"Please select Pickup location."
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        [alert show];
        return;
    }
    // process
    
//    NSString *str = shippingDateTxt.text;
//    int dateLength = [str length];
//    
//    if (dateLength > 8) {
//        
//        NSRange range = NSMakeRange(5, 6);
//        str = [str substringWithRange:range];
////        str = [str substringFromIndex:4];
//        
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
//        [dateFormatter setDateFormat:@"MMM dd"];
//        NSDate *date = [dateFormatter dateFromString: str];
//        
//        dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"MM-dd"];
//        
//        NSString *convertedString = [dateFormatter stringFromDate:date];
////        gShippingDate = [NSString stringWithFormat:@"%@-14",convertedString];
//        gShippingDate = [NSString stringWithFormat:@"%@",convertedString];
//    }

    gQuantity = quantityTxt.text;
    gPickupLocation = pickupLocationTxt.text;
    
    gConnectionService = connectionServiceTxt.text;

    if (self.orderToEdit) {
//        [self updateCartItem:self.orderToEdit];
        
        currentSection = @"removeProduct";
        [self apiCall:[NSString stringWithFormat:@"remove_shopping_cart_item/customer_id/%@/shopping_cart_id/%@/cart_token/%@/auth_token/%@",gCustomerID,self.orderToEdit.orderNumber,gCartToken,gAuthToken]:currentSection];
        
    } else {
        if (gIsLoggedIn)
        {
            
            NSString *URLString = @"add_product";
            URLString = [NSString stringWithFormat:@"%@/customer_id/%@", URLString, gCustomerID];
            URLString = [NSString stringWithFormat:@"%@/cartToken/%@", URLString, gCartToken];
            URLString = [NSString stringWithFormat:@"%@/product_id/%@", URLString, gProductID];
            URLString = [NSString stringWithFormat:@"%@/quantity/%@", URLString, gQuantity];
            URLString = [NSString stringWithFormat:@"%@/price/%@", URLString, gProductPrice];
            URLString = [NSString stringWithFormat:@"%@/shipping_day/%@", URLString, gShippingDate];
            URLString = [NSString stringWithFormat:@"%@/location_id/%@", URLString, gLocationLKPID];
            URLString = [NSString stringWithFormat:@"%@/delivery_notes/%@", URLString, gDeliveryNotes];
            URLString = [NSString stringWithFormat:@"%@/auth_token/%@", URLString, gAuthToken];
            
            currentSection = @"addProduct";
            [self apiCall:URLString :currentSection];
            
            
            // location fee and Connection Service
//            if (mUsesConnectionService) {
//                propaneServer.addProduct(String.valueOf(mConnectionService.product_id), 1, mConnectionService.price,
//                                         mDeliveryDay.date_formatted, mLocation.location_lkp_id);
//            }
            
            float productPrice = [gShippingPrice doubleValue];
            if (productPrice > 0)
            {
                NSString *URLString = @"add_product";
                URLString = [NSString stringWithFormat:@"%@/customer_id/%@", URLString, gCustomerID];
                URLString = [NSString stringWithFormat:@"%@/cartToken/%@", URLString, gCartToken];
                URLString = [NSString stringWithFormat:@"%@/product_id/%@", URLString, @"LOCATIONFEE"];
                URLString = [NSString stringWithFormat:@"%@/quantity/%@", URLString, @"1"];
                URLString = [NSString stringWithFormat:@"%@/price/%@", URLString, gShippingPrice];
                URLString = [NSString stringWithFormat:@"%@/shipping_day/%@", URLString, gShippingDate];
                URLString = [NSString stringWithFormat:@"%@/location_id/%@", URLString, gLocationLKPID];
                URLString = [NSString stringWithFormat:@"%@/delivery_notes/%@", URLString, gDeliveryNotes];
                URLString = [NSString stringWithFormat:@"%@/auth_token/%@", URLString, gAuthToken];
                
                currentSection = @"addFeeProduct";
                [self apiCall:URLString :currentSection];
            }

        } else {
            [self addItemToCart];
//            [self processAPICall:@"addProduct"];
            
            UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CheckoutAccountViewController"];
            [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];

        }
    }
}

#pragma mark - ShoppingCart Management

- (void)addItemToCart
{
    PTShoppingCart *cart = [PTShoppingCart sharedCart];
    PTShoppingItem *item = [[PTShoppingItem alloc] init];
    item.productID = gProductID;
    item.quantity = [NSNumber numberWithInteger:gQuantity.integerValue];
//    item.itemType = [currentProduct isEqualToString:@"Spare"] ? PTShoppingItemTypeSpareTank : PTShoppingItemTypeExchange;
    item.itemName = [currentProduct isEqualToString:@"Spare"] ? @"TANK - SPARE" : @"GRILL TANK - EXCHANGE";
    item.pricePerItem = @(gProductPrice.floatValue);
    item.additions = @"";
    item.discount = @0;
    item.shipping = gShippingPrice;
    item.shippingDate = [gShippingDate copy];
    item.pickupLocation = [gPickupLocation copy];
    item.pickupLocationId = [gLocationLKPID copy];
    item.connectionService = [gConnectionService copy];
    [cart addItem:[item copy]];
}

- (void)loadCartItem
{
    PTShoppingItem *itemToUpdate = [[PTShoppingCart sharedCart] itemByCartID:self.orderToEdit.orderNumber];
    self.orderToEdit = itemToUpdate;
}

- (void)updateCartItem:(PTShoppingItem*)itemToUpdate
{
    itemToUpdate.quantity = [NSNumber numberWithInteger:gQuantity.integerValue];
    itemToUpdate.itemName = [currentProduct isEqualToString:@"Spare"] ? @"TANK - SPARE" : @"GRILL TANK - EXCHANGE";
    itemToUpdate.pricePerItem = @(gProductPrice.floatValue);
    itemToUpdate.additions = @"";
    itemToUpdate.discount = @0;
    itemToUpdate.shipping = gShippingPrice;
    itemToUpdate.shippingDate = [gShippingDate copy];
    itemToUpdate.pickupLocation = [gPickupLocation copy];
    itemToUpdate.pickupLocationId = [gLocationLKPID copy];
    itemToUpdate.connectionService = [gConnectionService copy];
    
    if (gIsLoggedIn) {
        NSString *updateString = [NSString stringWithFormat:@"/update_shopping_cart_item/customer_id/%@/shopping_cart_id/%@/cart_token/%@/location_lkp_id/%@/quantity/%@/auth_token/%@",gCustomerID,itemToUpdate.orderNumber,gCartToken,gLocationLKPID,itemToUpdate.quantity,gAuthToken];
        [self apiCall:updateString :@"updateCart"];
    } else {
        [self processAPICall:@"updateCart"];
    }
}

#pragma mark - API Calls

- (void)apiCallPost:(NSString *)URLString postString:(NSString *)postString requestType:(NSString *)currentSection {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    [apiResponseArray removeAllObjects];
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    // NSLog(@"currentSection: %@",currentSection);
    // NSLog(@"URLString: %@",URLString);
    // NSLog(@"postString: %@",postString);
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        //       NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:currentSection];
            DLog(@"\napiresponsearray %@", apiResponseArray);
        });
    });
}

- (void)apiCall:(NSString *)URLString :(NSString *)requestFor  {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    [apiResponseArray removeAllObjects];
    
    activityIndicator.hidden = NO;
    [activityIndicator startAnimating];
    activityIndicator.hidesWhenStopped = YES;
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];

    // NSLog(@"%@",URLString);
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        if (response == nil)
            return;
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            
//             NSLog(@"apiResponseArray: %@",apiResponseArray);
            [self processAPICall:requestFor];
            
            [activityIndicator stopAnimating];
            
        });
    });
}


- (void) processAPICall:(NSString *)requestFor  {
    
    
    if ([requestFor isEqualToString:@"Quantity"]) {
        [itemArray removeAllObjects];
        [priceArray removeAllObjects];
        
        for (int i = 1; i < 31; i ++)
            [itemArray addObject:[NSString stringWithFormat:@"%d", i]];
        
        ZSYPopoverListView *listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 200, 250)];
        listView.titleName.text = @"Select Quantity";
        listView.datasource = self;
        listView.delegate = self;
        
        [listView show];
        
    }
    
    if ([requestFor isEqualToString:@"ShippingDate"]) {
        
        [itemArray removeAllObjects];
        [priceArray removeAllObjects];
        
        
        for (NSDictionary *dict in apiResponseArray) {
            double priceValue = [[dict objectForKey:@"price"] doubleValue];
            
            NSString *item = [NSString stringWithFormat:@"%@",[dict objectForKey:@"date_formatted"]];
            NSString *price;
            
            if (priceValue == 0)
                price = [NSString stringWithFormat:@"%@", @"FREE"];
            else if (priceValue < 0)
                price = [NSString stringWithFormat:@"Save $%.0f / tank", fabs(priceValue)];
            else
                price = [NSString stringWithFormat:@"$%.02f", priceValue];
            
            [itemArray addObject:item];
            [priceArray addObject:price];

            [shippingDateArray addObject:dict];
        }
        
        int height = [itemArray count] * 45 + 30;
        ZSYPopoverListView *listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 280, height)];
        
        listView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"order_popup.png"]];
        
        listView.titleName.text = @"Select Delivery Date";
        listView.datasource = self;
        listView.delegate = self;
        
        [listView show];
        
    }
    
    if ([requestFor isEqualToString:@"PickupLocation"]) {
        
        [itemArray removeAllObjects];
        [priceArray removeAllObjects];
        [pickupLocationArray removeAllObjects];
        [pickupPriceArray removeAllObjects];
        
        for (NSDictionary *dict in apiResponseArray) {
            
            NSString *locationID = [NSString stringWithFormat:@"%@",[dict objectForKey:@"location_lkp_id"]];
            
            NSString *pickupPrice = [NSString stringWithFormat:@"%@",[dict objectForKey:@"price"]];
            
            NSString *userFriendlyPrice = @"";
            
            if ([pickupPrice isEqualToString:@"0"]) {
                userFriendlyPrice = @"FREE";
            } else {
                userFriendlyPrice = [NSString stringWithFormat:@"$%@",pickupPrice];
            }
            
            [itemArray addObject:[dict objectForKey:@"description"]];
            [priceArray addObject:userFriendlyPrice];
            
            [pickupLocationArray addObject:locationID];
            [pickupPriceArray addObject:pickupPrice];
        }
        
        int height = [itemArray count] * 45 + 30;
        ZSYPopoverListView *listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 280, height)];
        listView.titleName.text = @"Select Pickup Location";
        listView.datasource = self;
        listView.delegate = self;
        
        [listView show];
    }
    
    if ([requestFor isEqualToString:@"ConnectionServices"]) {
        
        [itemArray removeAllObjects];
        [priceArray removeAllObjects];

        [itemArray addObject:@"No"];
        [priceArray addObject:@""];

        NSString *price = [NSString stringWithFormat:@"$%@", [[apiResponseArray valueForKey:@"price"] stringValue]];
        [itemArray addObject:[apiResponseArray valueForKey:@"product_name"]];
        [priceArray addObject:price];
        
        gProductID_conn = [[apiResponseArray valueForKey:@"product_id"]stringValue];
        gProductName_conn = [apiResponseArray valueForKey:@"product_name"];
        gProductPrice_conn = [[apiResponseArray valueForKey:@"price"] stringValue];
        
        int height = [itemArray count] * 45 + 30;
        ZSYPopoverListView *listView = [[ZSYPopoverListView alloc] initWithFrame:CGRectMake(0, 0, 280, height)];
        listView.titleName.text = @"Please Select";
        listView.datasource = self;
        listView.delegate = self;
        
        [listView show];
    }
    
    if ([requestFor isEqualToString:@"ConnectionServicesForced"]) {
        
        [itemArray removeAllObjects];
        [priceArray removeAllObjects];
        
        [itemArray addObject:[apiResponseArray valueForKey:@"product_name"]];
        [priceArray addObject:[[apiResponseArray valueForKey:@"price"] stringValue]];
        
        [itemArray addObject:@"No"];
        [priceArray addObject:@""];
        
        gProductID_conn = [[apiResponseArray valueForKey:@"product_id"]stringValue];
        gProductName_conn = [apiResponseArray valueForKey:@"product_name"];
        gProductPrice_conn = [[apiResponseArray valueForKey:@"price"] stringValue];
        
    }
    
    if ([requestFor isEqualToString:@"ProductDetails"]) {
        
        if ([currentProduct isEqual:@"Exchange"]) {
            // show exchange product details and price
            
            gProductID = @"1";
            
            for (NSDictionary *dict in apiResponseArray) {
                
                NSString* product_id = [NSString stringWithFormat:@"%@",[dict objectForKey:@"product_id"]];
                
                if ([product_id isEqualToString:gProductID]) {
                    
                    gProductName = [dict objectForKey:@"product_name"];
                    lblProductName.text = [dict objectForKey:@"product_name"];
                    lblProductDescription.text = @"Exchange your empty tank for our inspected,\nprecision-filled grill tank. Our tanks are filled\nto a net propane weight of 15lbs.";
                    gProductPrice = [[dict objectForKey:@"price"]  stringValue];
                    lblProductPrice.text = [NSString stringWithFormat:@"PRICE: $%@",gProductPrice];
                }
            }
        } else {
            // spare
            gProductID = @"2";
            for (NSDictionary *dict in apiResponseArray) {
                
                NSString* product_id = [ (NSNumber*)[dict objectForKey:@"product_id"] stringValue];
                // NSLog(@"ok: %@,%@,%@",[dict objectForKey:@"product_id"],gProductID, product_id);
                
                if ([product_id isEqualToString:gProductID]) {
                    
                    gProductName = [dict objectForKey:@"product_name"];
                    lblProductName.text = [dict objectForKey:@"product_name"];
                    lblProductDescription.text = @"A Spare tank allows you to have an extra tank at your disposal.";
                    gProductPrice = [[dict objectForKey:@"price"]  stringValue];
                    lblProductPrice.text = [NSString stringWithFormat:@"PRICE: $%@",gProductPrice];
                }
            }
            
        }
        
        float productPrice = 0;
        
        productPrice += [quantityTxt.text floatValue] * [gProductPrice floatValue];
        if (bConnectionService == YES)
            productPrice += 4.99;
        if (selectedShippingDate >= 0)
            productPrice += [[[shippingDateArray objectAtIndex:selectedShippingDate] objectForKey:@"price"] doubleValue];
        if (selectedTankLocation >= 0)
            productPrice += [pickupPriceArray[selectedTankLocation] doubleValue];
        
        lblProductPrice.text =  [NSString stringWithFormat:@"PRICE: $%.2f", productPrice];

        
    }
    
    if ([requestFor isEqualToString:@"addProduct"]) {
         NSLog(@"addProduct - apiResponseArray: %@",apiResponseArray);
        
        if (bConnectionService == YES )
        {
            // NSLog(@"here");
            
            NSString *URLString = @"add_product";
            URLString = [NSString stringWithFormat:@"%@/customer_id/%@", URLString, gCustomerID];
            URLString = [NSString stringWithFormat:@"%@/cartToken/%@", URLString, gCartToken];
            URLString = [NSString stringWithFormat:@"%@/product_id/%@", URLString, gProductID_conn];
            URLString = [NSString stringWithFormat:@"%@/quantity/%@", URLString, @"1"];
            URLString = [NSString stringWithFormat:@"%@/price/%@", URLString, gProductPrice_conn];
            URLString = [NSString stringWithFormat:@"%@/shipping_day/%@", URLString, gShippingDate];
            URLString = [NSString stringWithFormat:@"%@/location_id/%@", URLString, gLocationLKPID];
            URLString = [NSString stringWithFormat:@"%@/delivery_notes/%@", URLString, gDeliveryNotes];
            URLString = [NSString stringWithFormat:@"%@/auth_token/%@", URLString, gAuthToken];

            currentSection = @"addConnectionService";
            [self apiCall:URLString :currentSection];

        } else {
            
            if (self.orderToEdit)
                [self.updatedCartAlert show];
            else
                [self.addedToCartAlert show];
        }
    }
    
    if ([requestFor isEqualToString:@"addConnectionService"]) {
        // NSLog(@"addConnectionService - apiResponseArray: %@",apiResponseArray);
        
        if (self.orderToEdit)
            [self.updatedCartAlert show];
        else
            [self.addedToCartAlert show];
    }
    
    if ([requestFor isEqualToString:@"getShoppingCart"]) {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CartViewController"];
        [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    }
    
    if ([requestFor isEqualToString:@"removeProduct"]) {
        gCartID = @"";
        
        if (![gCartID_conn isEqualToString:@""]) {
            NSString *apiUrl = [NSString stringWithFormat:@"remove_shopping_cart_item/customer_id/%@/shopping_cart_id/%@/cart_token/%@/auth_token/%@",gCustomerID, gCartID_conn,gCartToken,gAuthToken];
            
            currentSection = @"removeProduct_conn";
            [self apiCall:apiUrl:currentSection];
        }
        else
        {
            // Test
            NSString *URLString = @"add_product";
            URLString = [NSString stringWithFormat:@"%@/customer_id/%@", URLString, gCustomerID];
            URLString = [NSString stringWithFormat:@"%@/cartToken/%@", URLString, gCartToken];
            URLString = [NSString stringWithFormat:@"%@/product_id/%@", URLString, gProductID];
            URLString = [NSString stringWithFormat:@"%@/quantity/%@", URLString, gQuantity];
            URLString = [NSString stringWithFormat:@"%@/price/%@", URLString, gProductPrice];
            URLString = [NSString stringWithFormat:@"%@/shipping_day/%@", URLString, gShippingDate];
            URLString = [NSString stringWithFormat:@"%@/location_id/%@", URLString, gLocationLKPID];
            URLString = [NSString stringWithFormat:@"%@/delivery_notes/%@", URLString, gDeliveryNotes];
            URLString = [NSString stringWithFormat:@"%@/auth_token/%@", URLString, gAuthToken];
            
            currentSection = @"addProduct";
            [self apiCall:URLString :currentSection];

        }
    }
    
    if ([requestFor isEqualToString:@"removeProduct_conn"]) {
        NSLog(@"\nremoveCart_conn: %@", apiResponseArray);
        gCartID_conn = @"";
        
        // Test
        NSString *URLString = @"add_product";
        URLString = [NSString stringWithFormat:@"%@/customer_id/%@", URLString, gCustomerID];
        URLString = [NSString stringWithFormat:@"%@/cartToken/%@", URLString, gCartToken];
        URLString = [NSString stringWithFormat:@"%@/product_id/%@", URLString, gProductID];
        URLString = [NSString stringWithFormat:@"%@/quantity/%@", URLString, gQuantity];
        URLString = [NSString stringWithFormat:@"%@/price/%@", URLString, gProductPrice];
        URLString = [NSString stringWithFormat:@"%@/shipping_day/%@", URLString, gShippingDate];
        URLString = [NSString stringWithFormat:@"%@/location_id/%@", URLString, gLocationLKPID];
        URLString = [NSString stringWithFormat:@"%@/delivery_notes/%@", URLString, gDeliveryNotes];
        URLString = [NSString stringWithFormat:@"%@/auth_token/%@", URLString, gAuthToken];
        
        currentSection = @"addProduct";
        [self apiCall:URLString :currentSection];
    }

    if ([requestFor isEqualToString:@"updateCart"]) {
        [self.updatedCartAlert show];
    }
    
    // NSLog(@"apiResponseArray: %@",apiResponseArray);
}


- (IBAction)btnAddPromoPressed:(id)sender{

}

- (IBAction)btnClosePopupPressed:(id)sender{
        self.popupView.hidden = YES;

}

@end
