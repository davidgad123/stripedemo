//
//  OrderConfirmViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderConfirmViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;


@property (strong,nonatomic) IBOutlet UILabel *lblOrderNumberTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblTotalTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblDeliveryDateTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblItemsOrderedTitle;

@property (strong,nonatomic) IBOutlet UILabel *lblOrderNumber;
@property (strong,nonatomic) IBOutlet UILabel *lblTotal;
@property (strong,nonatomic) IBOutlet UILabel *lblDeliveryDate;
@property (strong,nonatomic) IBOutlet UILabel *lblItemsOrdered;


- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;

@end
