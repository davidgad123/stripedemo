//
//  OrderViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/5/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSYPopoverListView.h"

@interface OrderViewController : UIViewController<ZSYPopoverListDatasource, ZSYPopoverListDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;

@property (nonatomic,retain)IBOutlet UIView *fieldsView;
@property (nonatomic,retain)IBOutlet UIScrollView *popupView;
@property (nonatomic,retain)IBOutlet UIView *addCartView;

@property (weak, nonatomic) IBOutlet UIButton *btnAddCart;
@property (weak, nonatomic) IBOutlet UIButton *btnClosePopup;

@property (weak, nonatomic) IBOutlet UIButton *btnQuantity;
@property (weak, nonatomic) IBOutlet UIButton *btnShippingDate;
@property (weak, nonatomic) IBOutlet UIButton *btnConnectionService;
@property (weak, nonatomic) IBOutlet UIButton *btnPickupLocation;
@property (weak, nonatomic) IBOutlet UIButton *changeProductButton;

@property (strong, nonatomic) IBOutlet CustomUITextField *quantityTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *shippingDateTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *pickupLocationTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *connectionServiceTxt;

// product details
@property (strong,nonatomic) IBOutlet UILabel *lblProductName;
@property (strong,nonatomic) IBOutlet UILabel *lblProductDescription;
@property (strong,nonatomic) IBOutlet UILabel *lblProductPrice;
@property (weak, nonatomic) IBOutlet UIButton *btnChooseDifferentProduct;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;

// titles
@property (strong,nonatomic) IBOutlet UILabel *lblQuantityTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblShippingDateTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblPickupLocationTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblConnectionTitle;
@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (nonatomic, retain) NSIndexPath *selectedIndexPath;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) PTShoppingItem *orderToEdit;
@property (weak, nonatomic) IBOutlet UIImageView *changeProductImage;

- (IBAction)btnAddCartPressed:(id)sender;
- (IBAction)btnClosePopupPressed:(id)sender;

- (IBAction)btnQuantityPressed:(id)sender;
- (IBAction)btnShippingDatePressed:(id)sender;
- (IBAction)btnConnectionServicePressed:(id)sender;
- (IBAction)btnPickupLocationPressed:(id)sender;
- (IBAction)btnChooseDifferentProductPressed:(id)sender;

@end
