//
//  CheckoutSummaryViewController.m
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import "CheckoutSummaryViewController.h"
#import "CartViewController.h"

@interface CheckoutSummaryViewController ()

@end

@implementation CheckoutSummaryViewController

@synthesize btnHome;
@synthesize btnNav;
@synthesize myScrollView;
@synthesize btnPlaceOrder;

@synthesize btnChangeShipping;
@synthesize btnChangeBilling;
@synthesize btnChangePayment;
@synthesize btnApplyPromo;
@synthesize btnViewCart;

@synthesize shippingAddressTxt;
@synthesize billingAddressTxt;
@synthesize paymentInfoTxt;
@synthesize promoTxt;
@synthesize deliveryNotesTxt;

@synthesize lblShippingAddressTitle;
@synthesize lblBillingAddressTitle;
@synthesize lblPaymentInfoTitle;
@synthesize lblAdditionalDetailsTitle;
@synthesize lblDeliveryNotesTitle;
@synthesize lblPromoCodeTitle;
@synthesize lblSummaryTitle;

@synthesize lblPromoCodeStatus;

// summary labels
@synthesize lblSubTotal;
@synthesize lblTotal;
@synthesize lblTaxes;
@synthesize lblShipping;
@synthesize lblDeliveryDate;
@synthesize lblTankLocation;
@synthesize lblPromoCodeMessage;
@synthesize lblNewSubTotal;
@synthesize lblDiscountApplied;

// summary label titles
@synthesize lblSubTotalTitle;
@synthesize lblTotalTitle;
@synthesize lblTaxesTitle;
@synthesize lblShippingTitle;
@synthesize lblDeliveryDateTitle;
@synthesize lblTankLocationTitle;
@synthesize lblPromoCodeMessageTitle;
@synthesize lblNewSubTotalTitle;
@synthesize lblDiscountAppliedTitle;

// popup view
@synthesize popupView;

@synthesize address1Txt;
@synthesize address2Txt;
@synthesize cityTxt;
@synthesize stateTxt;
@synthesize zipTxt;

@synthesize expirationYearTxt;

@synthesize btnDone;
@synthesize btnCancel;

@synthesize lblAddress1Title;
@synthesize lblAddress2Title;
@synthesize lblCityTitle;
@synthesize lblStateTitle;
@synthesize lblZipTitle;
@synthesize lblHeader;
@synthesize lblExpirationYearTitle;

@synthesize activityIndicator;

NSString *buttonPressed = @"";

@synthesize pickerState;
@synthesize pickerCardMonth;
@synthesize pickerCardYear;
@synthesize pickerCardType;

NSMutableArray *arrayState;
NSMutableArray *arrayMonth;
NSMutableArray *arrayYear;
NSMutableArray *arrayCardType;

@synthesize whiteBack1;
@synthesize whiteBack2;
@synthesize whiteBack3;
@synthesize whiteBack4;
@synthesize greenBack;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //
    LeftViewController *left = [[LeftViewController alloc] initWithStyle:UITableViewStylePlain];
    [self.revealSideViewController preloadViewController:left forSide:PPRevealSideDirectionLeft];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (IBAction)navBtnPressed:(id)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}



- (void) apiCall:(NSString *)URLString :(NSString *)requestFor {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    [apiResponseArray removeAllObjects];
    
    CGRect bounds = activityIndicator.superview.bounds;
    activityIndicator.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    
    activityIndicator.hidden = NO;
    [activityIndicator startAnimating];
    activityIndicator.hidesWhenStopped = YES;
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:requestFor];
        });
    });
}

- (void) processAPICall:(NSString *)requestFor  {
    
    
    if ([requestFor isEqualToString:@"shipping"]) {

    // NSLog(@"shipping - apiResponseArray: %@",apiResponseArray);
        
        gShipAddress1 = [apiResponseArray valueForKey:@"ship_address_1"];
        gShipAddress2 = [apiResponseArray valueForKey:@"ship_address_2"];
        gShipCity = [apiResponseArray valueForKey:@"ship_city"];
        gShipState = [apiResponseArray valueForKey:@"ship_state"];
        gShipZipCode = [[apiResponseArray valueForKey:@"ship_zip"] stringValue];
                        
        gPayAddress1 = [apiResponseArray valueForKey:@"address_1"];
        gPayAddress2 = [apiResponseArray valueForKey:@"address_2"];
        gPayCity = [apiResponseArray valueForKey:@"city"];
        gPayState = [apiResponseArray valueForKey:@"state"];
        gPayZipCode = [[apiResponseArray valueForKey:@"zip"] stringValue];
    
    shippingAddressTxt.text = [NSString stringWithFormat:@"%@ %@ \n%@ %@ %@",gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode];
    billingAddressTxt.text = [NSString stringWithFormat:@"%@ %@ \n%@ %@ %@",gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode];
    
        [self apiCall:[NSString stringWithFormat:@"/get_credit_card/customer_id/%@/auth_token/%@",gCustomerID,gAuthToken]:@"payment"];

    }
    if ([requestFor isEqualToString:@"payment"]) {
        
//        gCardHolderName = [apiResponseArray valueForKey:@"email"];
        gCardNumber = [apiResponseArray valueForKey:@"cardlastfour"];
        gCardType = [apiResponseArray valueForKey:@"cardType"];
//        gCVV = [apiResponseArray valueForKey:@"email"];
        gExpirationMonth = [apiResponseArray valueForKey:@"cardexpmonth"];
        gExpirationYear = [apiResponseArray valueForKey:@"cardexpyear"];
            gCVV = @"";
            
            NSString *cardType = @"";
            if ([gCardType isEqualToString:@"V"]) cardType = @"Visa";
            if ([gCardType isEqualToString:@"A"]) cardType = @"Amex";
            if ([gCardType isEqualToString:@"M"]) cardType = @"Mastercard";
            if ([gCardType isEqualToString:@"D"]) cardType = @"Discover";
            
    paymentInfoTxt.text = [NSString stringWithFormat:@"Card: %@ ending in %@\nExpiration: %@/%@",cardType, gCardNumber,gExpirationMonth, gExpirationYear];
            NSString *apiUrl = [NSString stringWithFormat:@"/get_shopping_cart/customer_id/%@/cart_token/%@/service_area_id/%@/auth_token/%@",gCustomerID,gCartToken,gServiceAreaID,gAuthToken];
        [self apiCall:apiUrl:@"totals"];
    
        }
    
    if ([requestFor isEqualToString:@"totals"]) {
        
//        NSLog(@"totals - apiResponseArray: %@",apiResponseArray);
        
        lblSubTotal.text = [NSString stringWithFormat:@"$%.2f", [[[apiResponseArray valueForKey:@"cart_data"] valueForKey:@"Total_Price_Amt"] floatValue]];
        lblTotal.text = [NSString stringWithFormat:@"$%.2f", [[[apiResponseArray valueForKey:@"cart_data"] valueForKey:@"Total_Cart_Amt"] floatValue]];
        lblTaxes.text = [NSString stringWithFormat:@"$%.2f", [[[apiResponseArray valueForKey:@"cart_data"] valueForKey:@"Total_Tax_Handling_Amt"] floatValue]];
        lblDiscountApplied.text = [NSString stringWithFormat:@"$%.2f",[[[apiResponseArray valueForKey:@"cart_data"] valueForKey:@"Total_Discount"] floatValue]];
        
        id cartData = [[[apiResponseArray valueForKey:@"cart_data"] objectForKey:@"cart"] objectForKey:@"DATA"];
        [self processCartData:cartData];

        
//        gTotal = lblTotal.text;
//        
//        lblNewSubTotal.text = [NSString stringWithFormat:@"%.2f",  [lblSubTotal.text floatValue] + [lblDiscountApplied.text floatValue]];
//        
//        lblSubTotal.text = [NSString stringWithFormat:@"%.2f",[lblSubTotal.text floatValue]];
//        lblTotal.text = [NSString stringWithFormat:@"%.2f",[lblTotal.text floatValue]];
    }
    
    if ([requestFor isEqualToString:@"confirmOrder"]) {
        // NSLog(@"confirmOrder - apiResponseArray: %@",apiResponseArray);
        // if good go to confirmed screen.
        
        if (![[[apiResponseArray valueForKey:@"order_id"]  stringValue] length] > 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: @"Unknown error. Please contact support."
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            [alert show];
            activityIndicator.hidden = YES;
            return;
            
        }
        
            
        if (![[apiResponseArray valueForKey:@"msg"] isEqualToString:@"error"]) {
            
            gOrderID = [[apiResponseArray valueForKey:@"order_id"] stringValue];
            gTotal = [[apiResponseArray valueForKey:@"total_amount"] stringValue];
            
            UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderConfirmViewController"];
            [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
            
        } else {
            
            NSString *errorMsg =[apiResponseArray valueForKey:@"msg_info"];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: [NSString stringWithFormat: @"Error: %@",errorMsg]
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            [alert show];
            activityIndicator.hidden = YES;
            return;
        }
    }
    
    if ([requestFor isEqualToString:@"addCard"]) {
        // NSLog(@"addCard - apiResponseArray: %@",apiResponseArray);
        
        if ([[apiResponseArray valueForKey:@"msg"]  isEqual: @"success"]) {
            
        } else {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                           message: @"Please check the credit card details."
                                                          delegate: self
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
            [alert show];
            activityIndicator.hidden = YES;
            return;
        }
    }
    
    if ([requestFor isEqualToString:@"checkPromo"]) {
        // NSLog(@"checkPromo - apiResponseArray: %@",apiResponseArray);
        
        lblPromoCodeMessage.text = [[[apiResponseArray valueForKey:@"promo_code_add"] valueForKey:@"args"]valueForKey:@"promo_msg"];
        
        // NSLog(@"lblPromoCodeMessage.text: %@",lblPromoCodeMessage.text);
    }
    

    activityIndicator.hidden = YES;
}


- (IBAction)btnTestPressed:(id)sender{
    
    /* NSLog(@"\ngAPIString = %@,\ngIsLoggedIn = %d,\ngCustomerID = %@,\ngAuthToken = %@,\ngCardID = %@,\ngCartToken = %@,\ngServiceAreaID = %@,\ngLocationLKPID = %@,\ngShippingPrice = %@,\ngProductPrice = %@,\ngQuantity = %@,\ngShippingDate = %@,\ngPickupLocation = %@,\ngConnectionService = %@,\ngProductID = %@,\ngProductName = %@,\ngProductID_conn = %@,\ngProductName_conn = %@,\ngProductPrice_conn = %@,\ngFirstName = %@,\ngLastName = %@,\ngEmail = %@,\ngPassword = %@,\ngMobilePhone = %@,\ngHomePhone = %@,\ngShipAddress1 = %@,\ngShipAddress2 = %@,\ngShipCity = %@,\ngShipState = %@,\ngShipZipCode = %@,\ngIsAddressSameAsShipping = %d,\ngCardHolderName = %@,\ngCardType = %@,\ngCardNumber = %@,\ngCVV = %@,\ngExpirationMonth = %@,\ngDeliveryNotes = %@,\ngPromoCode = %@,\ngPayAddress1 = %@,\ngPayAddress2 = %@,\ngPayCity = %@,\ngPayState = %@,\ngPayZipCode =  %@"
          ,gAPIString,gIsLoggedIn,gCustomerID,gAuthToken,gCardID,gCartToken,gServiceAreaID,gLocationLKPID,gShippingPrice,gProductPrice,gQuantity,gShippingDate,gPickupLocation,gConnectionService,gProductID,gProductName,gProductID_conn,gProductName_conn,gProductPrice_conn,gFirstName,gLastName,gEmail,gPassword,gMobilePhone,gHomePhone,gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode,gIsAddressSameAsShipping,gCardHolderName,gCardType,gCardNumber,gCVV,gExpirationMonth,gDeliveryNotes,gPromoCode,gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode);
     */
}
- (BOOL)textFieldShouldReturn:(CustomUITextField *)textField {
    [textField resignFirstResponder];
 //   [self.view endEditing:YES];
    return NO;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [viewControllersArray addObject:@"CheckoutSummaryViewController"];
    
    lblHeader.font = [UIFont fontWithName:@"HelveticaNeueLTCom-Blk" size:12];
    lblHeader.textColor = [UIColor darkGrayColor];

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]]];

    activityIndicator.hidden = YES;

    UIButton *btnTest = [[UIButton alloc]initWithFrame:CGRectMake(250, 25, 50,20)];
    [btnTest setTitle:@"Test" forState:UIControlStateNormal];
    [btnTest addTarget:self action:@selector(btnTestPressed:)forControlEvents:UIControlEventTouchUpInside];
    // [self.view addSubview:btnTest];
    
    if (SH > 500) {
        myScrollView.contentSize = CGSizeMake(320,800);
    } else {
        myScrollView.contentSize = CGSizeMake(320,900);
    }
    
    myScrollView.frame = CGRectMake(0, delta+120, 320, 480);
    
    btnPlaceOrder.frame = CGRectMake(10, delta+710, 294, 47);

    btnChangeShipping= [[UIButton alloc]initWithFrame:CGRectMake(12, delta+75, 296, 36)];
    btnChangeBilling= [[UIButton alloc]initWithFrame:CGRectMake(12, delta+180, 296, 36)];
    btnChangePayment= [[UIButton alloc]initWithFrame:CGRectMake(12, delta+285, 296, 36)];
    btnApplyPromo= [[UIButton alloc]initWithFrame:CGRectMake(233, delta+449, 75, 36)];
 //   btnViewCart= [[UIButton alloc]initWithFrame:CGRectMake(10, delta+430, 270, 20)];
    
    [btnChangeShipping addTarget:self action:@selector(btnChangeShippingPressed:)forControlEvents:UIControlEventTouchDown];
    [btnChangeBilling addTarget:self action:@selector(btnChangeBillingPressed:)forControlEvents:UIControlEventTouchDown];
    [btnChangePayment addTarget:self action:@selector(btnChangePaymentPressed:)forControlEvents:UIControlEventTouchDown];
    [btnApplyPromo addTarget:self action:@selector(btnApplyPromoPressed:)forControlEvents:UIControlEventTouchDown];
    [btnViewCart addTarget:self action:@selector(btnViewCartPressed:)forControlEvents:UIControlEventTouchDown];
    
//    [btnChangeShipping setTitle:@"Change Shipping" forState:UIControlStateNormal];
//    [btnChangeBilling setTitle:@"Change Billing" forState:UIControlStateNormal];
//    [btnChangePayment setTitle:@"Chnage Payment" forState:UIControlStateNormal];
//    [btnApplyPromo setTitle:@"Add" forState:UIControlStateNormal];
////    [btnViewCart setTitle:@"View Cart" forState:UIControlStateNormal];
//
//    btnChangeShipping.backgroundColor = [UIColor darkGrayColor];
//    btnChangeBilling.backgroundColor = [UIColor darkGrayColor];
//    btnChangePayment.backgroundColor = [UIColor darkGrayColor];
//    btnApplyPromo.backgroundColor = [UIColor darkGrayColor];
    
    [btnChangeBilling setBackgroundImage:[UIImage imageNamed:@"checkout_chooseDiffBilling.png"]
                        forState:UIControlStateNormal];
    [btnChangeShipping setBackgroundImage:[UIImage imageNamed:@"checkout_chooseDiffShipping.png"]
                                forState:UIControlStateNormal];
    [btnChangePayment setBackgroundImage:[UIImage imageNamed:@"checkout_chooseDiffPayment.png"]
                                forState:UIControlStateNormal];
    [btnApplyPromo setBackgroundImage:[UIImage imageNamed:@"payment_apply.png"]
                                forState:UIControlStateNormal];

    
    shippingAddressTxt= [[UITextView alloc]initWithFrame:CGRectMake(12, delta+30, 296, 40)];
    billingAddressTxt= [[UITextView alloc]initWithFrame:CGRectMake(12, delta+135, 296, 40)];
    paymentInfoTxt= [[UITextView alloc]initWithFrame:CGRectMake(12, delta+240, 296, 40)];
    deliveryNotesTxt= [[UITextView alloc]initWithFrame:CGRectMake(12, delta+380, 296, 40)];
    promoTxt= [[CustomUITextField alloc]initWithFrame:CGRectMake(12, delta+450, 215, 34)];

    promoTxt.background = [UIImage imageNamed:@"payment_promoFieldBack.png"];
    
//    [promoTxt setBorderStyle:UITextBorderStyleRoundedRect];

    
    shippingAddressTxt.userInteractionEnabled = NO;
    billingAddressTxt.userInteractionEnabled = NO;
    paymentInfoTxt.userInteractionEnabled = NO;
    
    shippingAddressTxt.backgroundColor = [UIColor whiteColor];
    billingAddressTxt.backgroundColor = [UIColor whiteColor];
    paymentInfoTxt.backgroundColor = [UIColor whiteColor];
    
    lblShippingAddressTitle= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+10, 270, 20)];
    lblBillingAddressTitle= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+115, 270, 20)];
    lblPaymentInfoTitle= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+220, 290, 20)];
    lblAdditionalDetailsTitle= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+330, 270, 20)];
    lblDeliveryNotesTitle= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+355, 270, 20)];
    lblPromoCodeTitle= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+430, 270, 20)];
    lblSummaryTitle= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+487, 270, 20)];
    
    lblShippingAddressTitle.text = @"Shipping Address";
    lblBillingAddressTitle.text = @"Billing Address";
    lblPaymentInfoTitle.text = @"Payment Information";
    lblAdditionalDetailsTitle.text = @"ADDITIONAL DETAILS";
    lblDeliveryNotesTitle.text = @"Delivery Notes";
    lblPromoCodeTitle.text = @"Promo Code";
    lblSummaryTitle.text = @"SUMMARY";
    
    lblShippingAddressTitle.textColor = customBlueColor;
    lblBillingAddressTitle.textColor = customBlueColor;
    lblPaymentInfoTitle.textColor = customBlueColor;
    lblAdditionalDetailsTitle.textColor = [UIColor blackColor];
    lblDeliveryNotesTitle.textColor = customBlueColor;
    lblPromoCodeTitle.textColor = customBlueColor;
    lblSummaryTitle.textColor = [UIColor blackColor];
    
    lblShippingAddressTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblBillingAddressTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblPaymentInfoTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblAdditionalDetailsTitle.font =  [UIFont boldSystemFontOfSize:14.0f];
    lblDeliveryNotesTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblPromoCodeTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblSummaryTitle.font =  [UIFont boldSystemFontOfSize:14.0f];
    
    lblSummaryTitle.font = [UIFont fontWithName:@"HelveticaNeueLTCom-Blk" size:12];
    lblAdditionalDetailsTitle.font = [UIFont fontWithName:@"HelveticaNeueLTCom-Blk" size:12];
    
    lblAdditionalDetailsTitle.textColor = [UIColor darkGrayColor];
    lblSummaryTitle.textColor = [UIColor darkGrayColor];

    
    lblPromoCodeStatus= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+430, 270, 20)];
    
    deliveryNotesTxt.delegate = (id)self;
    
    // summary labels
    lblSubTotal= [[UILabel alloc]initWithFrame:CGRectMake(170, delta+510, 130, 20)];
    lblTaxes= [[UILabel alloc]initWithFrame:CGRectMake(170, delta+530, 130, 20)];
    lblDeliveryDate= [[UILabel alloc]initWithFrame:CGRectMake(170, delta+550, 130, 20)];
    lblTankLocation= [[UILabel alloc]initWithFrame:CGRectMake(135, delta+570, 165, 20)];
    lblDiscountApplied= [[UILabel alloc]initWithFrame:CGRectMake(170, delta+590, 130, 20)];
    lblTotal= [[UILabel alloc]initWithFrame:CGRectMake(170, delta+610, 130, 30)];
    lblNewSubTotal= [[UILabel alloc]initWithFrame:CGRectMake(170, delta+630, 130, 20)];
    lblShipping= [[UILabel alloc]initWithFrame:CGRectMake(170, delta+650, 130, 20)];
  //  lblPromoCodeMessage= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+670, 270, 20)];
    
    lblSubTotal.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblTaxes.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblShipping.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblDeliveryDate.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblTankLocation.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblPromoCodeMessage.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblNewSubTotal.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblDiscountApplied.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblTotal.font = [UIFont fontWithName:@"OpenSans-Bold" size:16];
    
    lblSubTotal.textAlignment = NSTextAlignmentRight;
    lblTotal.textAlignment = NSTextAlignmentRight;
    lblTaxes.textAlignment = NSTextAlignmentRight;
    lblShipping.textAlignment = NSTextAlignmentRight;
    lblDeliveryDate.textAlignment = NSTextAlignmentRight;
    lblTankLocation.textAlignment = NSTextAlignmentRight;
    lblDiscountApplied.textAlignment = NSTextAlignmentRight;
    lblNewSubTotal.textAlignment = NSTextAlignmentRight;
    
    lblSubTotal.text = @"HHHHHHH";
//    lblPromoCodeMessage.textAlignment = NSTextAlignmentRight;

    // summary label titles
    lblSubTotalTitle= [[UILabel alloc]initWithFrame:CGRectMake(16, delta+510, 140, 20)];
    lblTaxesTitle= [[UILabel alloc]initWithFrame:CGRectMake(16, delta+530, 140, 20)];
    lblDeliveryDateTitle= [[UILabel alloc]initWithFrame:CGRectMake(16, delta+550, 140, 20)];
    lblTankLocationTitle= [[UILabel alloc]initWithFrame:CGRectMake(16, delta+570, 140, 20)];
    lblDiscountAppliedTitle= [[UILabel alloc]initWithFrame:CGRectMake(16, delta+590, 140, 20)];
    lblTotalTitle= [[UILabel alloc]initWithFrame:CGRectMake(16, delta+610, 140, 30)];
    lblNewSubTotalTitle= [[UILabel alloc]initWithFrame:CGRectMake(16, delta+630, 140, 20)];
    lblShippingTitle= [[UILabel alloc]initWithFrame:CGRectMake(16, delta+650, 140, 20)];
    
  //  lblPromoCodeMessageTitle= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+670, 270, 20)];
    
    lblSubTotalTitle.text = @"SUBTOTAL:";
    lblDiscountAppliedTitle.text = @"DISCOUNT";
    lblNewSubTotalTitle.text = @"NEW SUBTOTAL:";
    lblTaxesTitle.text = @"TAXES:";
    lblShippingTitle.text = @"SHIPPING:";
    lblDeliveryDateTitle.text = @"DELIVERY DATE:";
    lblTankLocationTitle.text = @"DELIVERY LOCATION:";
    lblTotalTitle.text = @"TOTAL:";
//    lblPromoCodeMessageTitle.text = @"Promo:";
    
    lblSubTotalTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblTotalTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:16];
    lblTaxesTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblShippingTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblDeliveryDateTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblTankLocationTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
 //   lblPromoCodeMessageTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblNewSubTotalTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblDiscountAppliedTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    
    lblSubTotal.textColor = [UIColor darkGrayColor];
    lblTaxes.textColor = [UIColor darkGrayColor];
    lblShipping.textColor = [UIColor darkGrayColor];
    lblDeliveryDate.textColor = [UIColor darkGrayColor];
    lblTankLocation.textColor = [UIColor darkGrayColor];
    lblNewSubTotal.textColor = [UIColor darkGrayColor];
    lblDiscountApplied.textColor = [UIColor darkGrayColor];
    
    lblSubTotalTitle.textColor = [UIColor darkGrayColor];
    lblTaxesTitle.textColor = [UIColor darkGrayColor];
    lblShippingTitle.textColor = [UIColor darkGrayColor];
    lblDeliveryDateTitle.textColor = [UIColor darkGrayColor];
    lblTankLocationTitle.textColor = [UIColor darkGrayColor];
    lblNewSubTotalTitle.textColor = [UIColor darkGrayColor];
    lblDiscountAppliedTitle.textColor = [UIColor darkGrayColor];
    
    // background white/green bands for summary
    
    whiteBack1 = [[UIView alloc]initWithFrame:CGRectMake(12, delta+510, 294, 20)];
    whiteBack2 = [[UIView alloc]initWithFrame:CGRectMake(12, delta+550, 294, 20)];
    whiteBack3 = [[UIView alloc]initWithFrame:CGRectMake(12, delta+590, 294, 20)];
    greenBack = [[UIView alloc]initWithFrame:CGRectMake(12, delta+610, 294, 30)];
    
    whiteBack1.backgroundColor = [UIColor whiteColor];
    whiteBack2.backgroundColor = [UIColor whiteColor];
    whiteBack3.backgroundColor = [UIColor whiteColor];
    whiteBack4.backgroundColor = [UIColor whiteColor];
    greenBack.backgroundColor = customGreenColor;

    
    lblTotal.textColor = [UIColor whiteColor];
    lblTotalTitle.textColor = [UIColor whiteColor];

    // popup view
    popupView= [[UIView alloc]initWithFrame:CGRectMake(10, delta+10, 300, 270)];
    
    
    if (SH>500) {
        popupView.frame = CGRectMake(10, delta+30, 300, 270);
    }
    
    popupView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"greyBG1158.png"]];
    popupView.layer.cornerRadius = 6.0f;
    popupView.layer.borderColor = [[UIColor darkGrayColor] CGColor];
    popupView.layer.borderWidth = 2.0f;
    
    address1Txt= [[CustomUITextField alloc]initWithFrame:CGRectMake(10, delta+40, 270, 20)];
    address2Txt= [[CustomUITextField alloc]initWithFrame:CGRectMake(10, delta+80, 270, 20)];
    cityTxt= [[CustomUITextField alloc]initWithFrame:CGRectMake(10, delta+120, 270, 20)];
    stateTxt= [[CustomUITextField alloc]initWithFrame:CGRectMake(10, delta+160, 150, 20)];
    zipTxt= [[CustomUITextField alloc]initWithFrame:CGRectMake(180, delta+160, 100, 20)];
    expirationYearTxt = [[CustomUITextField alloc]initWithFrame:CGRectMake(10, delta+200, 100, 20)];
    
    address1Txt.delegate = (id)self;
    address2Txt.delegate = (id)self;
    cityTxt.delegate = (id)self;
    stateTxt.delegate = (id)self;
    zipTxt.delegate = (id)self;
    expirationYearTxt.delegate = (id)self;
    
    [address1Txt setBorderStyle:UITextBorderStyleRoundedRect];
    [address2Txt setBorderStyle:UITextBorderStyleRoundedRect];
    [cityTxt setBorderStyle:UITextBorderStyleRoundedRect];
    [stateTxt setBorderStyle:UITextBorderStyleRoundedRect];
    [zipTxt setBorderStyle:UITextBorderStyleRoundedRect];
    [expirationYearTxt setBorderStyle:UITextBorderStyleRoundedRect];
    
    lblAddress1Title= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+15, 270, 20)];
    lblAddress2Title= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+60, 270, 20)];
    lblCityTitle= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+100, 270, 20)];
    lblStateTitle= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+140, 150, 20)];
    lblZipTitle= [[UILabel alloc]initWithFrame:CGRectMake(180, delta+140, 100, 20)];
    lblExpirationYearTitle= [[UILabel alloc]initWithFrame:CGRectMake(10, delta+180, 100, 20)];
    
    lblAddress1Title.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblAddress2Title.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblCityTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblStateTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblZipTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    lblExpirationYearTitle.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    
    lblAddress1Title.textColor = customBlueColor;
    lblAddress2Title.textColor = customBlueColor;
    lblCityTitle.textColor = customBlueColor;
    lblStateTitle.textColor = customBlueColor;
    lblZipTitle.textColor = customBlueColor;
    lblExpirationYearTitle.textColor = customBlueColor;
    
    btnDone= [[UIButton alloc]initWithFrame:CGRectMake(10, delta+230, 100, 25)];
    btnCancel= [[UIButton alloc]initWithFrame:CGRectMake(180, delta+230, 100, 25)];
    
    [btnDone addTarget:self action:@selector(btnDonePressed:)forControlEvents:UIControlEventTouchDown];
    [btnCancel addTarget:self action:@selector(btnCancelPressed:)forControlEvents:UIControlEventTouchDown];
    
    [btnDone setTitle:@"Save" forState:UIControlStateNormal];
    [btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    btnDone.backgroundColor = [UIColor darkGrayColor];
    btnCancel.backgroundColor = [UIColor darkGrayColor];
    

    // add subviews
    
    [myScrollView addSubview:greenBack];
    [myScrollView addSubview:whiteBack1];
    [myScrollView addSubview:whiteBack2];
    [myScrollView addSubview:whiteBack3];
//    [myScrollView addSubview:whiteBack4];
    
    [myScrollView addSubview:btnChangeShipping];
    [myScrollView addSubview:btnChangeBilling];
    [myScrollView addSubview:btnChangePayment];
    [myScrollView addSubview:btnApplyPromo];
//    [myScrollView addSubview:btnViewCart];
    
    [myScrollView addSubview:shippingAddressTxt];
    [myScrollView addSubview:billingAddressTxt];
    [myScrollView addSubview:paymentInfoTxt];
    [myScrollView addSubview:promoTxt];
    [myScrollView addSubview:deliveryNotesTxt];
    
    [myScrollView addSubview:lblShippingAddressTitle];
    [myScrollView addSubview:lblBillingAddressTitle];
    [myScrollView addSubview:lblPaymentInfoTitle];
    [myScrollView addSubview:lblAdditionalDetailsTitle];
    [myScrollView addSubview:lblDeliveryNotesTitle];
    [myScrollView addSubview:lblPromoCodeTitle];
    [myScrollView addSubview:lblSummaryTitle];
    
    [myScrollView addSubview:lblPromoCodeStatus];
    
    // summary labels
    [myScrollView addSubview:lblSubTotal];
    [myScrollView addSubview:lblTotal];
    [myScrollView addSubview:lblTaxes];
//    [myScrollView addSubview:lblShipping];
    [myScrollView addSubview:lblDeliveryDate];
    [myScrollView addSubview:lblTankLocation];
    [myScrollView addSubview:lblPromoCodeMessage];
//    [myScrollView addSubview:lblNewSubTotal];
    [myScrollView addSubview:lblDiscountApplied];
    
    // summary label titles
    [myScrollView addSubview:lblSubTotalTitle];
    [myScrollView addSubview:lblTotalTitle];
    [myScrollView addSubview:lblTaxesTitle];
//    [myScrollView addSubview:lblShippingTitle];
    [myScrollView addSubview:lblDeliveryDateTitle];
    [myScrollView addSubview:lblTankLocationTitle];
    [myScrollView addSubview:lblPromoCodeMessageTitle];
//    [myScrollView addSubview:lblNewSubTotalTitle];
    [myScrollView addSubview:lblDiscountAppliedTitle];
    
    // popup view
    [popupView addSubview:address1Txt];
    [popupView addSubview:address2Txt];
    [popupView addSubview:cityTxt];
    [popupView addSubview:stateTxt];
    [popupView addSubview:zipTxt];
    [popupView addSubview:expirationYearTxt];
    
    [popupView addSubview:btnDone];
    [popupView addSubview:btnCancel];
    
    [popupView addSubview:lblAddress1Title];
    [popupView addSubview:lblAddress2Title];
    [popupView addSubview:lblCityTitle];
    [popupView addSubview:lblStateTitle];
    [popupView addSubview:lblZipTitle];
    [popupView addSubview:lblExpirationYearTitle];
    
    [self.view addSubview:myScrollView];
    [self.view addSubview:popupView];
    
    popupView.hidden = YES;
    
    [self apiCall:[NSString stringWithFormat:@"/get_customer/customer_id/%@/auth_token/%@",gCustomerID,gAuthToken]:@"shipping"];

//    gShippingPrice = @"Free";
    lblShipping.text = gShippingPrice;
    lblDeliveryDate.text = gShippingDate;
    lblTankLocation.text = gPickupLocation;
    
    lblSubTotal.text = [NSString stringWithFormat:@"%.2f",[lblSubTotal.text floatValue]];
    lblTotal.text = [NSString stringWithFormat:@"%.2f",[lblTotal.text floatValue]];
    
    
    // arrays to hold picker data
    
    arrayState = [[NSMutableArray alloc] initWithObjects:@"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH",@"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY",nil];
    
    arrayYear = [[NSMutableArray alloc] initWithObjects:@"2014",@"2015",@"2016",@"2017",@"2018",@"2019",@"2020",@"2021",@"2022",@"2023",@"2024",@"2025",@"2026",@"2027",@"2028",@"2029",@"2030",nil];
    
    arrayMonth = [[NSMutableArray alloc] initWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12",nil];
    
    arrayCardType = [[NSMutableArray alloc] initWithObjects:@"Visa",@"Amex",@"Discover",@"Mastercard",nil];
    
    pickerCardMonth= [[UIPickerView alloc]initWithFrame:CGRectMake(0, delta+450, 320, 250)];
    pickerState= [[UIPickerView alloc]initWithFrame:CGRectMake(0, delta+450, 320, 250)];
    pickerCardYear= [[UIPickerView alloc]initWithFrame:CGRectMake(0, delta+450, 320, 250)];
    pickerCardType= [[UIPickerView alloc]initWithFrame:CGRectMake(0, delta+450, 320, 250)];
    
    pickerCardMonth.tag = 1;
    pickerState.tag = 2;
    pickerCardYear.tag = 3;
    pickerCardType.tag = 4;
    
    pickerCardMonth.delegate = (id)self;
    pickerState.delegate = (id)self;
    pickerCardYear.delegate = (id)self;
    pickerCardType.delegate = (id)self;
    
//    [self.view addSubview:pickerCardMonth];
//    [self.view addSubview:pickerState];
//    [self.view addSubview:pickerCardYear];
//    [self.view addSubview:pickerCardType];

    
    [pickerCardMonth selectRow:1 inComponent:0 animated:YES];
    [pickerCardMonth reloadComponent:0];
    [pickerCardYear selectRow:1 inComponent:0 animated:YES];
    [pickerCardYear reloadComponent:0];
    [pickerState selectRow:1 inComponent:0 animated:YES];
    [pickerState reloadComponent:0];
    [pickerCardType selectRow:1 inComponent:0 animated:YES];
    [pickerCardType reloadComponent:0];
    
    pickerCardMonth.hidden = YES;
    pickerCardYear.hidden = YES;
    pickerState.hidden = YES;
    pickerCardType.hidden = YES;
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    keyboardDoneButtonView.barStyle = UIBarStyleBlack;
    keyboardDoneButtonView.translucent = YES;
    keyboardDoneButtonView.tintColor = [UIColor whiteColor];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    // [barItems addObject:flexSpace];
    
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(pickerDoneClicked:)] ;
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexSpace,doneButton, nil]];
    
    if (([buttonPressed isEqualToString:@"shipping"]) || ([buttonPressed isEqualToString:@"billing"])) {
        stateTxt.inputAccessoryView = keyboardDoneButtonView;
    }
    
    if (([buttonPressed isEqualToString:@"payment"])) {
        expirationYearTxt.inputAccessoryView = keyboardDoneButtonView;
        zipTxt.inputAccessoryView = keyboardDoneButtonView;
        address2Txt.inputAccessoryView = keyboardDoneButtonView;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (IBAction)btnDonePressed:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([buttonPressed isEqualToString:@"shipping"]) {
        
        gShipAddress1 =  address1Txt.text;
        gShipAddress2 =  address2Txt.text;
        gShipCity =  cityTxt.text;
        gShipState =  stateTxt.text;
        gShipZipCode =  zipTxt.text;
    }

    if ([buttonPressed isEqualToString:@"billing"]) {
        
        gPayAddress1  =  address1Txt.text;
        gPayAddress2  =  address2Txt.text;
        gPayCity =  cityTxt.text;
        gPayState =  stateTxt.text;
        gPayZipCode =  zipTxt.text;
    }

    if ([buttonPressed isEqualToString:@"payment"]) {

        gCardHolderName  =  address1Txt.text;
        gCardType  =  address2Txt.text;
        gCardNumber =  cityTxt.text;
        gCVV =  stateTxt.text;
        gExpirationMonth =  zipTxt.text;
        gExpirationYear = expirationYearTxt.text;
        
        NSString *postString = [NSString stringWithFormat:@"customer_id=%@&cc_type=%@&cc_number=%@&exp_month=%@&exp_year=%@&auth_token=%@",gCustomerID,gCardType,gCardNumber,gExpirationMonth,gExpirationYear,gAuthToken];
        
        [self apiCallPost:@"add_credit_card/" :postString :@"addCard"];
    }

    shippingAddressTxt.text = [NSString stringWithFormat:@"%@ %@ \n%@ %@ %@",gShipAddress1,gShipAddress2,gShipCity,gShipState,gShipZipCode];
    billingAddressTxt.text = [NSString stringWithFormat:@"%@ %@ \n%@ %@ %@",gPayAddress1,gPayAddress2,gPayCity,gPayState,gPayZipCode];
    
    paymentInfoTxt.text = [NSString stringWithFormat:@"Card: %@ Number: %@\nExpiration: %@/%@",gCardType, gCardNumber,gExpirationMonth, gExpirationYear];

    
    popupView.hidden = YES;
}

- (void) apiCallPost:(NSString *)URLString :(NSString *)postString :(NSString *)currentSection {
    if (![[AFNetworkReachabilityManager sharedManager] isReachable]) {
        UIAlertView *unreachable = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Sorry, there was a problem with the connection. Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [unreachable show];
        return;
    }
    
    [apiResponseArray removeAllObjects];
    
    URLString = [NSString stringWithFormat:@"%@%@",gAPIString,URLString];
    
    // NSLog(@"currentSection: %@",currentSection);
    // NSLog(@"URLString: %@",URLString);
    // NSLog(@"postString: %@",postString);
    
    dispatch_queue_t queue = dispatch_get_global_queue(
                                                       DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        //       NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLString]];
        
        [request setHTTPMethod:@"POST"];
        
        [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:nil error:nil];
        
        NSError *jsonParsingError = nil;
        apiResponseArray = [NSJSONSerialization JSONObjectWithData:response
                                                           options:NSJSONReadingMutableContainers error:&jsonParsingError];
        dispatch_sync(dispatch_get_main_queue(),^ {
            [self processAPICall:currentSection];
        });
    });
}

- (IBAction)btnCancelPressed:(id)sender {
 
    [self.view endEditing:YES];
    popupView.hidden = YES;
}

- (IBAction)btnChangeShippingPressed:(id)sender{
    
    popupView.hidden = NO;
    buttonPressed = @"shipping";
        expirationYearTxt.hidden = YES;
    lblExpirationYearTitle.hidden = YES;
    
    lblAddress1Title.text = @"Shipping Address 1";
    lblAddress2Title.text = @"Shipping Address 1";
    lblCityTitle.text = @"Shipping City";
    lblStateTitle.text = @"Shipping State";
    lblZipTitle.text = @"Shipping Zip";
    
    address1Txt.text = gShipAddress1;
    address2Txt.text = gShipAddress2;
    cityTxt.text = gShipCity;
    stateTxt.text = gShipState;
    zipTxt.text = gShipZipCode;
    
    [stateTxt setInputView:pickerState];
    [expirationYearTxt setInputView:nil];
    [zipTxt setInputView:nil];
    [address2Txt setInputView:nil];

}

- (IBAction)btnChangeBillingPressed:(id)sender{
    
    popupView.hidden = NO;
    buttonPressed = @"billing";
        expirationYearTxt.hidden = YES;
        lblExpirationYearTitle.hidden = YES;
    
    lblAddress1Title.text = @"Billing Address 1";
    lblAddress2Title.text = @"Billing Address 1";
    lblCityTitle.text = @"Billing City";
    lblStateTitle.text = @"Billing State";
    lblZipTitle.text = @"Billing Zip";
    
    address1Txt.text = gPayAddress1;
    address2Txt.text = gPayAddress2;
    cityTxt.text = gPayCity;
    stateTxt.text = gPayState;
    zipTxt.text = gPayZipCode;
    
    [stateTxt setInputView:pickerState];
    [expirationYearTxt setInputView:nil];
    [zipTxt setInputView:nil];
    [address2Txt setInputView:nil];
}
- (IBAction)btnChangePaymentPressed:(id)sender{
    
    popupView.hidden = NO;
    buttonPressed = @"payment";
    expirationYearTxt.hidden = NO;
    lblExpirationYearTitle.hidden = NO;
    
    lblAddress1Title.text = @"Cardmember Name";
    lblAddress2Title.text = @"Card Type";
    lblCityTitle.text = @"Card Number";
    lblStateTitle.text = @"CVV";
    lblZipTitle.text = @"CC Exp Month";
    lblExpirationYearTitle.text = @"CC Exp Year";
    
    address1Txt.text = gFirstName;
    address2Txt.text = gCardType;
    cityTxt.text = [NSString stringWithFormat:@"%@",gCardNumber];
    stateTxt.text = [NSString stringWithFormat:@"%@",gCVV];
    zipTxt.text = [NSString stringWithFormat:@"%@",gExpirationMonth];
    expirationYearTxt.text = [NSString stringWithFormat:@"%@",gExpirationYear];
    
    [stateTxt setInputView:nil];
    [expirationYearTxt setInputView:pickerCardYear];
    [zipTxt setInputView:pickerCardMonth];
    [address2Txt setInputView:pickerCardType];
}

- (IBAction)pickerDoneClicked:(id)sender {
    
    self.pickerCardMonth.hidden = YES;
    self.pickerCardYear.hidden = YES;
    self.pickerState.hidden = YES;
    self.pickerCardYear.hidden = YES;
    
    [zipTxt resignFirstResponder];
    [stateTxt resignFirstResponder];
    [expirationYearTxt resignFirstResponder];
    [address2Txt resignFirstResponder];
}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    //One column
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView tag] == 1) {
        return arrayMonth.count;
    } else  if ([pickerView tag] == 2) {
        return arrayState.count;
    } else  if ([pickerView tag] == 3) {
        return arrayYear.count;
    } else  if ([pickerView tag] == 4) {
        return arrayCardType.count;
    }
    
    else return 0;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //set item per row
    if ([pickerView tag] == 1) {
        return [arrayMonth objectAtIndex:row];
    } else  if ([pickerView tag] == 2) {
        return [arrayState objectAtIndex:row];
    } else  if ([pickerView tag] == 3) {
        return [arrayYear objectAtIndex:row];
    }else  if ([pickerView tag] == 4) {
        return [arrayCardType objectAtIndex:row];
    }
    else return 0;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    
    if ([pickerView tag] == 1) {
        self.zipTxt.text = arrayMonth[row];
        
    } else  if ([pickerView tag] == 2) {
        self.stateTxt.text =arrayState[row];
        
    } else  if ([pickerView tag] == 3) {
        self.expirationYearTxt.text =arrayYear[row];
    } else  if ([pickerView tag] == 4) {
        self.address2Txt.text =arrayCardType[row];
    }
}

- (void)textFieldDidBeginEditing:(CustomUITextField *)textField {

    
    if ([buttonPressed isEqualToString:@"payment"]) {
    
        if ([textField isEqual:zipTxt]) {
            pickerCardMonth.hidden = NO;
            if ([zipTxt.text isEqualToString:@""]) {
                zipTxt.text = @"02";
            }
        }

        if ([textField isEqual:expirationYearTxt]) {
            pickerCardYear.hidden = NO;
            if ([expirationYearTxt.text isEqualToString:@""]) {
                expirationYearTxt.text = @"2015";
            }
        }

        if ([textField isEqual:address2Txt]) {
            pickerCardType.hidden = NO;
            if ([address2Txt.text isEqualToString:@""]) {
                address2Txt.text = @"Amex";
            }
        }
    }
    if (([buttonPressed isEqualToString:@"shipping"]) || ([buttonPressed isEqualToString:@"billing"])) {
    
        if ([textField isEqual:stateTxt]) {
            pickerState.hidden = NO;
            if ([stateTxt.text isEqualToString:@""]) {
                stateTxt.text = @"AK";
            }
            
        }
    }
}


- (IBAction)btnApplyPromoPressed:(id)sender{
    
    [self.view endEditing:YES];
    
    if ([promoTxt.text isEqualToString: @""]) {
        return;
    }
    
    gPromoCode  = promoTxt.text;
    
    
    // NSLog(@"promo pressed: %@",[NSString stringWithFormat:@"/promo_code_add/customer_id/%@/cart_token/%@/promoid/%@",gCustomerID,gCartToken,gPromoCode]);
    
    [self apiCall:[NSString stringWithFormat:@"/promo_code_add/customer_id/%@/cart_token/%@/promoid/%@/auth_token/%@",gCustomerID,gCartToken,gPromoCode,gAuthToken]:@"checkPromo"];
    
    
//    NSString *postString = [NSString stringWithFormat:@"customer_id=%@&cart_token=%@&promoid=%@",gCustomerID,gCartToken,gPromoCode];
//    
//    [self apiCallPost:@"promo_code_add/" :postString :@"checkPromo"];

}

- (IBAction)btnViewCartPressed:(id)sender{
    CartViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
    vc.shouldAddProduct = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)homeBtnPressed:(id)sender {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
    if (gIsLoggedIn) {
        vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoggedInHomeViewController"];
    }

    [self.revealSideViewController popViewControllerWithNewCenterController:vc animated:YES];
}
- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

- (IBAction)btnPlaceOrderPressed:(id)sender {
    
    if (([gShipAddress1 isEqualToString:@""]) ||
        ([gShipCity isEqualToString:@""]) ||
        ([gShipState isEqualToString:@""]) ||
        ([gShipZipCode isEqualToString:@""])) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"Please check the Shipping Address."
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if (([gPayAddress1 isEqualToString:@""]) ||
        ([gPayCity isEqualToString:@""]) ||
        ([gPayState isEqualToString:@""]) ||
        ([gPayZipCode isEqualToString:@""])) {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Alert!"
                                                       message: @"Please check the Payment Address."
                                                      delegate: self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    
    [self apiCall:[NSString stringWithFormat:@"/checkout_process/customer_id/%@/cart_token/%@/service_area_id/%@/auth_token/%@",gCustomerID,gCartToken,gServiceAreaID,gAuthToken]:@"confirmOrder"];
    
    // NSLog(@"Order string: %@",[NSString stringWithFormat:@"/checkout_process/customer_id/%@/cart_token/%@/service_area_id/%@",gCustomerID,gCartToken,gServiceAreaID]);
    
}

- (void)processCartData:(id)cartData
{
    NSArray *products = [cartData isKindOfClass:[NSArray class]] ? cartData : nil;
//    NSLog(@"processCartData - products: %@", products);
//    NSLog(@"count - %d", products.count);
    if (products && products.count) {
        [products enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            PTShoppingItem *shoppingItem = [[PTShoppingItem alloc] init];
            shoppingItem.productType = [obj objectAtIndex:3];
            shoppingItem.productID = [obj objectAtIndex:4];
            shoppingItem.itemName = [obj objectAtIndex:6];
            if ([[obj objectAtIndex:7] isEqual:[NSNull null]] != YES)
                shoppingItem.additions = [obj objectAtIndex:7];
            shoppingItem.quantity = [obj objectAtIndex:8] ? [NSNumber numberWithInteger:[[obj objectAtIndex:8] integerValue]] : @0;
            shoppingItem.disableQTY = [obj objectAtIndex:9];
            shoppingItem.pricePerItem = [NSNumber numberWithFloat:[[obj objectAtIndex:10] floatValue]];
            shoppingItem.extendedPrice = [NSNumber numberWithFloat:[[obj objectAtIndex:11] floatValue]];
            shoppingItem.discount = ![[obj objectAtIndex:12] isEqual:[NSNull null]] ? [NSNumber numberWithFloat:[[obj objectAtIndex:12] floatValue]] : @0;
            shoppingItem.orderNumber = [NSString stringWithFormat:@"%@",[obj objectAtIndex:0]];
            
            if ([[obj objectAtIndex:22] isEqual:[NSNull null]] != YES)
                shoppingItem.shippingDate = [obj objectAtIndex:22];
            
            if ([[obj objectAtIndex:24] isEqual:[NSNull null]] != YES)
                shoppingItem.pickupLocation = [obj objectAtIndex:24];
            
            if ([shoppingItem.disableQTY integerValue] == 0)
            {
                [lblTankLocation setText:shoppingItem.pickupLocation];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"MMMM, dd yyyy HH:mm:ss"];
                NSDate *createTime = [dateFormatter dateFromString:shoppingItem.shippingDate];
                [dateFormatter setDateFormat:@"MMMM, dd yyyy"];
                NSString *strShippingDate = [dateFormatter stringFromDate:createTime];
                [lblDeliveryDate setText:strShippingDate];
                
                [dateFormatter setDateFormat:@"MM-dd-yyyy"];
                gShippingDate = [dateFormatter stringFromDate:createTime];
            }
        }];
    } else {
        [lblTankLocation setText:@""];
        [lblDeliveryDate setText:@""];
    }
}


@end
