//
//  CartViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartViewController : UIViewController

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckOut;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

@property (strong,nonatomic) IBOutlet UILabel *lblCartStatus;
@property (strong,nonatomic) IBOutlet UILabel *lblItemsInCart;

@property (strong,nonatomic) IBOutlet UILabel *lblCartTotalPrice;

@property (weak, nonatomic) IBOutlet UIView *viewTaxes;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIView *viewSpliter;
@property (weak, nonatomic) IBOutlet UIView *viewCartContainer;


@property (weak, nonatomic) IBOutlet UILabel *lblSubtotal;
@property (weak, nonatomic) IBOutlet UILabel *lblTaxes;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscount;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;

@property (nonatomic, assign) BOOL shouldAddProduct;
@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;
@property (nonatomic, strong) NSArray *cartItems;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;
//- (IBAction)btnEditOrderPressed:(id)sender;
//- (IBAction)btnRemoveFromCartPressed:(id)sender;

- (IBAction)btnCheckOutPressed:(id)sender;
- (IBAction)btnContinuePressed:(id)sender;
@end
