//
//  CheckoutShippingViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSYPopoverListView.h"

@interface CheckoutShippingViewController : UIViewController<ZSYPopoverListDatasource, ZSYPopoverListDelegate,UIAlertViewDelegate>

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;

@property (strong, nonatomic) IBOutlet CustomUITextField *shipAddress1Txt;
@property (strong, nonatomic) IBOutlet CustomUITextField *shipAddress2Txt;
@property (strong, nonatomic) IBOutlet CustomUITextField *shipCityTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *shipStateTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *shipZipTxt;

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

@property (nonatomic, assign, getter = isCheckOut) BOOL checkOut;
@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewCartButton;


- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;
- (IBAction)btnContinuePressed:(id)sender;
- (IBAction)stateBtnPressed:(id)sender;
@end
