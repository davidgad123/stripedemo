//
//  CheckoutAccountViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckoutAccountViewController : UIViewController

@property (strong,nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

@property (strong, nonatomic) IBOutlet CustomUITextField *firstNameTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *lastNameTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *emailTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *mobilePhoneTxt;
@property (strong, nonatomic) IBOutlet CustomUITextField *homePhoneTxt;

@property (nonatomic, assign, getter = isCheckOut) BOOL checkOut;
@property (weak, nonatomic) IBOutlet UILabel *navigationTitle;
@property (weak, nonatomic) IBOutlet UIButton *viewCartButton;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;
- (IBAction)btnContinuePressed:(id)sender;
@end
