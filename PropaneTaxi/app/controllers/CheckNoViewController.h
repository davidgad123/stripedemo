//
//  CheckNoViewController.h
//  PropaneTaxi
//
//  Created by Jake on 11/18/13.
//  Copyright (c) 2013 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckNoViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnNewAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnKeepMeInformed;

@property (nonatomic,retain) IBOutlet UIScrollView *myScrollView;

@property (strong, nonatomic) IBOutlet CustomUITextField *emailTxt;
@property (strong, nonatomic) IBOutlet UILabel *emailTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblToBeNotified;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet CustomUITextField *firstNameField;
@property (weak, nonatomic) IBOutlet CustomUITextField *lastNameField;

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)navBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)btnNewAddressPressed:(id)sender;
- (IBAction)btnKeepMeInformedPressed:(id)sender;

@end
