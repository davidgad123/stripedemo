//
//  PTCartCollectionCell.h
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/23/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PTCartViewCell;

@protocol PTCartCellDelegate <NSObject>
-(void)cartCellDidPressEdit:(PTCartViewCell*)cell;
-(void)cartCellDidPressRemove:(PTCartViewCell*)cell;
@end

@interface PTCartViewCell : UIView
// data
@property (nonatomic, weak) id<PTCartCellDelegate> delegate;
@property (nonatomic, strong) PTShoppingItem *cartItem;


// actions
- (void)didPressEditOrder;
- (void)didPressRemove;
@end
