//
//  PTCartCollectionCell.m
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/23/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import "PTCartViewCell.h"

@implementation PTCartViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)layoutSubviews
{
    [super layoutSubviews];

    CGRect orgRect = self.frame;
    int y = 20;

    if (self.cartItem)
    {
        // ITEM
        {
            UILabel *lblTitleItem = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 20)];
            lblTitleItem.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
            lblTitleItem.textColor = customBlueColor;
            lblTitleItem.textAlignment = NSTextAlignmentRight;
            lblTitleItem.text = @"ITEM:";
            
            UILabel *lblItem = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 180, 20)];
            [lblItem setFont:[UIFont fontWithName:@"Open Sans" size:12.0f]];
            lblItem.text = self.cartItem.itemName;
            
            [self addSubview:lblTitleItem];
            [self addSubview:lblItem];
            
            y+= 20;
        }

        // QUANTITY
        {
            UILabel *lblTitleQuantity = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 20)];
            lblTitleQuantity.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
            lblTitleQuantity.textColor = customBlueColor;
            lblTitleQuantity.textAlignment = NSTextAlignmentRight;
            lblTitleQuantity.text = @"QUANTITY:";
            
            UILabel *lblQuantity = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 180, 20)];
            [lblQuantity setFont:[UIFont fontWithName:@"Open Sans" size:12.0f]];
            lblQuantity.text = [NSString stringWithFormat:@"%@",self.cartItem.quantity];
            
            [self addSubview:lblTitleQuantity];
            [self addSubview:lblQuantity];
            
            y+= 20;
        }

        // Additions
        if (self.cartItem.additions != nil)
        {
            UILabel *lblTitleAddition = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 20)];
            lblTitleAddition.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
            lblTitleAddition.textColor = customBlueColor;
            lblTitleAddition.textAlignment = NSTextAlignmentRight;
            lblTitleAddition.text = @"ADDITIONS:";
            
            UILabel *lblAddition = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 180, 20)];
            [lblAddition setFont:[UIFont fontWithName:@"Open Sans" size:12.0f]];
            lblAddition.text = self.cartItem.additions;
            
            [self addSubview:lblTitleAddition];
            [self addSubview:lblAddition];
            
            y+= 20;
  
        }
        
        // Discounts
        double discount = [self.cartItem.discount floatValue];
        if (discount > 0)
        {
            UILabel *lblTitleDiscount = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 20)];
            lblTitleDiscount.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
            lblTitleDiscount.textColor = customBlueColor;
            lblTitleDiscount.textAlignment = NSTextAlignmentRight;
            lblTitleDiscount.text = @"DISCOUNTS:";
            
            UILabel *lblDiscount = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 180, 20)];
            [lblDiscount setFont:[UIFont fontWithName:@"Open Sans" size:12.0f]];
            lblDiscount.text = [NSString stringWithFormat:@"%.2f",self.cartItem.discount.floatValue];
            
            [self addSubview:lblTitleDiscount];
            [self addSubview:lblDiscount];
            
            y+= 20;

        }
        
        // Price
        {
            UILabel *lblTitlePrice = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 20)];
            lblTitlePrice.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
            lblTitlePrice.textColor = customBlueColor;
            lblTitlePrice.textAlignment = NSTextAlignmentRight;
            lblTitlePrice.text = @"PRICE:";
            
            UILabel *lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 180, 20)];
            [lblPrice setFont:[UIFont fontWithName:@"Open Sans" size:12.0f]];
            lblPrice.text = [NSString stringWithFormat:@"%.2f",self.cartItem.extendedPrice.floatValue];
            
            [self addSubview:lblTitlePrice];
            [self addSubview:lblPrice];
            
            y+= 20;

        }
        
        // Shipping
        if (self.cartItem.shipping != nil)
        {
            UILabel *lblTitleShipping = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 20)];
            lblTitleShipping.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
            lblTitleShipping.textColor = customBlueColor;
            lblTitleShipping.textAlignment = NSTextAlignmentRight;
            lblTitleShipping.text = @"SHIPPING:";
            
            UILabel *lblShipping = [[UILabel alloc] initWithFrame:CGRectMake(100, y, 180, 20)];
            [lblShipping setFont:[UIFont fontWithName:@"Open Sans" size:12.0f]];
            lblShipping.text = self.cartItem.shipping;
            
            [self addSubview:lblTitleShipping];
            [self addSubview:lblShipping];
            
            y+= 20;
        }
        
//        CGFloat totalPrice = self.cartItem.quantity.floatValue*self.cartItem.pricePerItem.floatValue;
//        if ([self.cartItem.connectionService isEqualToString:@"Connection Service Price:4.99"]) {
//            totalPrice += 4.99;
//        }
//        self.priceLabel.text = [NSString stringWithFormat:@"%.2f",totalPrice];
        
        
        // Buttons
        if ([self.cartItem.disableQTY integerValue] == 0)
        {
            y += 20;
            
            UIButton *btnEdit = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            btnEdit.frame = CGRectMake(10, y, 130, 20);
            [btnEdit setBackgroundImage:[UIImage imageNamed:@"checkout_edit.png"] forState:UIControlStateNormal];
            [btnEdit addTarget:self action:@selector(didPressEditOrder) forControlEvents:UIControlEventTouchUpInside];

            UIButton *btnRemove = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            btnRemove.frame = CGRectMake(150, y, 130, 20);
            [btnRemove setBackgroundImage:[UIImage imageNamed:@"checkout_remove.png"] forState:UIControlStateNormal];
            [btnRemove addTarget:self action:@selector(didPressRemove) forControlEvents:UIControlEventTouchUpInside];

            [self addSubview:btnEdit];
            [self addSubview:btnRemove];
            
            y += 20;
        }
        
        self.frame = CGRectMake(orgRect.origin.x, orgRect.origin.y, orgRect.size.width, y);
    }
}

#pragma mark - Setters
- (void)setCartItem:(PTShoppingItem *)cartItem
{
    if (![cartItem isEqual:_cartItem]) {
        _cartItem = cartItem;
    }
    [self layoutSubviews];
}

#pragma mark - selector
- (void)didPressEditOrder {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cartCellDidPressEdit:)]) {
        [self.delegate cartCellDidPressEdit:self];
    }
}

- (void)didPressRemove {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cartCellDidPressRemove:)]) {
        [self.delegate cartCellDidPressRemove:self];
    }
}

@end
