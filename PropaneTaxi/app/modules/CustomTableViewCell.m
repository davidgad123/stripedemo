//
//  CustomTableViewCell.m
//  PropaneTaxi
//
//  Created by BoalLing on 7/8/15.
//  Copyright (c) 2015 appcodeworld. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGSize size = self.bounds.size;
    self.textLabel.frame = CGRectMake(10, 0, size.width * 0.85, size.height);
}
@end
