//
//  CustomUITextField.m
//  PropaneTaxi
//
//  Created by Jake on 3/3/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import "CustomUITextField.h"

@implementation CustomUITextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.font = [UIFont fontWithName:@"OpenSans-Bold" size:10];
//        self.fontColor = [UIColor darkGrayColor];
    }
    return self;
}
// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 12 , 2 );
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 12 , 2 );
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
