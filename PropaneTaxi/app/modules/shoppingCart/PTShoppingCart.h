//
//  PTShoppingCart.h
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/23/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PTShoppingItemType) {
    PTShoppingItemTypeExchange = 0,
    PTShoppingItemTypeSpareTank,
    PTShoppingItemTypeConnection
};

@interface PTShoppingItem : NSObject <NSCopying>
//@property (nonatomic, assign) PTShoppingItemType itemType;
@property (nonatomic, strong) NSString *itemName;
@property (nonatomic, strong) NSString *orderNumber;
@property (nonatomic, strong) NSNumber *pricePerItem;
@property (nonatomic, strong) NSNumber *extendedPrice;
@property (nonatomic, strong) NSNumber *quantity;
@property (nonatomic, strong) NSString *additions;
@property (nonatomic, strong) NSNumber *discount;
@property (nonatomic, strong) NSString *shipping;
@property (nonatomic, strong) NSString *shippingDate;
@property (nonatomic, strong) NSString *pickupLocation;
@property (nonatomic, strong) NSString *pickupLocationId;
@property (nonatomic, strong) NSString *connectionService;
@property (nonatomic, strong) NSString *productType;
@property (nonatomic, strong) NSString *productID;
@property (nonatomic, strong) NSString *disableQTY;
@end

@interface PTShoppingCart : NSObject
@property (nonatomic, strong) NSString *cartNumber;
@property (nonatomic, strong) NSString *cartToken;
@property (nonatomic, strong) NSArray *items;

+(instancetype)sharedCart;
-(CGFloat)cartTotal;
-(void)addItem:(PTShoppingItem*)itemToAdd;
-(PTShoppingItem*)itemByCartID:(NSString*)orderNumber;
-(void)removeItem:(PTShoppingItem*)itemToRemove;
-(void)emptyCart;
@end
