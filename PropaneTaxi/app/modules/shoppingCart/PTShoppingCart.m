//
//  PTShoppingCart.m
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/23/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import "PTShoppingCart.h"

@implementation PTShoppingItem
- (id)copyWithZone:(NSZone *)zone
{
    PTShoppingItem *item = [[self class] allocWithZone:zone];
    item.productID = self.productID;
    item.itemName = self.itemName;
//    item.itemType = self.itemType;
    item.pricePerItem = self.pricePerItem;
    item.quantity = self.quantity;
    item.orderNumber = self.orderNumber;
    item.additions = self.additions;
    item.discount = self.discount;
    item.shipping = self.shipping;
    item.shippingDate = self.shippingDate;
    item.pickupLocation = self.pickupLocation;
    item.pickupLocationId = self.pickupLocationId;
    item.connectionService = self.connectionService;
    
    return item;
}
@end

@interface PTShoppingCart ()
@end

@implementation PTShoppingCart

+(instancetype)sharedCart
{
    static PTShoppingCart *shoppingCart = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shoppingCart = [[self alloc] init];
    });
    return shoppingCart;
}

- (CGFloat)cartTotal
{
    __block CGFloat totalPriceOfAllItems = 0.0;
    [self.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        PTShoppingItem *item = [obj isKindOfClass:[PTShoppingItem class]] ? obj : nil;
        if (item) {
            CGFloat totalPriceOfSingleItem = item.pricePerItem.floatValue * item.quantity.floatValue;
            totalPriceOfAllItems += totalPriceOfSingleItem;
        }
    }];
    return totalPriceOfAllItems;
}

- (void)addItem:(PTShoppingItem *)itemToAdd
{
    if (itemToAdd) {
        self.items = [self.items arrayByAddingObject:itemToAdd];
    }
}

- (PTShoppingItem*)itemByCartID:(NSString*)orderNumber
{
    __block PTShoppingItem *match = nil;
    [self.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj cartNumber] isEqualToString:orderNumber]) {
            match = obj;
            *stop = YES;
        }
    }];
    return match;
}

- (void)removeItem:(PTShoppingItem *)itemToRemove
{
    __block NSMutableArray *itemsToKeep = [NSMutableArray array];
    [self.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        PTShoppingItem *potentialMatch = [obj isKindOfClass:[PTShoppingItem class]] ? obj : nil;
        if (!potentialMatch) {
            return;
        }
        
//        if ([potentialMatch.orderNumber isEqualToString:itemToRemove.orderNumber] || potentialMatch.itemType == itemToRemove.itemType) {
        if ([potentialMatch.orderNumber isEqualToString:itemToRemove.orderNumber]) {

            return;
        }
        
        [itemsToKeep addObject:potentialMatch];
    }];
    self.items = [NSArray arrayWithArray:itemsToKeep];
}

- (void)emptyCart
{
    self.items = [NSArray array];
}

#pragma mark - Property Getters
- (NSArray*)items
{
    if (!_items) {
        _items = [NSArray array];
    }
    return _items;
}





















@end
