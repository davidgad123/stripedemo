//
//  PTArticleCollection.m
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/21/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import "PTArticleCollection.h"
#import "PTArticle.h"
#import "PTCategoryCollection.h"
#import "PTCategory.h"
#import "AFNetworking.h"
#import "NSData+OADataHelpers.h"

@implementation PTArticleCollection

- (void)updateArticles
{
    self.articles = [NSArray array];
    WEAKSELF
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [[manager requestSerializer] setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    [manager GET:[NSString stringWithFormat:@"%@%@",gAPIString,@"get_articles/"]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSData *correctedJsonData = [weakSelf convertCorruptedData:operation.responseData];
             NSError *error = nil;
             id correctedResponseObject = [operation.responseSerializer responseObjectForResponse:operation.response data:correctedJsonData error:&error];
             [correctedResponseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                 PTArticle *article = [PTArticle objectFromJSONObject:obj mapping:[PTArticle mappingDictionary]];
                 if (!article) {
                     return;
                 }
                 weakSelf.articles = [weakSelf.articles arrayByAddingObject:article];
             }];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             // NSLog(@"Error: %@", error);
    }];
}

- (NSData*)convertCorruptedData:(NSData*)corruptedData
{
    NSString *jsonStringFromData = [corruptedData UTF8String];
    return [jsonStringFromData dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSArray*)articlesByCategory:(NSString*)categoryName
{
    NSArray *categoryNames = [self getCategoryNames];
    __block NSArray *matchingArticles = [NSArray new];
    if (categoryNames.count > 0) {
        [self.articles enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if (obj && [[obj category_name] isEqualToString:categoryName]) {
                matchingArticles = [matchingArticles arrayByAddingObject:obj];
            }
        }];
    }
    return matchingArticles;
}

- (NSArray*)getCategoryNames
{
    __block NSArray *categoryNames = [NSArray array];
    if (self.collectionOwner && self.collectionOwner.categoryCollection) {
        [self.collectionOwner.categoryCollection.categories enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            PTCategory *category = [obj isKindOfClass:[PTCategory class]] ? obj : nil;
            if (category) {
                categoryNames = [categoryNames arrayByAddingObject:category.category_name];
            }
        }];
    }
    return categoryNames;
}

@end
