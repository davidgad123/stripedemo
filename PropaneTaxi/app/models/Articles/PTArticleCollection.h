//
//  PTArticleCollection.h
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/21/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GrillMasterViewController.h"

@interface PTArticleCollection : NSObject
@property (nonatomic, strong) NSArray *articles;
@property (nonatomic, weak) GrillMasterViewController *collectionOwner;

-(void)updateArticles;
-(NSArray*)articlesByCategory:(NSString*)categoryName;
@end
