//
//  PTArticle.m
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/18/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import "PTArticle.h"

@implementation PTArticle

+ (NSDictionary*)mappingDictionary {
    return @{@"author" : @"author",
             @"category_name": @"category_name",
             @"recipe_total_time": @"recipe_total_time",
             @"image_thumb": @"image_thumb",
             @"recipe_cooking_method": @"recipe_cooking_method",
             @"url_media": @"url_media",
             @"recipe_cuisine": @"recipe_cuisine",
             @"article_content": @"article_content",
             @"msg": @"msg",
             @"image_header": @"image_header",
             @"recipe_cook_time": @"recipe_cook_time",
             @"recipe_prep_time": @"recipe_prep_time",
             @"recipe_category": @"recipe_category",
             @"article_id": @"article_id",
             @"create_date": @"create_date",
             @"title": @"title"};
}




@end
