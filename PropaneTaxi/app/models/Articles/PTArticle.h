//
//  PTArticle.h
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/18/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+JTObjectMapping.h"

@interface PTArticle : NSObject

@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) NSString *category_name;
@property (nonatomic, strong) NSString *recipe_total_time;
@property (nonatomic, strong) NSString *image_thumb;
@property (nonatomic, strong) NSString *recipe_cooking_method;
@property (nonatomic, strong) NSString *url_media;
@property (nonatomic, strong) NSString *recipe_cuisine;
@property (nonatomic, strong) NSString *article_content;
@property (nonatomic, strong) NSString *msg;
@property (nonatomic, strong) NSString *image_header;
@property (nonatomic, strong) NSString *recipe_cook_time;
@property (nonatomic, strong) NSString *recipe_prep_time;
@property (nonatomic, strong) NSString *recipe_category;
@property (nonatomic, strong) NSNumber *article_id;
@property (nonatomic, strong) NSString *create_date;
@property (nonatomic, strong) NSString *title;

+(NSDictionary*)mappingDictionary;

@end
