//
//  PTCategoryCollection.h
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/21/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol PTCategoryCollectionDelegate <NSObject>
@optional
-(void)didFetchCategories;
-(void)didFetchCategoriesWithError;
@end

@interface PTCategoryCollection : NSObject
@property (nonatomic, weak) id <PTCategoryCollectionDelegate> delegate;
@property (nonatomic, strong) NSArray *categories;
- (void)updateCategories;
- (NSString*)getCategoryNameBy:(NSNumber*)categoryId;
- (NSString*)getShortCategoryNameBy:(NSNumber*)categoryId;
@end
