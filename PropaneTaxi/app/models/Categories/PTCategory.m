//
//  PTCategory.m
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/18/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import "PTCategory.h"

@implementation PTCategory

@synthesize category_name = _category_name;

+ (NSDictionary*)mappingDictionary {
    return @{@"category_name": @"category_name",
             @"category_name_short": @"category_name_short",
             @"category_icon": @"category_icon",
             @"parent_category_name": @"parent_category_name",
             @"parent_category_id": @"parent_category_id",
             @"article_category_id": @"article_category_id"
             };
}

@end
