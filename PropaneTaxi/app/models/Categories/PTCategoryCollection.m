//
//  PTCategoryCollection.m
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/21/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import "PTCategoryCollection.h"
#import "PTCategory.h"
#import "AFNetworking.h"

@implementation PTCategoryCollection

- (void)updateCategories
{
    self.categories = [NSArray array];
    WEAKSELF
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [[manager requestSerializer] setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    [manager GET:[NSString stringWithFormat:@"%@%@",gAPIString,@"get_article_categories/"]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                 PTCategory *category = [PTCategory objectFromJSONObject:obj mapping:[PTCategory mappingDictionary]];
                 if (!category) {
                     return;
                 }
                 weakSelf.categories = [weakSelf.categories arrayByAddingObject:category];
             }];
             
             if (self.delegate && [self.delegate respondsToSelector:@selector(didFetchCategories)]) {
                 [self.delegate didFetchCategories];
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             DLog(@"\nerror %@", error);
             if (self.delegate && [self.delegate respondsToSelector:@selector(didFetchCategoriesWithError)]) {
                 [self.delegate didFetchCategoriesWithError];
             }
         }
     ];
}

- (NSString*)getCategoryNameWithLength:(NSString*)length byCategoryId:(NSNumber*)categoryId {
    __block NSString *categoryName;
    [self.categories enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj article_category_id].integerValue == categoryId.integerValue) {
            if ([length isEqualToString:@"short"]) {
                categoryName = [obj category_name_short];
            } else {
                categoryName = [obj category_name];
            }
            *stop = YES;
        }
    }];
    return categoryName;
}

- (NSString*)getCategoryNameBy:(NSNumber *)categoryId
{
    return [self getCategoryNameWithLength:@"long" byCategoryId:categoryId];
}

- (NSString*)getShortCategoryNameBy:(NSNumber *)categoryId
{
    return [self getCategoryNameWithLength:@"short" byCategoryId:categoryId];
}














@end
