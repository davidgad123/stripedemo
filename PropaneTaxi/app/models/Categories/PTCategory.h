//
//  PTCategory.h
//  PropaneTaxi
//
//  Created by Michael Thongvanh on 7/18/14.
//  Copyright (c) 2014 appcodeworld. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+JTObjectMapping.h"

@interface PTCategory : NSObject

@property (nonatomic, strong) NSString *category_name;
@property (nonatomic, strong) NSString *category_name_short;
@property (nonatomic, strong) NSString *category_icon;
@property (nonatomic, strong) NSString *parent_category_name;
@property (nonatomic, strong) NSString *parent_category_id;
@property (nonatomic, strong) NSString *article_category_id;

+(NSDictionary*)mappingDictionary;

@end
