//
//  CDatePickerViewEx.h
//  PropaneTaxi
//
//  Created by BoalLing on 8/17/15.
//  Copyright (c) 2015 appcodeworld. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CDatePickerDelegate <NSObject>

- (void)changedDatePickerValue;

@end

@interface CDatePickerViewEx : UIPickerView <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UIColor *monthSelectedTextColor;
@property (nonatomic, strong) UIColor *monthTextColor;

@property (nonatomic, strong) UIColor *yearSelectedTextColor;
@property (nonatomic, strong) UIColor *yearTextColor;

@property (nonatomic, strong) UIFont *monthSelectedFont;
@property (nonatomic, strong) UIFont *monthFont;

@property (nonatomic, strong) UIFont *yearSelectedFont;
@property (nonatomic, strong) UIFont *yearFont;

@property (nonatomic, assign) NSInteger rowHeight;

@property (nonatomic, strong, readonly) NSDate *date;


@property(nonatomic,weak) id<CDatePickerDelegate> datePickerDelegate;


-(void)setupMinYear:(NSInteger)minYear maxYear:(NSInteger)maxYear;
-(void)selectToday;

@end