//
//  GGGGUtil.h
//  Ganguo
//
//  Created by gump on 11-5-13.
//  Copyright 2011年 f4 . All rights reserved.
//


@interface GGUtil : NSObject

+(NSString *)manageHePaiData:(NSString *)timeString;
+(NSString *)manageHePaiReturnData:(NSString *)timeString;
+(NSString *)manageHePaiDataReturnOnlyDay:(NSString *)timeString;

+(NSString *)manageHePaiReturnContinueTime:(NSString *)timeString;

+ (NSString *)urlAppendRetina:(NSMutableString *)urlString;

+ (UIImage *)scaleAndRotateImage:(UIImage *)image withSize:(CGSize)size;
+ (UIImage *)imageAlpha:(UIImage *)inImage;
+ (UIImage *)imageMono:(UIImage *)image ;
+ (CGSize)getScaleRect:(CGSize)originSize targetSize:(CGSize)maxSize;

+ (UIImage *)makeAShotWithView:(UIView*)aView;


+ (void)setNavBarBackGround:(UINavigationBar *)navigationBar withImageName:(NSString *)imageName;

+ (void)setTableViewBackgroundColor:(UIViewController *)viewController;

+ (void)setTableViewBackgroundImage:(UIViewController *)viewController;

+ (void)setTableViewBackgroundImage:(UITableViewController *)viewController 
                          withImage:(NSString *)imageName;

+ (void)setViewBackgroundColor:(UIViewController *)viewController;

+ (NSMutableDictionary *)getClassPropertys:(id)fromClass;

+ (void)allTextResignFirstResponder:(id)fromClass component:(NSString *)component;

+ (UIColor *)randomColor;

+ (void) showAlert: (NSString *) theMessage title:(NSString *) theTitle;

+ (void) cleanCellFooter: (UITableViewController *)tableView ;


+ (NSMutableDictionary *)stringToDictionary:(NSString *)sourceString withSeparator:(NSString *)separator;

+ (NSString *)getShowName:(NSString *)username  
            withFirstName:(NSString *)firstName 
             withLastName:(NSString *)lastName;

+ (BOOL)canSendSMS;

+ (NSInteger) getSystemVersionAsAnInteger;
/*
+ (BOOL)isExistenceNetwork;

+ (BOOL)isWWANNetwork;

+ (BOOL)checkNetworkConnection;

+ (void)setSFHFKey:(NSString *)key withVale:(NSString *)value;
+ (NSString *)getSFHFKey:(NSString *)key;
+ (void)deleteSFHFKey:(NSString *)key;
*/
+ (CGFloat)getScreenWidth;
+ (CGFloat)getScreenHeight;


+ (NSString *)getDistanceText:(double)distance;

+ (void)storeBadge;

+ (int)getStoredBadge;

+ (void)simulateMemoryWarning;

+ (NSString *)limitString:(NSString *)string withLength:(unsigned short)length;

+ (NSString*)getLocalYYMMDD;

+ (NSString*)tranDate:(NSDate*)aDate Format:(NSString*)dateFormat;

+ (NSDate*)getDateWithString:(NSString*) dateStr Format:(NSString*)dateFormat;
+ (NSDate *)getToday;
+ (NSString *)timeToText:(NSDate *)time;

+ (NSString *)getLargeSinaAvatar:(NSString *)shortUrl;
/*
+ (NSString *)emptyLazyString:(NSString *)source;
*/
+ (BOOL)stringIsEmpty:(NSString *) aString;
+ (BOOL)stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace;

+ (NSString *)urlEncodeValue:(NSString *)str;

+ (id)loadNibWithClassStr:(NSString*)classStr;
/*
+ (MBProgressHUD *)progressHUD:(MBProgressHUD *)hub
                         owner:(UIViewController *)controller;

+ (void)showProgressHUDWithMessage:(NSString *)message
                               hub:(MBProgressHUD *)hub
                             owner:(UIViewController *)controller;

+ (void)hideProgressHUD:(BOOL)animated
                    hub:(MBProgressHUD *)hub
                  owner:(UIViewController *)controller;

+ (void)showProgressHUDCompleteMessage:(NSString *)message
                                   hub:(MBProgressHUD *)hub
                                  mode:(MBProgressHUDMode)mode
                                 owner:(UIViewController *)controller;

+ (void)showProgressHUDWarningMessage:(NSString *)message
                                  hub:(MBProgressHUD *)hub
                                owner:(UIViewController *)controller;

+ (void)showProgressHUDSuccessMessage:(NSString *)message
                                  hub:(MBProgressHUD *)hub
                                owner:(UIViewController *)controller;

+ (void)showProgressHUDErrorMessage:(NSString *)message
                                hub:(MBProgressHUD *)hub
                              owner:(UIViewController *)controller;
*/
+ (void)configAvatar:(UIImageView *)avatar borderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth;


+ (CGFloat)calculateLabelWidth:(NSString *)labelContent withFont:(UIFont *)font withHeight:(CGFloat)height;

+ (CGFloat)calculateLabelHeight:(NSString *)labelContent withFont:(UIFont *)font withWidth:(CGFloat)width;

//将字典中除了NSArray以及NSDictionary类型之外的，其他所有类型都转换为NSString
+ (NSDictionary *)convertAllNSNumberToNSStringInDictionary:(NSDictionary *)paramDict;

+ (void)configLayer:(CALayer *)layer bounds:(CGRect)bounds;
//+ (void)configNavigationBar:(UINavigationBar *)bar;

+ (UITableViewCell *)emptyCellForTable:(UITableView *)tableView;

@end
