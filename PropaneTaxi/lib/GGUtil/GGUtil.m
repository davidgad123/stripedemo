//
//  GGUtil.m
//  Ganguo
//
//  Created by gump on 11-5-13.
//  Copyright 2011年 f4 . All rights reserved.
//

#import "GGUtil.h"


#import <objc/runtime.h>
//#import "NSString+Reverse.h"
//#import "SFHFKeychainUtils.h"
#import <MessageUI/MessageUI.h>
//#import "Reachability.h"

@implementation GGUtil

+(NSString *)manageHePaiData:(NSString *)timeString{
    return [[[timeString componentsSeparatedByString:@"+"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@"T" withString:@" "];
}

+(NSString *)manageHePaiDataReturnOnlyDay:(NSString *)timeString{
    return [[[timeString componentsSeparatedByString:@"+"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@"T" withString:@" "];
}
+(NSString *)manageHePaiReturnData:(NSString *)timeString{
    return [[[[timeString componentsSeparatedByString:@"+"] objectAtIndex:0] componentsSeparatedByString:@"T"] objectAtIndex:0];
}

+(NSString *)manageHePaiReturnContinueTime:(NSString *)timeString{
    NSString *time = [[[timeString componentsSeparatedByString:@"+"] objectAtIndex:0]
                      stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    
    NSDateFormatter *date=[[NSDateFormatter alloc] init];
    [date setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *d=[date dateFromString:time];
    
    NSTimeInterval late=[d timeIntervalSince1970]*1;
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval now=[dat timeIntervalSince1970]*1;
    NSString *continueTime=@"";
    NSTimeInterval cha=now-late;
    
    if (cha/60<1) {
        continueTime = [NSString stringWithFormat:@"%f", cha];
        continueTime = [continueTime substringToIndex:continueTime.length-7];
        continueTime=[NSString stringWithFormat:@"%@秒前", continueTime];
        
    }
    if (cha/60>1&&cha/3600<1) {
        continueTime = [NSString stringWithFormat:@"%f", cha/60];
        continueTime = [continueTime substringToIndex:continueTime.length-7];
        continueTime=[NSString stringWithFormat:@"%@分钟前", continueTime];
        
    }
    if (cha/3600>1&&cha/86400<1) {
        continueTime = [NSString stringWithFormat:@"%f", cha/3600];
        continueTime = [continueTime substringToIndex:continueTime.length-7];
        continueTime=[NSString stringWithFormat:@"%@小时前", continueTime];
    }
    
    if (cha/86400>1&&cha/2592000<1)
        
    {
        continueTime = [NSString stringWithFormat:@"%f", cha/86400];
        continueTime = [continueTime substringToIndex:continueTime.length-7];
        continueTime=[NSString stringWithFormat:@"%@天前", continueTime];
    }
    if (cha/2592000>1) {
        continueTime = [NSString stringWithFormat:@"%f", cha/2592000];
        continueTime = [continueTime substringToIndex:continueTime.length-7];
        continueTime=[NSString stringWithFormat:@"%@月前", continueTime];
    }
    
    return continueTime;

}

- (void)dealloc {    

}


// -------------------------------- nouse begein --------------------------------

//+ (BOOL)isRetina {
//    CGFloat scale = [[UIScreen mainScreen] scale];
//    if (scale > 1.0) {
//        return YES;
//    } else {
//        return NO;
//    }
//}


//zoom image size
//+ (UIImage *)zoomImage_nouse:(UIImage *)img withSize:(CGSize)size{
//	UIImage* returnImage = nil;
//	int h = img.size.height;
//	int w = img.size.width;
//    CGFloat  maxWidth=size.width;
//    CGFloat  maxHeight=size.height;
//    
//	if(h <= maxWidth && w <= maxHeight)
//	{
//		returnImage=img;
//	}else {
//		float b = (float)maxWidth/w < (float)maxHeight/h ? (float)maxWidth/w : (float)maxHeight/h;
//		CGSize itemSize = CGSizeMake(b*w, b*h);
//		UIGraphicsBeginImageContext(itemSize);
//		CGRect imageRect = CGRectMake(0, 0, b*w, b*h);
//		[img drawInRect:imageRect];
//		returnImage = UIGraphicsGetImageFromCurrentImageContext();
//		UIGraphicsEndImageContext();
//	}
//	return returnImage;
//}

//+ (TKLoadingView *)showLoading:(UIViewController *)controller 
//        withLoadingView:(TKLoadingView *)loadingView 
//        withTitle:(NSString *)title{    
//    
//    loadingView  = [[TKLoadingView alloc] initWithTitle:title];
//    loadingView.center = CGPointMake(controller.view.bounds.size.width/2, controller.view.bounds.size.height/2);
//    [loadingView startAnimating];
//    [controller.view addSubview:loadingView];
//    
//    return [loadingView autorelease];    
//}
//
//+ (void)removeLoading:(TKLoadingView *)loadingView {   
//    DLog(@"in Util removeLoading");
//     [loadingView stopAnimating];
//     [loadingView removeFromSuperview];   
//}

// -------------------------------- nouse end --------------------------------



#pragma mark -
#pragma mark Image 
+ (UIImage *)scaleAndRotateImage:(UIImage *)image withSize:(CGSize)size{
    CGFloat  maxWidth=size.width;
    CGFloat  maxHeight=size.height;
	
	CGImageRef imgRef = image.CGImage;
	
	CGFloat width = CGImageGetWidth(imgRef);
	CGFloat height = CGImageGetHeight(imgRef);
	
	CGAffineTransform transform = CGAffineTransformIdentity;
    
	CGRect bounds = CGRectMake(0, 0, width, height);
    
    if (width > maxWidth || height > maxHeight) {
        CGFloat maxRatio = maxWidth/maxHeight;
		CGFloat ratio = width/height;
		if (ratio > maxRatio) {
			bounds.size.width = maxWidth;
			bounds.size.height = bounds.size.width / ratio;
		}
		else {
			bounds.size.height = maxHeight;
			bounds.size.width = bounds.size.height * ratio;
		}
	}
	
	CGFloat scaleRatio = bounds.size.width / width;
    
	CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
	CGFloat boundHeight;
	UIImageOrientation orient = image.imageOrientation;
	switch(orient) {
			
		case UIImageOrientationUp: //EXIF = 1
			transform = CGAffineTransformIdentity;
			break;
			
		case UIImageOrientationUpMirrored: //EXIF = 2
			transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			break;
			
		case UIImageOrientationDown: //EXIF = 3
			transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationDownMirrored: //EXIF = 4
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
			transform = CGAffineTransformScale(transform, 1.0, -1.0);
			break;
			
		case UIImageOrientationLeftMirrored: //EXIF = 5
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
			
		case UIImageOrientationLeft: //EXIF = 6
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
			break;
			
		case UIImageOrientationRightMirrored: //EXIF = 7
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeScale(-1.0, 1.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
			
		case UIImageOrientationRight: //EXIF = 8
			boundHeight = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = boundHeight;
			transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
			transform = CGAffineTransformRotate(transform, M_PI / 2.0);
			break;
			
		default:
			[NSException raise:NSInternalInconsistencyException format:NSLocalizedString(@"Invalid image orientation", @"")];
			
	}
	
	UIGraphicsBeginImageContext(bounds.size);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
		CGContextScaleCTM(context, -scaleRatio, scaleRatio);
		CGContextTranslateCTM(context, -height, 0);
	}
	else {
		CGContextScaleCTM(context, scaleRatio, -scaleRatio);
		CGContextTranslateCTM(context, 0, -height);
	}
	
	CGContextConcatCTM(context, transform);
	
	CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
	UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	//[self setRotatedImage:imageCopy];
	return imageCopy;
}



+(UIImage *)imageAlpha:(UIImage *)inImage{
    
    CGRect imageRect = CGRectMake(0, 0, inImage.size.width, inImage.size.height);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    void* data = malloc(inImage.size.height*inImage.size.width*4);
    
    CGContextRef context = CGBitmapContextCreate(data, imageRect.size.width, imageRect.size.height, 8, imageRect.size.width * 4, colorSpace, kCGImageAlphaPremultipliedLast);
    CGContextClipToMask(context, imageRect, inImage.CGImage);
    CGContextSetRGBFillColor(context, 1, 1, 1, 1);
    CGContextFillRect(context, imageRect);
    
    CGImageRef finalImage = CGBitmapContextCreateImage(context);
    UIImage *returnImage = [UIImage imageWithCGImage:finalImage];       
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    CGImageRelease(finalImage);
    
    return returnImage;
}




+(UIImage *)imageMono:(UIImage *)originalImage{    
    
    CGColorSpaceRef colorSapce = CGColorSpaceCreateDeviceGray();
    CGContextRef context = CGBitmapContextCreate(nil, originalImage.size.width, originalImage.size.height, 8, originalImage.size.width, colorSapce, kCGImageAlphaNone);
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGContextSetShouldAntialias(context, NO);
    CGContextDrawImage(context, CGRectMake(0, 0, originalImage.size.width, originalImage.size.height), [originalImage CGImage]);
    
    CGImageRef bwImage = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSapce);
    
    UIImage *returnImage = [UIImage imageWithCGImage:bwImage]; // This is result B/W image.
    CGImageRelease(bwImage);
    

    return returnImage;
}

+ (UIImage *)makeAShotWithView:(UIView*)aView
{
    UIGraphicsBeginImageContextWithOptions(aView.bounds.size, YES, [UIScreen mainScreen].scale);
    [aView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *rtImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return rtImage;
}


+(CGSize)getScaleRect:(CGSize)originSize targetSize:(CGSize)maxSize{
    CGFloat width=originSize.width;
    CGFloat height=originSize.height;
    
    CGFloat wSize=width/maxSize.width;
    CGFloat hSize=height/maxSize.height;
    
    CGFloat currSize= wSize > hSize ? wSize : hSize;
    
    CGFloat finelWidth=width/currSize;
    CGFloat finelHeight=height/currSize;
    
    return CGSizeMake(finelWidth, finelHeight);
}



#pragma mark -
#pragma mark Controller 
+ (void)setNavBarBackGround:(UINavigationBar *)navigationBar withImageName:(NSString *)imageName{
    //navController.navigationBar.barStyle = UIBarStyleBlack;
    //UINavigationBar* customNavigationBar = (UINavigationBar*)navController.navigationBar;
    //[navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bg.png"] forBarMetrics:UIBarMetricsDefault];
    //navigationBar.tintColor = RGB(38, 121, 199);
    
    [[UIDevice currentDevice] systemVersion];
    
    UIImage *toolBarIMG = [UIImage imageNamed: imageName];  

    
    if ([GGUtil getSystemVersionAsAnInteger] >= 50000) {        
        //iOS 5        
        if ([navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) { 
            [navigationBar setBackgroundImage:toolBarIMG forBarMetrics:UIBarMetricsDefault];
        }        
    } else {        
        //iOS 4
        
//        for (UIView *view in navigationBar.subviews) {
//            DLog(@"in nav bar: %@",view);
//            if (view.tag == 999) {
//                [view removeFromSuperview];
//            }
//        }        
        
        UIView *oldBg = [navigationBar viewWithTag:999];
        if (oldBg) {
            [oldBg removeFromSuperview];
        }
    
        UIImageView *navBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        navBackground.tag = 999;
        [navigationBar insertSubview:navBackground atIndex:0]; 
    }
    
//    navigationBar.tintColor = RGB(38, 121, 199);
//    navigationBar.tintColor = RGB(10, 124, 203);
//    navigationBar.tintColor = RGB(34, 114, 185);
//    navigationBar.tintColor = RGB(11, 115, 186);
//    navigationBar.tintColor = RGB(48, 97, 194);


}


+ (void)setTableViewBackgroundColor:(UIViewController *)viewController{
    viewController.navigationController.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
}

+ (void)setTableViewBackgroundImage:(UITableViewController *)viewController{

        viewController.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"new_ditu.png"]];

}


+ (void)setTableViewBackgroundImage:(UITableViewController *)viewController withImage:(NSString *)imageName{

        viewController.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",imageName]]];
 
}


+ (void)setViewBackgroundColor:(UIViewController *)viewController{
//    [viewController.view setBackgroundColor:RGB(241, 238, 233)];
    
    [viewController.view setBackgroundColor:[UIColor whiteColor]];
    
}

static const char *getPropertyType(objc_property_t property) {
    const char *attributes = property_getAttributes(property);
    char buffer[1 + strlen(attributes)];
    strcpy(buffer, attributes);
    char *state = buffer, *attribute;
    while ((attribute = strsep(&state, ",")) != NULL) {
        if (attribute[0] == 'T') {
            //fprintf(stdout, "%s \n",attribute);
            if (attribute[1] == '@'){
                    return (const char *)[[NSData dataWithBytes:(attribute + 3) length:strlen(attribute) - 4] bytes];
            }
        }
    }
    return "";
}

+ (NSMutableDictionary *)getClassPropertys:(id)fromClass {
    NSMutableDictionary *props = [[NSMutableDictionary alloc] init];
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([fromClass class], &outCount);
    for(i = 0; i < outCount; i++) {
        //        objc_property_t property = properties[i];
        //        fprintf(stdout, "%s %s\n", property_getName(property), property_getAttributes(property));
        objc_property_t property = properties[i];
        const char *propName = property_getName(property);
        const char *propType = getPropertyType(property);
        if(propName) {
            
            NSString *propertyName = [NSString stringWithUTF8String:propName];
            NSString *propertyType = [NSString stringWithUTF8String:propType];
            if (propertyType) {
                //DLog(@"propName=%@ propType=%@",propertyName,propertyType);                
                [props setObject:propertyType forKey:propertyName];
            } 
        }
    }
    free(properties);
    return props;
}

+ (void)allTextResignFirstResponder:(id)fromClass component:(NSString *)component{
    NSMutableDictionary *props=[self getClassPropertys:fromClass];
    NSString *propType;
    for (NSString *prop in props) {
        propType=[props objectForKey:prop];
        //DLog(@"%@ is '%@'",prop, propType);        
       
        if([propType isEqualToString: component])  {
            //DLog(@"in UITextField: %@", propType);
            SEL s = NSSelectorFromString(prop);
            UIControl *tempText=[(UIControl *)fromClass performSelector:s];
            [tempText resignFirstResponder];
        }
     
	}
    
 
    
}


#pragma mark -
#pragma mark Cell
+ (void) cleanCellFooter: (UITableViewController *)tableView {    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.tableView.tableFooterView = footer;
}


#pragma mark -
#pragma mark Color
+ (UIColor *)randomColor
{
    static BOOL seeded = NO;
    if (!seeded) {
        seeded = YES;
        srandom(time(NULL));
    }
    CGFloat red = (CGFloat)random()/(CGFloat)RAND_MAX;
    CGFloat blue = (CGFloat)random()/(CGFloat)RAND_MAX;
    CGFloat green = (CGFloat)random()/(CGFloat)RAND_MAX;
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0f];
}

#pragma mark -
#pragma mark Dictionary
+ (NSMutableDictionary *)stringToDictionary:(NSString *)sourceString withSeparator:(NSString *)separator{
    NSArray *chunks = [sourceString componentsSeparatedByString: separator];
    NSMutableDictionary *stringDict=[[NSMutableDictionary alloc]init];
    
    for(NSString *chunk in chunks){
        NSArray *keyValue=[chunk componentsSeparatedByString: @"="];      
        DLog(@"key:%@", [keyValue objectAtIndex:0]);
        DLog(@"value:%@", [keyValue objectAtIndex:1]);        
        [stringDict setValue:[keyValue objectAtIndex:1] forKey:[keyValue objectAtIndex:0]];
    }
    
    return stringDict;
}

#pragma mark -
#pragma mark System Check
+ (BOOL)canSendSMS{
    BOOL canSMS=NO;
    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));

    DLog(@"can send SMS [%d]", [messageClass canSendText]);
    
    if (messageClass != nil) {
        if ([messageClass canSendText]) {
            canSMS=YES;
        } else {
            [GGUtil showAlert:NSLocalizedString(@"Device can not use SMS.",@"") 
                      title:NSLocalizedString(@"Warning",@"")];

            // [self alertWithTitle:nil msg:@"设备没有短信功能"];
        }
    } else {
        [GGUtil showAlert:NSLocalizedString(@"SMS supported on iOS 4.0 .",@"") 
                  title:NSLocalizedString(@"Warning",@"")];
        //[self alertWithTitle:nil msg:@"iOS版本过低，iOS4.0以上才支持程序内发送短信"];
    }
    return canSMS;

}


+ (NSInteger) getSystemVersionAsAnInteger{
    int index = 0;
    NSInteger version = 0;
    
    NSArray* digits = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    NSEnumerator* enumer = [digits objectEnumerator];
    NSString* number;
    while (number = [enumer nextObject]) {
        if (index>2) {
            break;
        }
        NSInteger multipler = powf(100, 2-index);
        version += [number intValue]*multipler;
        index++;
    }
    return version;
}

/*
+(BOOL)isExistenceNetwork
{
	BOOL isExistenceNetwork=NO;
	Reachability *r = [Reachability reachabilityWithHostname:@"www.yiqi.at"];
    switch ([r currentReachabilityStatus]) {
        case NotReachable:
			isExistenceNetwork=NO;
            //   DLog(@"没有网络");
            break;
        case ReachableViaWWAN:
			isExistenceNetwork=YES;
            //   DLog(@"正在使用3G网络");
            break;
        case ReachableViaWiFi:
			isExistenceNetwork=YES;
            //  DLog(@"正在使用wifi网络");        
            break;
    }
//	if (!isExistenceNetwork) {
//		UIAlertView *myalert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning",@"")
//                                                          message:NSLocalizedString(@"Network not available" ,@"")
//                                                         delegate:self 
//                                                cancelButtonTitle:NSLocalizedString(@"OK",@"")
//                                                otherButtonTitles:nil,nil];
//		[myalert show];
//	}
	return isExistenceNetwork;
}

+ (BOOL)isWWANNetwork
{
    BOOL isWWAN = NO;
    NetworkStatus status = [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    if (status == ReachableViaWWAN) {
        isWWAN = YES;
    }
    return isWWAN;
}

+ (BOOL)checkNetworkConnection
{
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    if (!didRetrieveFlags) {
        return NO;
    }
    
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    return (isReachable && !needsConnection) ? YES : NO;
}

#pragma mark -
#pragma mark SFHFKeychainUtils
+ (void)setSFHFKey:(NSString *)key withVale:(NSString *)value{
    [SFHFKeychainUtils storeUsername:key andPassword:value forServiceName:KEYCHAI_SERVICE_NAME updateExisting:TRUE error:nil];
}

+ (NSString *)getSFHFKey:(NSString *)key{
    return [SFHFKeychainUtils getPasswordForUsername:key andServiceName:KEYCHAI_SERVICE_NAME error:nil];
}

+ (void)deleteSFHFKey:(NSString *)key{
    [SFHFKeychainUtils deleteItemForUsername:key andServiceName:KEYCHAI_SERVICE_NAME error:nil]; 
    
}
*/

#pragma mark -
#pragma mark Screen
+ (CGFloat)getScreenWidth{
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGFloat width = CGRectGetWidth(screen);
    return width;
    
}

+ (CGFloat)getScreenHeight{
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGFloat height = CGRectGetHeight(screen);
    return height;    
}



#pragma mark -
#pragma mark Alert
+ (void) showAlert: (NSString *) theMessage title:(NSString *) theTitle{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:theTitle message:theMessage delegate:nil cancelButtonTitle:NSLocalizedString(@"我知道了", @"") otherButtonTitles: nil];
    [av show]; 
}

#pragma mark -
#pragma mark URL
+ (NSString *)urlAppendRetina:(NSMutableString *)urlString{
    if ( isRetina) {
        NSRange range = [urlString rangeOfString:@"?"];
        if (range.length == 0) {
            [urlString appendString:@"?hd=1"];
        } else {
            [urlString appendString:@"&hd=1"];
        }
    }
    return  urlString;
}


#pragma mark -
#pragma mark Badge
+ (void)storeBadge{
    NSString *path = [DOCUMENT_PATH stringByAppendingString:@"/badge"];
    NSString *badge = [NSString stringWithFormat:@"%d", [[UIApplication sharedApplication] applicationIconBadgeNumber]];
    [badge writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

+ (int)getStoredBadge{
    NSString *path = [DOCUMENT_PATH stringByAppendingString:@"/badge"];
    int badge = [[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil] intValue];
    return badge;
}

#pragma mark -
#pragma mark String
+ (BOOL)stringIsEmpty:(NSString *) aString {
    
    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }
    
    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    } else {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }
    
    return NO; 
}

+ (BOOL)stringIsEmpty:(NSString *) aString shouldCleanWhiteSpace:(BOOL)cleanWhileSpace {
    
    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }
    
    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    } 
    
    if (cleanWhileSpace) {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }
    
    return NO; 
}


#pragma mark -
#pragma mark Text
+ (NSString *)limitString:(NSString *)string withLength:(unsigned short)length{
    NSString* returnString=string;
    if([string length]>length) {
        returnString=[NSString stringWithFormat:@"%@...",[string substringToIndex:length-3]];
    }
    DLog(@"limitString length:%d :%@",[returnString length],returnString);
    return returnString;
}



+ (NSString *)getDistanceText:(double)distance{
    NSString* distanceText=@"";
    if (distance<1000) {
        distanceText=[NSString stringWithFormat:@"%d00米", ((int)distance/100)+1];
    }else{
        distanceText=[NSString stringWithFormat:@"%dkm", ((int)distance/1000)];
    }
    return distanceText;    
}

+ (NSString *)getShowName:(NSString *)username withFirstName:(NSString *)firstName withLastName:(NSString *)lastName{
    if ((firstName && [firstName length]>0) ||  (lastName && [lastName length]>0 )) {
        //DLog(@"username:%@  firstname:%@  lastname:%@ = %@ %@", username,firstName,lastName,firstName,lastName);
        return [NSString stringWithFormat:@"%@ %@",firstName,lastName];
    }else{
        //DLog(@"username:%@  firstname:%@  lastname:%@ = %@ ", username,firstName,lastName,username);
        return username;
    }
}
/*
+ (NSString *)emptyLazyString:(NSString *)source{
    DLog(@"in emptyLazyString: %@ length:%d",source,[source length]);
    //source=@"";
    if ([GGUtil stringIsEmpty:source shouldCleanWhiteSpace:YES]) {
        DLog(@"in emptyLazyString return: %@ ",LS(@"This guy is lazy and have nothing left."));
        return LS(@"This guy is lazy and have nothing left.");
    }else{
        DLog(@"in emptyLazyString return: %@ ",source);
        return source;
    }
}
*/
#pragma mark -
#pragma mark Date
+ (NSString*)getLocalYYMMDD
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyyMMdd";
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *localDateString = [dateFormatter stringFromDate:[NSDate date]];
    return localDateString;
}

+ (NSString*)tranDate:(NSDate*)aDate Format:(NSString*)dateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = dateFormat;
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *localDateString = [dateFormatter stringFromDate:aDate];
    return localDateString;
}

+ (NSDate*)getDateWithString:(NSString*) dateStr Format:(NSString*)dateFormat
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:dateFormat];
    NSDate *date = [formatter dateFromString:dateStr];
    return date;
}

+ (NSDate *)getToday{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSWeekdayCalendarUnit;

    NSDateComponents *comps = [calendar components:unitFlags fromDate:[NSDate date]];
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];
    NSDate *suppliedDate = [calendar dateFromComponents:comps];
    

    
    return suppliedDate;
}

+ (NSString *)timeToText:(NSDate *)time{
    NSString *timeText=@"";
    
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *components = [calender components:(NSYearCalendarUnit | 
                                                         NSMonthCalendarUnit | 
                                                         NSDayCalendarUnit | 
                                                         NSHourCalendarUnit | 
                                                         NSMinuteCalendarUnit | 
                                                         NSSecondCalendarUnit) 
                                               fromDate:time
                                                 toDate:[NSDate date] options:0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    if ([components year]) {
        timeText = [[formatter stringFromDate:time] substringToIndex:10];
    } else if ([components month]) {
        timeText = [[formatter stringFromDate:time] substringToIndex:10];
    } else if ([components day]) {
        if ([components day] > 7) {
            timeText = [[formatter stringFromDate:time] substringToIndex:10];
        } else {
            timeText = [NSString stringWithFormat:@"%d天前", [components day]];
        }
    } else if ([components hour]) {
        timeText = [NSString stringWithFormat:@"%d小时前", [components hour]];
    } else if ([components minute]) {
        if ([components minute] < 0) {
            timeText = @"刚刚";
        } else {
            timeText = [NSString stringWithFormat:@"%d分钟前", [components minute]];
        }
    } else if ([components second]) {
        if ([components second] < 0) {
            timeText = @"刚刚";
        } else {
            timeText = [NSString stringWithFormat:@"%d秒前", [components second]];
        }
    } else {
        timeText = @"刚刚";
    }
    
    return timeText;
}

#pragma mark -
#pragma mark Memory
+ (void)simulateMemoryWarning
{
#if TARGET_IPHONE_SIMULATOR
#ifdef DEBUG
    CFNotificationCenterPostNotification(CFNotificationCenterGetDarwinNotifyCenter(), 
                                         (CFStringRef)@"UISimulatedMemoryWarningNotification", NULL, NULL, true);
#endif
#endif
}


//+ (NSString *)genToken:(NSNumber *)currTimestamp withPlus:(int)plus{
//    NSNumber* timeStamp=[NSNumber numberWithInt:[currTimestamp intValue]+plus];
//    NSString* timeStampString=[[NSString stringWithFormat:@"%d",[timeStamp intValue]] reverseString] ;
//    return [NSString base64encode:timeStampString] ;   
//}


#pragma mark -
#pragma mark URL
+ (NSString *)getLargeSinaAvatar:(NSString *)shortUrl{
    NSString *largeUrl=shortUrl;
    NSRange textRange = [shortUrl rangeOfString:@"sinaimg"];
    if(textRange.location != NSNotFound) {
        largeUrl = [shortUrl stringByReplacingOccurrencesOfString:@"/50/" withString:@"/180/"];   
        DLog(@"short url:%@",shortUrl);
        DLog(@"large url:%@",largeUrl);
    }
    return largeUrl;
}

+ (NSString *)urlEncodeValue:(NSString *)str
{
    NSString *result = (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, CFSTR(":/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8));
    return result;
}

+ (id)loadNibWithClassStr:(NSString*)classStr
{
    NSString *suffix = @"";
    if (isPad) {
        suffix = @"-pad";
    }
    else if(isiPhone5){
        suffix = @"-568h";
    }
    return [[NSClassFromString(classStr) alloc] initWithNibName:[NSString stringWithFormat:@"%@%@",classStr,suffix] bundle:nil];
}

#pragma mark - MBProgressHUD
/*
+ (MBProgressHUD *)progressHUD:(MBProgressHUD *)hub
                         owner:(UIViewController *)controller {
    if (!hub) {
        hub = [[MBProgressHUD alloc] initWithView:controller.view];
        hub.minSize = CGSizeMake(100, 100);
        hub.minShowTime = 1;
        hub.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MWPhotoBrowser.bundle/images/Checkmark.png"]];
        [controller.view addSubview:hub];
        [controller.view bringSubviewToFront:hub];
    }
    return hub;
}

+ (void)showProgressHUDWithMessage:(NSString *)message
                               hub:(MBProgressHUD *)hub
                             owner:(UIViewController *)controller {
    //hub = [GGUtil progressHUD:hub owner:controller];
    [controller.view bringSubviewToFront:hub];
    hub.labelText = message;
    hub.mode = MBProgressHUDModeIndeterminate;
    [hub show:YES];
    controller.navigationController.navigationBar.userInteractionEnabled = YES;
}

+ (void)hideProgressHUD:(BOOL)animated
                    hub:(MBProgressHUD *)hub
                  owner:(UIViewController *)controller {
    //hub = [GGUtil progressHUD:hub owner:controller];
    if (hub) {
        [hub hide:animated];
    }
    controller.navigationController.navigationBar.userInteractionEnabled = YES;
}

+ (void)showProgressHUDCompleteMessage:(NSString *)message
                                   hub:(MBProgressHUD *)hub
                                  mode:(MBProgressHUDMode)mode
                                 owner:(UIViewController *)controller {
    [controller.view bringSubviewToFront:hub];
    if (message) {
        //hub = [GGUtil progressHUD:hub owner:controller];
        if (hub.isHidden) [hub show:YES];
        hub.labelText = message;
        hub.mode = mode;
        [hub hide:YES afterDelay:1.0];
    } else {
        [hub hide:YES];
    }
    controller.navigationController.navigationBar.userInteractionEnabled = YES;
}


+ (void)showProgressHUDWarningMessage:(NSString *)message
                                  hub:(MBProgressHUD *)hub
                                owner:(UIViewController *)controller {
    [controller.view bringSubviewToFront:hub];
    if (message) {
        //hub = [GGUtil progressHUD:hub owner:controller];
        [hub show:YES];
        hub.labelText = message;
        hub.mode = MBProgressHUDModeCustomView;
        UIView *hubView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 98, 98)];
        UIImageView *hubImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"chat_volume_warning.png"]];
        [hubView addSubview:hubImage];
        hub.customView = hubView;
        [hub hide:YES afterDelay:1.0];
    } else {
        [hub hide:YES];
    }
    controller.navigationController.navigationBar.userInteractionEnabled = YES;
}

+ (void)showProgressHUDSuccessMessage:(NSString *)message
                                  hub:(MBProgressHUD *)hub
                                owner:(UIViewController *)controller {
    [controller.view bringSubviewToFront:hub];
    if (message) {
        //hub = [GGUtil progressHUD:hub owner:controller];
        [hub show:YES];
        hub.labelText = message;
        hub.mode = MBProgressHUDModeCustomView;
        UIImageView *hubImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"mb_success.png"]];
        hub.customView = hubImage;
        [hub hide:YES afterDelay:1.0];
    } else {
        [hub hide:YES];
    }
    controller.navigationController.navigationBar.userInteractionEnabled = YES;
}

+ (void)showProgressHUDErrorMessage:(NSString *)message
                                hub:(MBProgressHUD *)hub
                              owner:(UIViewController *)controller {
    [controller.view bringSubviewToFront:hub];
    if (message) {
        //hub = [GGUtil progressHUD:hub owner:controller];
        [hub show:YES];
        hub.labelText = message;
        hub.mode = MBProgressHUDModeCustomView;
        UIImageView *hubImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"mb_error.png"]];
        hub.customView = hubImage;
        [hub hide:YES afterDelay:1.0];
    } else {
        [hub hide:YES];
    }
    controller.navigationController.navigationBar.userInteractionEnabled = YES;
}
*/
+ (void)configAvatar:(UIImageView *)avatar borderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth{
    avatar.layer.borderColor = borderColor.CGColor;
    avatar.layer.borderWidth = borderWidth;
    avatar.layer.shadowPath = [UIBezierPath bezierPathWithRect:avatar.bounds].CGPath;
    avatar.layer.shadowColor = [UIColor blackColor].CGColor;
    avatar.layer.shadowRadius = 1.0;
    avatar.layer.shadowOpacity = 0.2;
    avatar.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    avatar.layer.masksToBounds = NO;
}

//label calculate
+ (CGFloat)calculateLabelWidth:(NSString *)labelContent withFont:(UIFont *)font withHeight:(CGFloat)height{
    CGSize labelSize =  [labelContent
                         sizeWithFont:font
                         constrainedToSize:CGSizeMake(MAXFLOAT, height)
                         lineBreakMode:NSLineBreakByWordWrapping];
    return labelSize.width;
}
+ (CGFloat)calculateLabelHeight:(NSString *)labelContent withFont:(UIFont *)font withWidth:(CGFloat)width{
    CGSize labelSize =  [labelContent
                         sizeWithFont:font
                         constrainedToSize:CGSizeMake(width, MAXFLOAT)
                         lineBreakMode:NSLineBreakByWordWrapping];
    return labelSize.height;
}

+ (NSDictionary *)convertAllNSNumberToNSStringInDictionary:(NSDictionary *)paramDict{
    NSArray *allKeys = paramDict.allKeys;
    NSMutableDictionary *resultDict = [NSMutableDictionary dictionary];
    for (NSString *key in allKeys) {
        id value = [paramDict objectForKey:key];
        if ([value isKindOfClass:NSDictionary.class] || [value isKindOfClass:NSArray.class]) {
            [resultDict setObject:value forKey:key];
        }else{
            NSString *valueString = [NSString stringWithFormat:@"%@",value];
            [resultDict setObject:valueString forKey:key];
        }
    }
    return resultDict;
}


+ (void)configLayer:(CALayer *)layer bounds:(CGRect)bounds{
    layer.masksToBounds = NO;
    layer.shadowPath = [UIBezierPath bezierPathWithRect:bounds].CGPath;
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowRadius = 1.0f;
    layer.shadowOpacity = 0.5f;
    layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
}
/*
+ (void)configNavigationBar:(UINavigationBar *)bar{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
        bar.shadowImage = [UIImage new];
    }
    [GGUtil configLayer:bar.layer bounds:bar.bounds];
    UIImage *gradientImage44 = [UIImage imageNamed:@"nav_bg"];
    [bar setBackgroundImage:gradientImage44  forBarMetrics:UIBarMetricsDefault];
}
*/
+ (UITableViewCell *)emptyCellForTable:(UITableView *)tableView{
    static NSString *MyIdentifier = @"EmptyCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return cell;
}

@end
